<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="6.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="2.54" unitdist="mm" unit="mm" style="dots" multiple="1" display="yes" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="15" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="15" fill="8" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="13" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="13" fill="8" visible="no" active="no"/>
<layer number="25" name="tNames" color="3" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="3" fill="8" visible="no" active="no"/>
<layer number="27" name="tValues" color="11" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="11" fill="8" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="10" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="14" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="14" fill="8" visible="no" active="no"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="3" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="9" fill="1" visible="yes" active="yes"/>
<layer number="100" name="PaJa" color="12" fill="7" visible="no" active="yes"/>
<layer number="101" name="Doplnky" color="5" fill="1" visible="yes" active="yes"/>
<layer number="102" name="Kola" color="11" fill="7" visible="no" active="yes"/>
<layer number="103" name="Popisy" color="2" fill="8" visible="no" active="yes"/>
<layer number="104" name="Zapojeni" color="6" fill="7" visible="no" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="110" name="wago-seda" color="7" fill="8" visible="yes" active="yes"/>
<layer number="111" name="wago-cervena" color="12" fill="8" visible="yes" active="yes"/>
<layer number="112" name="wago-zelena" color="2" fill="8" visible="yes" active="yes"/>
<layer number="113" name="wago-modra" color="1" fill="8" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="107" name="Crop" color="7" fill="1" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="98" name="Guide" color="6" fill="1" visible="no" active="no"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="no" active="no"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="no"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="no"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="no" active="no"/>
<layer number="200" name="200bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="no" active="no"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="no" active="no"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="no"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="no"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="no"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="no"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="no" active="no"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="no"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="217" name="217bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="OrgLBR" color="13" fill="1" visible="no" active="no"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="#PaJa_20">
<packages>
<package name="DIL8">
<wire x1="-5.079" y1="-0.635" x2="-5.079" y2="0.635" width="0.127" layer="21" curve="180" cap="flat"/>
<wire x1="3.81" y1="3.3338" x2="3.81" y2="3.175" width="0.127" layer="102"/>
<wire x1="-3.81" y1="3.3338" x2="-3.81" y2="3.175" width="0.127" layer="102"/>
<wire x1="-1.27" y1="3.3338" x2="-1.27" y2="3.175" width="0.127" layer="102"/>
<wire x1="1.27" y1="3.3338" x2="1.27" y2="3.175" width="0.127" layer="102"/>
<wire x1="-3.81" y1="-3.3338" x2="-3.81" y2="-3.175" width="0.127" layer="102"/>
<wire x1="3.81" y1="-3.3338" x2="3.81" y2="-3.175" width="0.127" layer="102"/>
<wire x1="1.27" y1="-3.3338" x2="1.27" y2="-3.175" width="0.127" layer="102"/>
<wire x1="-1.27" y1="-3.3338" x2="-1.27" y2="-3.175" width="0.127" layer="102"/>
<wire x1="-5.08" y1="3.175" x2="-5.08" y2="0.635" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-0.635" x2="-5.08" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-5.08" y1="3.175" x2="5.08" y2="3.175" width="0.127" layer="21"/>
<wire x1="5.08" y1="3.175" x2="5.08" y2="-3.175" width="0.127" layer="21"/>
<wire x1="5.08" y1="-3.175" x2="-5.08" y2="-3.175" width="0.127" layer="21"/>
<circle x="3.81" y="3.81" radius="0.4763" width="0.127" layer="102"/>
<circle x="-3.81" y="3.81" radius="0.4763" width="0.127" layer="102"/>
<circle x="-1.27" y="3.81" radius="0.4763" width="0.127" layer="102"/>
<circle x="1.27" y="3.81" radius="0.4763" width="0.127" layer="102"/>
<circle x="-3.81" y="-3.81" radius="0.4763" width="0.127" layer="102"/>
<circle x="3.81" y="-3.81" radius="0.4763" width="0.127" layer="102"/>
<circle x="1.27" y="-3.81" radius="0.4763" width="0.127" layer="102"/>
<circle x="-1.27" y="-3.81" radius="0.4763" width="0.127" layer="102"/>
<pad name="1" x="-3.81" y="-3.81" drill="0.8128" diameter="1.4" shape="long" rot="R90"/>
<pad name="2" x="-1.27" y="-3.81" drill="0.8128" diameter="1.4" shape="long" rot="R90"/>
<pad name="3" x="1.27" y="-3.81" drill="0.8128" diameter="1.4" shape="long" rot="R90"/>
<pad name="4" x="3.81" y="-3.81" drill="0.8128" diameter="1.4" shape="long" rot="R90"/>
<pad name="5" x="3.81" y="3.81" drill="0.8128" diameter="1.4" shape="long" rot="R90"/>
<pad name="6" x="1.27" y="3.81" drill="0.8128" diameter="1.4" shape="long" rot="R90"/>
<pad name="7" x="-1.27" y="3.81" drill="0.8128" diameter="1.4" shape="long" rot="R90"/>
<pad name="8" x="-3.81" y="3.81" drill="0.8128" diameter="1.4" shape="long" rot="R90"/>
<text x="-3.975" y="-2.385" size="1.016" layer="21">1</text>
<text x="-4.134" y="1.431" size="1.016" layer="21">8</text>
<text x="3.339" y="-2.385" size="1.016" layer="21">4</text>
<text x="3.498" y="1.431" size="1.016" layer="21">5</text>
<text x="4.929" y="-0.636" size="0.254" layer="100" rot="R90">PaJa</text>
<text x="-2.2305" y="0.5974" size="1.4224" layer="25">&gt;Name</text>
<text x="-3.5015" y="-1.1486" size="1.4224" layer="27">&gt;Value</text>
</package>
<package name="D-7,5">
<description>&lt;B&gt;Dioda&lt;/B&gt; - roztec nozek 7,5 mm</description>
<wire x1="-2.541" y1="-0.889" x2="2.541" y2="-0.889" width="0.127" layer="21"/>
<wire x1="2.541" y1="-0.889" x2="2.541" y2="0" width="0.127" layer="21"/>
<wire x1="2.541" y1="0.889" x2="-2.541" y2="0.889" width="0.127" layer="21"/>
<wire x1="2.541" y1="0" x2="3.207" y2="0" width="0.127" layer="21"/>
<wire x1="2.541" y1="0" x2="2.541" y2="0.889" width="0.127" layer="21"/>
<wire x1="-3.207" y1="0" x2="-2.541" y2="0" width="0.127" layer="21"/>
<wire x1="-2.541" y1="0" x2="-2.541" y2="-0.889" width="0.127" layer="21"/>
<wire x1="-2.541" y1="0.889" x2="-2.541" y2="0" width="0.127" layer="21"/>
<wire x1="-3.247" y1="0" x2="-3.259" y2="0" width="0.127" layer="21"/>
<wire x1="3.259" y1="0" x2="3.247" y2="0" width="0.127" layer="21"/>
<circle x="-3.816" y="0" radius="0.477" width="0.127" layer="102"/>
<circle x="3.816" y="0" radius="0.477" width="0.127" layer="102"/>
<pad name="K" x="3.81" y="0" drill="0.8128" diameter="1.9304" shape="square"/>
<pad name="A" x="-3.81" y="0" drill="0.8128" diameter="1.9304"/>
<text x="-2.385" y="-0.636" size="1.27" layer="25">&gt;Name</text>
<text x="-2.3865" y="-2.385" size="1.27" layer="27">&gt;Value</text>
<text x="-2.617" y="-0.9985" size="0.254" layer="100" rot="R90">PaJa</text>
<rectangle x1="1.398" y1="-0.889" x2="1.906" y2="0.889" layer="21"/>
</package>
<package name="D-10">
<description>&lt;B&gt;Dioda&lt;/B&gt; - roztec nozek 10 mm</description>
<wire x1="-4.519" y1="0" x2="-4.531" y2="0" width="0.127" layer="21"/>
<wire x1="-3.3338" y1="1.5875" x2="3.3338" y2="1.5875" width="0.127" layer="21"/>
<wire x1="3.3338" y1="1.5875" x2="3.3338" y2="-1.5875" width="0.127" layer="21"/>
<wire x1="3.3338" y1="-1.5875" x2="-3.3338" y2="-1.5875" width="0.127" layer="21"/>
<wire x1="-3.3338" y1="-1.5875" x2="-3.3338" y2="1.5875" width="0.127" layer="21"/>
<circle x="-5.08" y="0" radius="0.5724" width="0.127" layer="102"/>
<circle x="5.08" y="0" radius="0.5724" width="0.127" layer="102"/>
<pad name="K" x="5.08" y="0" drill="1.016" diameter="2.1844" shape="square"/>
<pad name="A" x="-5.08" y="0" drill="1.016" diameter="2.1844"/>
<text x="-3.183" y="0.14" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.183" y="-1.315" size="1.27" layer="27">&gt;VALUE</text>
<text x="-3.4865" y="0.4765" size="0.254" layer="100" rot="R90">PaJa</text>
<rectangle x1="1.842" y1="-1.524" x2="2.604" y2="1.524" layer="21"/>
<rectangle x1="-4.5985" y1="-0.318" x2="-3.3265" y2="0.318" layer="21"/>
<rectangle x1="3.3265" y1="-0.318" x2="4.5985" y2="0.318" layer="21"/>
</package>
<package name="SOD-80">
<wire x1="-1.5241" y1="0.635" x2="1.2001" y2="0.635" width="0.127" layer="21"/>
<wire x1="1.2001" y1="0.635" x2="1.2001" y2="-0.635" width="0.127" layer="21"/>
<wire x1="1.2001" y1="-0.635" x2="-1.5241" y2="-0.635" width="0.127" layer="21"/>
<wire x1="-1.5241" y1="-0.635" x2="-1.5241" y2="0.635" width="0.127" layer="21"/>
<wire x1="-0.7938" y1="0" x2="0.1588" y2="0.4763" width="0.127" layer="21"/>
<wire x1="0.1588" y1="-0.4763" x2="-0.7938" y2="0" width="0.127" layer="21"/>
<wire x1="0.1588" y1="0.4763" x2="0.1588" y2="-0.4763" width="0.127" layer="21"/>
<smd name="A" x="1.905" y="0" dx="1.397" dy="1.7018" layer="1"/>
<smd name="K" x="-2.2225" y="0" dx="1.397" dy="1.7018" layer="1"/>
<text x="0.7145" y="-0.3968" size="0.254" layer="100" rot="R90">PaJa</text>
<text x="-2.3813" y="-2.0637" size="1.016" layer="27">&gt;Value</text>
<text x="-2.3812" y="1.1114" size="1.016" layer="25">&gt;Name</text>
<rectangle x1="-1.0349" y1="-0.635" x2="-0.6793" y2="0.635" layer="21"/>
<rectangle x1="-1.905" y1="-0.7938" x2="-1.4288" y2="0.7938" layer="51"/>
<rectangle x1="1.1113" y1="-0.7938" x2="1.5875" y2="0.7938" layer="51"/>
</package>
<package name="SOT-23">
<description>&lt;b&gt;SMD tranzistor&lt;/b&gt;</description>
<wire x1="1.4288" y1="-0.3175" x2="-1.4288" y2="-0.3175" width="0.127" layer="21"/>
<wire x1="-1.4288" y1="-0.3175" x2="-1.4288" y2="0.4763" width="0.127" layer="21"/>
<wire x1="-1.4288" y1="0.4763" x2="1.4288" y2="0.4763" width="0.127" layer="21"/>
<wire x1="1.4288" y1="0.4763" x2="1.4288" y2="-0.3175" width="0.127" layer="21"/>
<smd name="2" x="-0.9525" y="-0.9525" dx="0.7874" dy="0.889" layer="1"/>
<smd name="1" x="0.9525" y="-0.9525" dx="0.7874" dy="0.889" layer="1"/>
<smd name="3" x="0" y="1.27" dx="0.7874" dy="0.889" layer="1"/>
<text x="-1.2701" y="-0.0001" size="0.254" layer="100">PaJa</text>
<text x="-1.27" y="-2.5401" size="1.016" layer="27">&gt;Value</text>
<text x="0.6351" y="0.7938" size="1.016" layer="25">&gt;Name</text>
<rectangle x1="-0.1588" y1="0.4763" x2="0.1588" y2="1.1113" layer="51"/>
<rectangle x1="-1.1113" y1="-0.9525" x2="-0.7938" y2="-0.3175" layer="51"/>
<rectangle x1="0.7938" y1="-0.9525" x2="1.1113" y2="-0.3175" layer="51"/>
</package>
<package name="SM-1">
<wire x1="-0.635" y1="0" x2="0.3175" y2="0.635" width="0.127" layer="21"/>
<wire x1="0.3175" y1="0.635" x2="0.3175" y2="-0.635" width="0.127" layer="21"/>
<wire x1="0.3175" y1="-0.635" x2="-0.635" y2="0" width="0.127" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-1.905" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-1.905" y1="-1.27" x2="1.905" y2="-1.27" width="0.127" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.905" y2="1.27" width="0.127" layer="21"/>
<wire x1="1.905" y1="1.27" x2="-1.905" y2="1.27" width="0.127" layer="21"/>
<smd name="K" x="-2.8575" y="0" dx="2.54" dy="1.27" layer="1" rot="R90"/>
<smd name="A" x="2.8575" y="0" dx="2.54" dy="1.27" layer="1" rot="R90"/>
<text x="-1.3494" y="-0.4763" size="0.254" layer="100" rot="R90">PaJa</text>
<text x="-2.0638" y="-2.5401" size="1.016" layer="27">&gt;Value</text>
<text x="1.5875" y="-0.9525" size="1.016" layer="25" rot="R90">&gt;Name</text>
<rectangle x1="-2.54" y1="-1.27" x2="-1.905" y2="1.27" layer="51"/>
<rectangle x1="1.905" y1="-1.27" x2="2.54" y2="1.27" layer="51"/>
<rectangle x1="-1.27" y1="-1.27" x2="-0.635" y2="1.27" layer="21"/>
</package>
<package name="LED_10">
<description>&lt;B&gt;LED dioda&lt;/B&gt; - 10mm prumer</description>
<wire x1="-1.268" y1="-0.446" x2="-1.268" y2="-1.5875" width="0.127" layer="21"/>
<wire x1="1.272" y1="-1.5875" x2="1.272" y2="-0.446" width="0.127" layer="21"/>
<wire x1="-0.633" y1="-0.8255" x2="-0.633" y2="-1.5875" width="0.127" layer="21"/>
<wire x1="-0.633" y1="-2.3495" x2="0.637" y2="-1.5875" width="0.127" layer="21"/>
<wire x1="0.637" y1="-1.5875" x2="1.272" y2="-1.5875" width="0.127" layer="21"/>
<wire x1="0.637" y1="-1.5875" x2="-0.633" y2="-0.8255" width="0.127" layer="21"/>
<wire x1="-0.347" y1="-2.7303" x2="0.415" y2="-3.4923" width="0.127" layer="21"/>
<wire x1="0.288" y1="-2.3493" x2="1.05" y2="-3.1113" width="0.127" layer="21"/>
<wire x1="0.034" y1="-3.3653" x2="0.288" y2="-3.1113" width="0.127" layer="21"/>
<wire x1="0.288" y1="-3.1113" x2="0.415" y2="-3.4923" width="0.127" layer="21"/>
<wire x1="0.415" y1="-3.4923" x2="0.034" y2="-3.3653" width="0.127" layer="21"/>
<wire x1="1.05" y1="-3.1113" x2="0.669" y2="-2.9843" width="0.127" layer="21"/>
<wire x1="0.669" y1="-2.9843" x2="0.923" y2="-2.7303" width="0.127" layer="21"/>
<wire x1="0.923" y1="-2.7303" x2="1.05" y2="-3.1113" width="0.127" layer="21"/>
<wire x1="0.796" y1="-2.9843" x2="0.923" y2="-2.8573" width="0.127" layer="21"/>
<wire x1="0.161" y1="-3.3653" x2="0.288" y2="-3.2383" width="0.127" layer="21"/>
<wire x1="0.637" y1="-2.3495" x2="0.637" y2="-1.5875" width="0.127" layer="21"/>
<wire x1="0.637" y1="-1.5875" x2="0.637" y2="-0.8255" width="0.127" layer="21"/>
<wire x1="-1.268" y1="-1.5875" x2="-0.633" y2="-1.5875" width="0.127" layer="21"/>
<wire x1="-0.633" y1="-1.5875" x2="-0.633" y2="-2.3495" width="0.127" layer="21"/>
<wire x1="3.81" y1="-2.2225" x2="3.81" y2="2.2225" width="0.127" layer="21"/>
<wire x1="3.81" y1="-2.2225" x2="3.81" y2="2.2225" width="0.127" layer="21" curve="-299.487126"/>
<circle x="-1.272" y="0" radius="0.477" width="0.127" layer="102"/>
<circle x="1.272" y="0" radius="0.477" width="0.127" layer="102"/>
<circle x="0" y="0" radius="3.81" width="0.127" layer="51"/>
<pad name="K" x="1.27" y="0" drill="0.8128" diameter="1.9304" shape="square"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" diameter="1.9304"/>
<text x="-2.2268" y="1.4225" size="1.27" layer="25">&gt;Name</text>
<text x="5.2472" y="-2.8622" size="1.27" layer="27" rot="R90">&gt;Value</text>
<text x="0.793" y="-2.0675" size="0.254" layer="100">PaJa</text>
</package>
<package name="LED_3">
<description>&lt;B&gt;LED dioda&lt;/B&gt; - 3mm prumer</description>
<wire x1="-0.381" y1="-0.381" x2="-0.381" y2="-0.889" width="0.127" layer="21"/>
<wire x1="-0.381" y1="-1.397" x2="0.381" y2="-0.889" width="0.127" layer="21"/>
<wire x1="0.381" y1="-0.889" x2="0.889" y2="-0.889" width="0.127" layer="21"/>
<wire x1="0.381" y1="-0.889" x2="-0.381" y2="-0.381" width="0.127" layer="21"/>
<wire x1="-0.635" y1="0.508" x2="0.127" y2="1.27" width="0.127" layer="21"/>
<wire x1="0" y1="0.127" x2="0.762" y2="0.889" width="0.127" layer="21"/>
<wire x1="-0.254" y1="1.143" x2="0" y2="0.889" width="0.127" layer="21"/>
<wire x1="0" y1="0.889" x2="0.127" y2="1.27" width="0.127" layer="21"/>
<wire x1="0.127" y1="1.27" x2="-0.254" y2="1.143" width="0.127" layer="21"/>
<wire x1="0.762" y1="0.889" x2="0.381" y2="0.762" width="0.127" layer="21"/>
<wire x1="0.381" y1="0.762" x2="0.635" y2="0.508" width="0.127" layer="21"/>
<wire x1="0.635" y1="0.508" x2="0.762" y2="0.889" width="0.127" layer="21"/>
<wire x1="0.508" y1="0.762" x2="0.635" y2="0.635" width="0.127" layer="21"/>
<wire x1="-0.127" y1="1.143" x2="0" y2="1.016" width="0.127" layer="21"/>
<wire x1="0.381" y1="-1.397" x2="0.381" y2="-0.889" width="0.127" layer="21"/>
<wire x1="0.381" y1="-0.889" x2="0.381" y2="-0.381" width="0.127" layer="21"/>
<wire x1="0.889" y1="-0.889" x2="1.1685" y2="-0.5239" width="0.127" layer="21"/>
<wire x1="-0.8889" y1="-0.889" x2="-1.0921" y2="-0.5238" width="0.127" layer="21"/>
<wire x1="-0.8889" y1="-0.889" x2="-0.381" y2="-0.889" width="0.127" layer="21"/>
<wire x1="-0.381" y1="-0.889" x2="-0.381" y2="-1.397" width="0.127" layer="21"/>
<wire x1="-1.5081" y1="0.4763" x2="1.4289" y2="0.7145" width="0.127" layer="21" curve="-135.860035" cap="flat"/>
<wire x1="-1.5081" y1="-0.4763" x2="1.4288" y2="-0.7144" width="0.127" layer="21" curve="135.855325" cap="flat"/>
<wire x1="1.4288" y1="0.7144" x2="1.4288" y2="0.4763" width="0.127" layer="21"/>
<wire x1="1.4288" y1="-0.7144" x2="1.4288" y2="-0.4763" width="0.127" layer="21"/>
<wire x1="1.4288" y1="0.4763" x2="1.4288" y2="-0.4763" width="0.127" layer="51"/>
<wire x1="-1.5081" y1="0.4763" x2="-1.5081" y2="-0.4763" width="0.127" layer="51" curve="35.055137" cap="flat"/>
<circle x="-1.272" y="0" radius="0.5028" width="0.127" layer="102"/>
<circle x="1.272" y="0" radius="0.477" width="0.127" layer="102"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" diameter="1.778"/>
<pad name="K" x="1.27" y="0" drill="0.8128" diameter="1.778" shape="square"/>
<text x="-2.544" y="1.749" size="1.27" layer="25">&gt;Name</text>
<text x="-2.703" y="-3.021" size="1.27" layer="27">&gt;Value</text>
<text x="-0.318" y="0.636" size="0.254" layer="100" rot="R270">PaJa</text>
</package>
<package name="LED_5">
<description>&lt;B&gt;LED dioda&lt;/B&gt; - 5mm prumer</description>
<wire x1="-1.268" y1="-0.446" x2="-1.268" y2="-1.27" width="0.127" layer="21"/>
<wire x1="1.272" y1="-1.27" x2="1.272" y2="-0.446" width="0.127" layer="21"/>
<wire x1="-0.633" y1="-0.508" x2="-0.633" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-0.633" y1="-2.032" x2="0.637" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0.637" y1="-1.27" x2="1.272" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0.637" y1="-1.27" x2="-0.633" y2="-0.508" width="0.127" layer="21"/>
<wire x1="-0.347" y1="0.984" x2="0.415" y2="1.746" width="0.127" layer="21"/>
<wire x1="0.288" y1="0.603" x2="1.05" y2="1.365" width="0.127" layer="21"/>
<wire x1="0.034" y1="1.619" x2="0.288" y2="1.365" width="0.127" layer="21"/>
<wire x1="0.288" y1="1.365" x2="0.415" y2="1.746" width="0.127" layer="21"/>
<wire x1="0.415" y1="1.746" x2="0.034" y2="1.619" width="0.127" layer="21"/>
<wire x1="1.05" y1="1.365" x2="0.669" y2="1.238" width="0.127" layer="21"/>
<wire x1="0.669" y1="1.238" x2="0.923" y2="0.984" width="0.127" layer="21"/>
<wire x1="0.923" y1="0.984" x2="1.05" y2="1.365" width="0.127" layer="21"/>
<wire x1="0.796" y1="1.238" x2="0.923" y2="1.111" width="0.127" layer="21"/>
<wire x1="0.161" y1="1.619" x2="0.288" y2="1.492" width="0.127" layer="21"/>
<wire x1="0.637" y1="-2.032" x2="0.637" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0.637" y1="-1.27" x2="0.637" y2="-0.508" width="0.127" layer="21"/>
<wire x1="-1.268" y1="-1.27" x2="-0.633" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-0.633" y1="-1.27" x2="-0.633" y2="-2.032" width="0.127" layer="21"/>
<wire x1="2.544" y1="-1.431" x2="2.544" y2="1.431" width="0.127" layer="21" curve="-301.284493"/>
<wire x1="2.544" y1="1.431" x2="2.544" y2="-1.431" width="0.127" layer="21"/>
<circle x="-1.272" y="0" radius="0.477" width="0.127" layer="102"/>
<circle x="1.272" y="0" radius="0.477" width="0.127" layer="102"/>
<circle x="0" y="0" radius="2.5489" width="0.127" layer="51"/>
<pad name="K" x="1.27" y="0" drill="0.8128" diameter="1.778" shape="square"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" diameter="1.778"/>
<text x="3.975" y="-2.703" size="1.27" layer="25" rot="R90">&gt;Name</text>
<text x="5.7235" y="-2.7035" size="1.27" layer="27" rot="R90">&gt;Value</text>
<text x="-0.477" y="-2.385" size="0.254" layer="100">PaJa</text>
</package>
<package name="LED_5X5">
<description>&lt;B&gt;LED dioda&lt;/B&gt; - ctverec - 5mm x 5mm</description>
<wire x1="-1.27" y1="0.031" x2="-1.27" y2="-0.9515" width="0.127" layer="21"/>
<wire x1="1.27" y1="-0.9515" x2="1.27" y2="0.031" width="0.127" layer="21"/>
<wire x1="-0.635" y1="-0.1895" x2="-0.635" y2="-0.9515" width="0.127" layer="21"/>
<wire x1="-0.635" y1="-1.7135" x2="0.635" y2="-0.9515" width="0.127" layer="21"/>
<wire x1="0.635" y1="-0.9515" x2="1.27" y2="-0.9515" width="0.127" layer="21"/>
<wire x1="0.635" y1="-0.9515" x2="-0.635" y2="-0.1895" width="0.127" layer="21"/>
<wire x1="-0.349" y1="0.3485" x2="0.413" y2="1.1105" width="0.127" layer="21"/>
<wire x1="0.286" y1="-0.0325" x2="1.048" y2="0.7295" width="0.127" layer="21"/>
<wire x1="0.032" y1="0.9835" x2="0.286" y2="0.7295" width="0.127" layer="21"/>
<wire x1="0.286" y1="0.7295" x2="0.413" y2="1.1105" width="0.127" layer="21"/>
<wire x1="0.413" y1="1.1105" x2="0.032" y2="0.9835" width="0.127" layer="21"/>
<wire x1="1.048" y1="0.7295" x2="0.667" y2="0.6025" width="0.127" layer="21"/>
<wire x1="0.667" y1="0.6025" x2="0.921" y2="0.3485" width="0.127" layer="21"/>
<wire x1="0.921" y1="0.3485" x2="1.048" y2="0.7295" width="0.127" layer="21"/>
<wire x1="0.794" y1="0.6025" x2="0.921" y2="0.4755" width="0.127" layer="21"/>
<wire x1="0.159" y1="0.9835" x2="0.286" y2="0.8565" width="0.127" layer="21"/>
<wire x1="0.635" y1="-1.7135" x2="0.635" y2="-0.9515" width="0.127" layer="21"/>
<wire x1="0.635" y1="-0.9515" x2="0.635" y2="-0.1895" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-0.9515" x2="-0.635" y2="-0.9515" width="0.127" layer="21"/>
<wire x1="-0.635" y1="-0.9515" x2="-0.635" y2="-1.7135" width="0.127" layer="21"/>
<wire x1="2.5399" y1="-2.54" x2="-2.54" y2="-2.54" width="0.127" layer="21"/>
<wire x1="2.5399" y1="2.54" x2="2.5399" y2="-2.54" width="0.127" layer="21"/>
<wire x1="2.5399" y1="2.54" x2="-2.54" y2="2.54" width="0.127" layer="21"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="-2.54" width="0.127" layer="21"/>
<circle x="-1.274" y="0" radius="0.477" width="0.127" layer="102"/>
<circle x="1.27" y="0" radius="0.477" width="0.127" layer="102"/>
<pad name="K" x="1.27" y="0" drill="0.8128" diameter="1.9304" shape="square"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" diameter="1.9304"/>
<text x="-2.228" y="1.2722" size="1.016" layer="25">&gt;Name</text>
<text x="-2.387" y="-3.655" size="1.016" layer="27">&gt;Value</text>
<text x="1.27" y="-2.3795" size="0.254" layer="100">PaJa</text>
</package>
<package name="LED_8">
<description>&lt;B&gt;LED dioda&lt;/B&gt; - 8mm prumer</description>
<wire x1="-1.268" y1="-0.446" x2="-1.268" y2="-1.5875" width="0.127" layer="21"/>
<wire x1="1.272" y1="-1.5875" x2="1.272" y2="-0.446" width="0.127" layer="21"/>
<wire x1="-0.633" y1="-0.8255" x2="-0.633" y2="-1.5875" width="0.127" layer="21"/>
<wire x1="-0.633" y1="-2.3495" x2="0.637" y2="-1.5875" width="0.127" layer="21"/>
<wire x1="0.637" y1="-1.5875" x2="1.272" y2="-1.5875" width="0.127" layer="21"/>
<wire x1="0.637" y1="-1.5875" x2="-0.633" y2="-0.8255" width="0.127" layer="21"/>
<wire x1="-0.347" y1="-2.7303" x2="0.415" y2="-3.4923" width="0.127" layer="21"/>
<wire x1="0.288" y1="-2.3493" x2="1.05" y2="-3.1113" width="0.127" layer="21"/>
<wire x1="0.034" y1="-3.3653" x2="0.288" y2="-3.1113" width="0.127" layer="21"/>
<wire x1="0.288" y1="-3.1113" x2="0.415" y2="-3.4923" width="0.127" layer="21"/>
<wire x1="0.415" y1="-3.4923" x2="0.034" y2="-3.3653" width="0.127" layer="21"/>
<wire x1="1.05" y1="-3.1113" x2="0.669" y2="-2.9843" width="0.127" layer="21"/>
<wire x1="0.669" y1="-2.9843" x2="0.923" y2="-2.7303" width="0.127" layer="21"/>
<wire x1="0.923" y1="-2.7303" x2="1.05" y2="-3.1113" width="0.127" layer="21"/>
<wire x1="0.796" y1="-2.9843" x2="0.923" y2="-2.8573" width="0.127" layer="22"/>
<wire x1="0.161" y1="-3.3653" x2="0.288" y2="-3.2383" width="0.127" layer="21"/>
<wire x1="0.637" y1="-2.3495" x2="0.637" y2="-1.5875" width="0.127" layer="21"/>
<wire x1="0.637" y1="-1.5875" x2="0.637" y2="-0.8255" width="0.127" layer="21"/>
<wire x1="-1.268" y1="-1.5875" x2="-0.633" y2="-1.5875" width="0.127" layer="21"/>
<wire x1="-0.633" y1="-1.5875" x2="-0.633" y2="-2.3495" width="0.127" layer="21"/>
<wire x1="3.81" y1="-2.2225" x2="3.81" y2="2.2225" width="0.127" layer="21"/>
<wire x1="3.81" y1="-2.2225" x2="3.81" y2="2.2225" width="0.127" layer="21" curve="-299.487126"/>
<circle x="-1.272" y="0" radius="0.477" width="0.127" layer="102"/>
<circle x="1.272" y="0" radius="0.477" width="0.127" layer="102"/>
<circle x="0" y="0" radius="3.81" width="0.127" layer="51"/>
<pad name="K" x="1.27" y="0" drill="0.8128" diameter="1.9304" shape="square"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" diameter="1.9304"/>
<text x="-2.2268" y="1.4225" size="1.27" layer="25">&gt;Name</text>
<text x="5.2472" y="-2.386" size="1.27" layer="27" rot="R90">&gt;Value</text>
<text x="0.793" y="-2.0675" size="0.254" layer="100">PaJa</text>
</package>
<package name="LED_TROJ">
<description>&lt;B&gt;LED dioda&lt;/B&gt; - trojuhelnik</description>
<wire x1="0.637" y1="0.764" x2="0.637" y2="0.002" width="0.127" layer="21"/>
<wire x1="1.907" y1="0.002" x2="2.542" y2="0.002" width="0.127" layer="21"/>
<wire x1="0.822" y1="-1.143" x2="0.06" y2="-1.905" width="0.127" layer="21"/>
<wire x1="0.187" y1="-0.762" x2="-0.575" y2="-1.524" width="0.127" layer="21"/>
<wire x1="0.441" y1="-1.778" x2="0.187" y2="-1.524" width="0.127" layer="21"/>
<wire x1="0.187" y1="-1.524" x2="0.06" y2="-1.905" width="0.127" layer="21"/>
<wire x1="0.06" y1="-1.905" x2="0.441" y2="-1.778" width="0.127" layer="21"/>
<wire x1="-0.575" y1="-1.524" x2="-0.194" y2="-1.397" width="0.127" layer="21"/>
<wire x1="-0.194" y1="-1.397" x2="-0.448" y2="-1.143" width="0.127" layer="21"/>
<wire x1="-0.448" y1="-1.143" x2="-0.575" y2="-1.524" width="0.127" layer="21"/>
<wire x1="-0.321" y1="-1.397" x2="-0.448" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0.314" y1="-1.778" x2="0.187" y2="-1.651" width="0.127" layer="21"/>
<wire x1="1.907" y1="-0.76" x2="1.907" y2="0.002" width="0.127" layer="21"/>
<wire x1="1.907" y1="0.002" x2="1.907" y2="0.764" width="0.127" layer="21"/>
<wire x1="0.002" y1="0.002" x2="0.637" y2="0.002" width="0.127" layer="21"/>
<wire x1="0.637" y1="0.002" x2="0.637" y2="-0.76" width="0.127" layer="21"/>
<wire x1="1.903" y1="0.76" x2="0.633" y2="-0.002" width="0.127" layer="21"/>
<wire x1="0.633" y1="-0.002" x2="1.903" y2="-0.764" width="0.127" layer="21"/>
<wire x1="-0.9525" y1="2.6988" x2="-0.9525" y2="-2.6988" width="0.127" layer="21"/>
<wire x1="-0.9525" y1="-2.6988" x2="3.81" y2="0" width="0.127" layer="21"/>
<wire x1="3.81" y1="0" x2="-0.9525" y2="2.6988" width="0.127" layer="21"/>
<circle x="-0.002" y="0" radius="0.477" width="0.127" layer="102"/>
<circle x="2.542" y="0" radius="0.477" width="0.127" layer="102"/>
<pad name="K" x="0" y="0" drill="0.8128" diameter="1.9304" shape="square"/>
<pad name="A" x="2.54" y="0" drill="0.8128" diameter="1.9304"/>
<text x="-1.2737" y="-2.226" size="1.016" layer="25" rot="R90">&gt;Name</text>
<text x="-2.3868" y="-2.544" size="1.016" layer="27" rot="R90">&gt;Value</text>
<text x="-0.7964" y="1.113" size="0.254" layer="100">PaJa</text>
</package>
<package name="LED2,5X5">
<description>&lt;B&gt;LED dioda&lt;/B&gt; - obdelnik - 5mm x 2,5mm</description>
<wire x1="0.639" y1="0.287" x2="0.639" y2="-0.475" width="0.127" layer="21"/>
<wire x1="0.639" y1="-1.237" x2="1.909" y2="-0.475" width="0.127" layer="21"/>
<wire x1="1.909" y1="-0.475" x2="2.544" y2="-0.475" width="0.127" layer="21"/>
<wire x1="1.909" y1="-0.475" x2="0.639" y2="0.287" width="0.127" layer="21"/>
<wire x1="1.561" y1="0.348" x2="2.323" y2="1.11" width="0.127" layer="21"/>
<wire x1="2.196" y1="-0.033" x2="2.958" y2="0.729" width="0.127" layer="21"/>
<wire x1="1.942" y1="0.983" x2="2.196" y2="0.729" width="0.127" layer="21"/>
<wire x1="2.196" y1="0.729" x2="2.323" y2="1.11" width="0.127" layer="21"/>
<wire x1="2.323" y1="1.11" x2="1.942" y2="0.983" width="0.127" layer="21"/>
<wire x1="2.958" y1="0.729" x2="2.577" y2="0.602" width="0.127" layer="21"/>
<wire x1="2.577" y1="0.602" x2="2.831" y2="0.348" width="0.127" layer="21"/>
<wire x1="2.831" y1="0.348" x2="2.958" y2="0.729" width="0.127" layer="21"/>
<wire x1="2.704" y1="0.602" x2="2.831" y2="0.475" width="0.127" layer="21"/>
<wire x1="2.069" y1="0.983" x2="2.196" y2="0.856" width="0.127" layer="21"/>
<wire x1="1.909" y1="-1.237" x2="1.909" y2="-0.475" width="0.127" layer="21"/>
<wire x1="1.909" y1="-0.475" x2="1.909" y2="0.287" width="0.127" layer="21"/>
<wire x1="0.004" y1="0.002" x2="0" y2="0.002" width="0.127" layer="21"/>
<wire x1="0" y1="0.002" x2="0" y2="-0.477" width="0.127" layer="21"/>
<wire x1="0" y1="-0.477" x2="0.639" y2="-0.475" width="0.127" layer="21"/>
<wire x1="0.639" y1="-0.475" x2="0.639" y2="-1.237" width="0.127" layer="21"/>
<wire x1="2.544" y1="-0.477" x2="2.544" y2="0" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="1.27" width="0.127" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="3.81" y2="1.27" width="0.127" layer="21"/>
<wire x1="3.81" y1="1.27" x2="3.81" y2="-1.27" width="0.127" layer="21"/>
<wire x1="3.81" y1="-1.27" x2="-1.27" y2="-1.27" width="0.127" layer="21"/>
<circle x="0" y="0" radius="0.477" width="0.127" layer="102"/>
<circle x="2.544" y="0" radius="0.477" width="0.127" layer="102"/>
<pad name="K" x="0" y="0" drill="0.8128" diameter="1.9304" shape="square"/>
<pad name="A" x="2.54" y="0" drill="0.8128" diameter="1.9304"/>
<text x="-1.113" y="1.431" size="1.016" layer="25">&gt;Name</text>
<text x="-1.431" y="-2.385" size="1.016" layer="27">&gt;Value</text>
<text x="1.431" y="0.159" size="0.254" layer="100" rot="R90">PaJa</text>
</package>
<package name="P1206">
<wire x1="1.7463" y1="-0.7938" x2="1.7463" y2="-0.3176" width="0.127" layer="21"/>
<wire x1="1.1113" y1="0.7937" x2="-1.4287" y2="0.7937" width="0.127" layer="21"/>
<wire x1="-1.4287" y1="-0.7938" x2="-0.7937" y2="-0.7938" width="0.127" layer="21"/>
<wire x1="-0.7937" y1="-0.7938" x2="1.1113" y2="-0.7938" width="0.127" layer="21"/>
<wire x1="1.1113" y1="-0.7938" x2="1.7463" y2="-0.7938" width="0.127" layer="21"/>
<wire x1="1.1113" y1="-0.7938" x2="1.1113" y2="0.7937" width="0.127" layer="21"/>
<wire x1="-1.4288" y1="-0.3175" x2="-1.4288" y2="0.3175" width="0.127" layer="21" curve="180"/>
<wire x1="1.7462" y1="-0.3175" x2="1.7462" y2="0.3175" width="0.127" layer="21" curve="-180" cap="flat"/>
<wire x1="1.7462" y1="0.3175" x2="1.7462" y2="0.7938" width="0.127" layer="21"/>
<wire x1="1.1112" y1="0.7938" x2="1.7462" y2="0.7938" width="0.127" layer="21"/>
<wire x1="-1.4288" y1="-0.7937" x2="-1.4288" y2="-0.3175" width="0.127" layer="21"/>
<wire x1="-1.4288" y1="0.3175" x2="-1.4288" y2="0.7938" width="0.127" layer="21"/>
<wire x1="-0.1588" y1="0.4763" x2="-0.1588" y2="0" width="0.127" layer="21"/>
<wire x1="-0.1588" y1="0" x2="-0.1588" y2="-0.4763" width="0.127" layer="21"/>
<wire x1="-0.1588" y1="0" x2="0.4763" y2="0.4763" width="0.127" layer="21"/>
<wire x1="0.4763" y1="0.4763" x2="0.4761" y2="0" width="0.127" layer="21"/>
<wire x1="0.4761" y1="0" x2="0.4763" y2="-0.4762" width="0.127" layer="21"/>
<wire x1="0.4763" y1="-0.4762" x2="-0.1588" y2="0" width="0.127" layer="21"/>
<wire x1="-0.1588" y1="0" x2="-0.4763" y2="0" width="0.127" layer="21"/>
<wire x1="0.4761" y1="0" x2="0.7937" y2="0" width="0.127" layer="21"/>
<wire x1="1.7463" y1="0.3175" x2="1.7463" y2="-0.3175" width="0.127" layer="51" curve="180" cap="flat"/>
<wire x1="-0.7938" y1="-0.7938" x2="-1.4288" y2="-0.7938" width="0.127" layer="51"/>
<wire x1="-1.4288" y1="0.3175" x2="-1.4288" y2="-0.3175" width="0.127" layer="51" curve="-180" cap="flat"/>
<wire x1="-0.7938" y1="-0.7938" x2="-0.7938" y2="-0.3175" width="0.127" layer="51"/>
<wire x1="-0.7937" y1="-0.7938" x2="-0.7937" y2="0.7937" width="0.127" layer="21"/>
<circle x="-1.0319" y="-0.5556" radius="0.2024" width="0.127" layer="51"/>
<smd name="K" x="-1.5875" y="0" dx="0.9144" dy="1.778" layer="1"/>
<smd name="A" x="1.9051" y="-0.0001" dx="0.9144" dy="1.778" layer="1"/>
<text x="0.9922" y="-0.4762" size="0.254" layer="100" rot="R90">PaJa</text>
<text x="-1.1113" y="1.1113" size="1.016" layer="25">&gt;Name</text>
<text x="-1.1113" y="-2.0638" size="1.016" layer="27">&gt;Value</text>
<rectangle x1="0" y1="-0.1588" x2="0.3175" y2="0.1588" layer="51"/>
<polygon width="0.127" layer="51">
<vertex x="1.7463" y="0.3175"/>
<vertex x="1.7463" y="0.7938"/>
<vertex x="1.1113" y="0.7938"/>
<vertex x="1.1113" y="-0.7938"/>
<vertex x="1.7463" y="-0.7938"/>
<vertex x="1.7463" y="-0.3175"/>
<vertex x="1.5875" y="-0.3175"/>
<vertex x="1.4288" y="-0.1588"/>
<vertex x="1.4288" y="0.1588"/>
<vertex x="1.5875" y="0.3175"/>
</polygon>
<polygon width="0.127" layer="51">
<vertex x="-1.4288" y="-0.7938"/>
<vertex x="-1.4288" y="-0.3174"/>
<vertex x="-1.4288" y="-0.3175"/>
<vertex x="-1.27" y="-0.3175"/>
<vertex x="-1.1113" y="-0.1588"/>
<vertex x="-1.1113" y="0.1588"/>
<vertex x="-1.27" y="0.3175"/>
<vertex x="-1.4288" y="0.3175"/>
<vertex x="-1.4288" y="0.7938"/>
<vertex x="-0.7938" y="0.7938"/>
<vertex x="-0.7938" y="0.635"/>
<vertex x="-0.7937" y="0.635"/>
<vertex x="-0.7937" y="-0.3175"/>
<vertex x="-1.27" y="-0.3175"/>
<vertex x="-1.27" y="-0.7938"/>
</polygon>
</package>
<package name="C-2,5">
<circle x="-1.272" y="0" radius="0.477" width="0.127" layer="102"/>
<circle x="1.272" y="0" radius="0.477" width="0.127" layer="102"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" diameter="1.27" shape="long" rot="R90"/>
<pad name="2" x="1.27" y="0" drill="0.8128" diameter="1.27" shape="long" rot="R90"/>
<text x="-2.226" y="1.272" size="1.27" layer="25">&gt;Name</text>
<text x="-2.544" y="-2.544" size="1.27" layer="27">&gt;Value</text>
<text x="0.159" y="0.318" size="0.254" layer="100" rot="R90">PaJa</text>
<rectangle x1="-0.635" y1="-1.27" x2="-0.254" y2="1.27" layer="21"/>
<rectangle x1="0.254" y1="-1.27" x2="0.635" y2="1.27" layer="21"/>
<rectangle x1="-0.7938" y1="-0.1588" x2="-0.635" y2="0.1588" layer="21"/>
<rectangle x1="0.635" y1="-0.1588" x2="0.7938" y2="0.1588" layer="21"/>
</package>
<package name="C-5">
<circle x="-2.544" y="0" radius="0.477" width="0.127" layer="102"/>
<circle x="2.544" y="0" radius="0.477" width="0.127" layer="102"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" diameter="1.9304" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" diameter="1.9304" shape="octagon"/>
<text x="0.795" y="0.954" size="1.016" layer="25">&gt;Name</text>
<text x="0.795" y="-1.9085" size="1.016" layer="27">&gt;Value</text>
<text x="0.159" y="0.3182" size="0.254" layer="100" rot="R90">PaJa</text>
<rectangle x1="-0.7143" y1="-1.27" x2="-0.238" y2="1.27" layer="21"/>
<rectangle x1="0.2381" y1="-1.27" x2="0.7144" y2="1.27" layer="21"/>
<rectangle x1="-2.0638" y1="-0.1588" x2="-0.635" y2="0.1588" layer="21"/>
<rectangle x1="0.635" y1="-0.1588" x2="2.0638" y2="0.1588" layer="21"/>
</package>
<package name="C-7,5">
<circle x="-3.814" y="0" radius="0.477" width="0.127" layer="102"/>
<circle x="3.814" y="0" radius="0.477" width="0.127" layer="102"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" diameter="1.9304" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.8128" diameter="1.9304" shape="octagon"/>
<text x="0.795" y="0.954" size="1.016" layer="25">&gt;Name</text>
<text x="0.795" y="-1.9085" size="1.016" layer="27">&gt;Value</text>
<text x="0.159" y="0.477" size="0.254" layer="100" rot="R90">PaJa</text>
<rectangle x1="-0.7155" y1="-1.431" x2="-0.2385" y2="1.431" layer="21"/>
<rectangle x1="0.2385" y1="-1.431" x2="0.7155" y2="1.431" layer="21"/>
<rectangle x1="-3.3338" y1="-0.1588" x2="-0.635" y2="0.1588" layer="21"/>
<rectangle x1="0.635" y1="-0.1588" x2="3.3338" y2="0.1588" layer="21"/>
</package>
<package name="C-10">
<wire x1="-6.35" y1="2.6035" x2="-6.35" y2="-2.6035" width="0.127" layer="21"/>
<wire x1="-6.35" y1="-2.6035" x2="6.35" y2="-2.6035" width="0.127" layer="21"/>
<wire x1="6.35" y1="-2.6035" x2="6.35" y2="2.6035" width="0.127" layer="21"/>
<wire x1="6.35" y1="2.6035" x2="-6.35" y2="2.6035" width="0.127" layer="21"/>
<circle x="-5.08" y="0" radius="0.4763" width="0.127" layer="102"/>
<circle x="5.08" y="0" radius="0.4762" width="0.127" layer="102"/>
<pad name="1" x="-5.08" y="0" drill="0.8128" diameter="2.1844" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="0.8128" diameter="2.1844" shape="octagon"/>
<text x="0.159" y="0.3182" size="0.254" layer="100" rot="R90">PaJa</text>
<text x="-4.0444" y="1.1525" size="1.27" layer="25">&gt;Name</text>
<text x="-4.3507" y="-2.4225" size="1.27" layer="27">&gt;Value</text>
<rectangle x1="-0.7144" y1="-1.27" x2="-0.2381" y2="1.27" layer="21"/>
<rectangle x1="0.238" y1="-1.27" x2="0.7143" y2="1.27" layer="21"/>
<rectangle x1="-4.6038" y1="-0.1588" x2="-0.635" y2="0.1588" layer="21"/>
<rectangle x1="0.635" y1="-0.1588" x2="4.6038" y2="0.1588" layer="21"/>
</package>
<package name="1206">
<description>&lt;B&gt;SMD&lt;/B&gt; - velikost 1206</description>
<wire x1="-1.0541" y1="0.7938" x2="1.0541" y2="0.7938" width="0.127" layer="21"/>
<wire x1="-1.0541" y1="-0.7938" x2="1.0541" y2="-0.7938" width="0.127" layer="21"/>
<wire x1="1.0541" y1="0.7938" x2="1.0541" y2="-0.7938" width="0.127" layer="21"/>
<wire x1="-1.0541" y1="0.7938" x2="-1.0541" y2="-0.7938" width="0.127" layer="21"/>
<smd name="1" x="-1.5875" y="0" dx="1.143" dy="1.7018" layer="1"/>
<smd name="2" x="1.5875" y="0" dx="1.143" dy="1.7018" layer="1"/>
<text x="-0.3175" y="-1.1906" size="0.254" layer="100">PaJa</text>
<text x="-0.7938" y="-0.4763" size="1.016" layer="25">&gt;Name</text>
<text x="-0.7938" y="0.9525" size="1.016" layer="27">&gt;Value</text>
<rectangle x1="-1.4541" y1="-0.7874" x2="-0.9461" y2="0.7874" layer="51"/>
<rectangle x1="0.9461" y1="-0.7874" x2="1.4541" y2="0.7874" layer="51"/>
</package>
<package name="0805">
<description>&lt;B&gt;SMD&lt;/B&gt; - velikost 0805</description>
<wire x1="0.5557" y1="0.5557" x2="-0.5557" y2="0.5557" width="0.127" layer="21"/>
<wire x1="-0.5557" y1="0.5557" x2="-0.5557" y2="-0.5556" width="0.127" layer="21"/>
<wire x1="-0.5557" y1="-0.5556" x2="0.5557" y2="-0.5556" width="0.127" layer="21"/>
<wire x1="0.5557" y1="-0.5556" x2="0.5557" y2="0.5557" width="0.127" layer="21"/>
<smd name="1" x="-0.9525" y="0" dx="1.016" dy="1.4224" layer="1"/>
<smd name="2" x="0.9525" y="0" dx="1.016" dy="1.4224" layer="1"/>
<text x="-1.397" y="-1.6351" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<text x="-1.3177" y="0.8413" size="0.8128" layer="25" ratio="10">&gt;NAME</text>
<text x="0.4763" y="-0.4763" size="0.254" layer="100" rot="R90">PaJa</text>
<rectangle x1="0.4064" y1="-0.6096" x2="0.9144" y2="0.6096" layer="51"/>
<rectangle x1="-0.9144" y1="-0.6096" x2="-0.4064" y2="0.6096" layer="51"/>
</package>
<package name="R-5">
<description>&lt;B&gt;Odpor&lt;/B&gt; - vel. 0204 - 0,4W - miniaturni</description>
<wire x1="-1.778" y1="0.635" x2="-1.524" y2="0.889" width="0.127" layer="21" curve="-90" cap="flat"/>
<wire x1="-1.778" y1="-0.635" x2="-1.524" y2="-0.889" width="0.127" layer="21" curve="90" cap="flat"/>
<wire x1="1.524" y1="-0.889" x2="1.778" y2="-0.635" width="0.127" layer="21" curve="90" cap="flat"/>
<wire x1="1.524" y1="0.889" x2="1.778" y2="0.6388" width="0.127" layer="21" curve="-89.149199"/>
<wire x1="1.778" y1="0.6388" x2="1.778" y2="0.635" width="0.127" layer="21" curve="-0.857165"/>
<wire x1="-1.524" y1="0.889" x2="-1.27" y2="0.889" width="0.127" layer="21"/>
<wire x1="-1.143" y1="0.762" x2="-1.27" y2="0.889" width="0.127" layer="21"/>
<wire x1="-1.524" y1="-0.889" x2="-1.27" y2="-0.889" width="0.127" layer="21"/>
<wire x1="-1.143" y1="-0.762" x2="-1.27" y2="-0.889" width="0.127" layer="21"/>
<wire x1="1.143" y1="0.762" x2="1.27" y2="0.889" width="0.127" layer="21"/>
<wire x1="1.143" y1="0.762" x2="-1.143" y2="0.762" width="0.127" layer="21"/>
<wire x1="1.143" y1="-0.762" x2="1.27" y2="-0.889" width="0.127" layer="21"/>
<wire x1="1.143" y1="-0.762" x2="-1.143" y2="-0.762" width="0.127" layer="21"/>
<wire x1="1.524" y1="0.889" x2="1.27" y2="0.889" width="0.127" layer="21"/>
<wire x1="1.524" y1="-0.889" x2="1.27" y2="-0.889" width="0.127" layer="21"/>
<wire x1="1.778" y1="0.6388" x2="1.778" y2="-0.6332" width="0.127" layer="21"/>
<wire x1="-1.7787" y1="0.6274" x2="-1.7787" y2="-0.6446" width="0.127" layer="21"/>
<circle x="-2.54" y="0" radius="0.4763" width="0.127" layer="102"/>
<circle x="2.54" y="0" radius="0.4762" width="0.127" layer="102"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" diameter="1.778" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" diameter="1.778" shape="octagon"/>
<text x="-1.59" y="-0.477" size="1.016" layer="25">&gt;Name</text>
<text x="-2.544" y="-1.908" size="1.016" layer="27">&gt;Value</text>
<text x="-0.4797" y="0.8527" size="0.254" layer="100">PaJa</text>
<rectangle x1="-2.1022" y1="-0.306" x2="-1.8124" y2="0.3068" layer="21"/>
<rectangle x1="1.8124" y1="-0.3068" x2="2.1022" y2="0.306" layer="21"/>
</package>
<package name="R-10">
<description>&lt;B&gt;Odpor&lt;/B&gt; - vel. 0207 - 0,6W - vetsi roztec</description>
<wire x1="-2.572" y1="1.016" x2="-2.699" y2="1.143" width="0.127" layer="21"/>
<wire x1="-2.572" y1="-1.016" x2="-2.699" y2="-1.143" width="0.127" layer="21"/>
<wire x1="2.572" y1="1.016" x2="2.699" y2="1.143" width="0.127" layer="21"/>
<wire x1="2.572" y1="1.016" x2="-2.572" y2="1.016" width="0.127" layer="21"/>
<wire x1="2.572" y1="-1.016" x2="2.699" y2="-1.143" width="0.127" layer="21"/>
<wire x1="2.572" y1="-1.016" x2="-2.572" y2="-1.016" width="0.127" layer="21"/>
<wire x1="3.08" y1="1.139" x2="2.699" y2="1.139" width="0.127" layer="21"/>
<wire x1="3.08" y1="-1.147" x2="2.699" y2="-1.147" width="0.127" layer="21"/>
<wire x1="-3.334" y1="0.893" x2="-3.08" y2="1.147" width="0.127" layer="21" curve="-90" cap="flat"/>
<wire x1="-3.334" y1="-0.885" x2="-3.08" y2="-1.139" width="0.127" layer="21" curve="90" cap="flat"/>
<wire x1="-3.08" y1="-1.139" x2="-2.699" y2="-1.139" width="0.127" layer="21"/>
<wire x1="-3.08" y1="1.147" x2="-2.699" y2="1.147" width="0.127" layer="21"/>
<wire x1="-3.3321" y1="0.8823" x2="-3.3321" y2="-0.8667" width="0.127" layer="21"/>
<wire x1="3.08" y1="-1.147" x2="3.334" y2="-0.893" width="0.127" layer="21" curve="90" cap="flat"/>
<wire x1="3.08" y1="1.139" x2="3.334" y2="0.885" width="0.127" layer="21" curve="-90" cap="flat"/>
<wire x1="3.3321" y1="-0.8823" x2="3.3321" y2="0.8667" width="0.127" layer="21"/>
<circle x="-5.08" y="0" radius="0.4763" width="0.127" layer="102"/>
<circle x="5.08" y="0" radius="0.4762" width="0.127" layer="102"/>
<pad name="1" x="-5.08" y="0" drill="0.8128" diameter="1.9304" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="0.8128" diameter="1.9304" shape="octagon"/>
<text x="-3.1152" y="-0.6276" size="1.27" layer="25">&gt;Name</text>
<text x="-0.3178" y="-0.6358" size="1.27" layer="27">&gt;Value</text>
<text x="2.3342" y="-0.9351" size="0.254" layer="100">PaJa</text>
<rectangle x1="-4.611" y1="-0.318" x2="-3.339" y2="0.318" layer="21"/>
<rectangle x1="3.339" y1="-0.318" x2="4.611" y2="0.318" layer="21"/>
</package>
<package name="R-12,7">
<description>&lt;B&gt;Odpor&lt;/B&gt; - roztec nozek 12,7mm</description>
<wire x1="3.7648" y1="1.2546" x2="3.8918" y2="1.3816" width="0.127" layer="21"/>
<wire x1="3.7648" y1="-1.2546" x2="3.8918" y2="-1.3816" width="0.127" layer="21"/>
<wire x1="4.2728" y1="1.3776" x2="3.8918" y2="1.3776" width="0.127" layer="21"/>
<wire x1="4.2728" y1="-1.3856" x2="3.8918" y2="-1.3856" width="0.127" layer="21"/>
<wire x1="4.2728" y1="-1.3856" x2="4.5268" y2="-1.1316" width="0.127" layer="21" curve="90" cap="flat"/>
<wire x1="4.2728" y1="1.3776" x2="4.5268" y2="1.1236" width="0.127" layer="21" curve="-90" cap="flat"/>
<wire x1="4.5249" y1="-1.1209" x2="4.5249" y2="1.1053" width="0.127" layer="21"/>
<wire x1="-3.7649" y1="1.2547" x2="-3.8919" y2="1.3817" width="0.127" layer="21"/>
<wire x1="-3.7649" y1="-1.2546" x2="-3.8919" y2="-1.3816" width="0.127" layer="21"/>
<wire x1="3.7648" y1="1.2546" x2="-3.7649" y2="1.2547" width="0.127" layer="21"/>
<wire x1="3.7648" y1="-1.2546" x2="-3.7649" y2="-1.2546" width="0.127" layer="21"/>
<wire x1="-4.5269" y1="1.1316" x2="-4.2729" y2="1.3856" width="0.127" layer="21" curve="-90" cap="flat"/>
<wire x1="-4.5269" y1="-1.1236" x2="-4.2729" y2="-1.3776" width="0.127" layer="21" curve="90" cap="flat"/>
<wire x1="-4.2729" y1="-1.3776" x2="-3.8919" y2="-1.3776" width="0.127" layer="21"/>
<wire x1="-4.2729" y1="1.3856" x2="-3.8919" y2="1.3856" width="0.127" layer="21"/>
<wire x1="-4.525" y1="1.1209" x2="-4.525" y2="-1.1054" width="0.127" layer="21"/>
<circle x="-6.35" y="0" radius="0.4763" width="0.127" layer="102"/>
<circle x="6.35" y="0" radius="0.4762" width="0.127" layer="102"/>
<pad name="1" x="-6.35" y="0" drill="0.8128" diameter="2.1844" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.8128" diameter="2.1844" shape="octagon"/>
<text x="-0.4813" y="-0.7958" size="1.4224" layer="27">&gt;Value</text>
<text x="-4.2905" y="-0.7144" size="1.4224" layer="25">&gt;Name</text>
<text x="3.5712" y="-1.1046" size="0.254" layer="100">PaJa</text>
<rectangle x1="-5.8738" y1="-0.3175" x2="-4.5244" y2="0.3175" layer="21"/>
<rectangle x1="4.5244" y1="-0.3175" x2="5.8738" y2="0.3175" layer="21"/>
</package>
<package name="R-7,5">
<description>&lt;B&gt;Odpor&lt;/B&gt; - vel. 0207 - 0,6W</description>
<wire x1="-3.175" y1="0.893" x2="-2.921" y2="1.147" width="0.127" layer="21" curve="-90" cap="flat"/>
<wire x1="-3.175" y1="-0.885" x2="-2.921" y2="-1.139" width="0.127" layer="21" curve="90" cap="flat"/>
<wire x1="2.413" y1="-1.012" x2="2.54" y2="-1.139" width="0.127" layer="21"/>
<wire x1="2.413" y1="1.02" x2="2.54" y2="1.147" width="0.127" layer="21"/>
<wire x1="-2.413" y1="-1.012" x2="-2.54" y2="-1.139" width="0.127" layer="21"/>
<wire x1="-2.413" y1="-1.012" x2="2.413" y2="-1.012" width="0.127" layer="21"/>
<wire x1="-2.413" y1="1.02" x2="-2.54" y2="1.147" width="0.127" layer="21"/>
<wire x1="-2.413" y1="1.02" x2="2.413" y2="1.02" width="0.127" layer="21"/>
<wire x1="-2.921" y1="-1.139" x2="-2.54" y2="-1.139" width="0.127" layer="21"/>
<wire x1="-2.921" y1="1.147" x2="-2.54" y2="1.147" width="0.127" layer="21"/>
<wire x1="-3.1731" y1="0.8823" x2="-3.1731" y2="-0.8667" width="0.127" layer="21"/>
<wire x1="2.921" y1="-1.147" x2="3.175" y2="-0.893" width="0.127" layer="21" curve="90" cap="flat"/>
<wire x1="2.921" y1="1.139" x2="3.175" y2="0.885" width="0.127" layer="21" curve="-90" cap="flat"/>
<wire x1="2.921" y1="1.139" x2="2.54" y2="1.139" width="0.127" layer="21"/>
<wire x1="2.921" y1="-1.147" x2="2.54" y2="-1.147" width="0.127" layer="21"/>
<wire x1="3.1731" y1="-0.8823" x2="3.1731" y2="0.8667" width="0.127" layer="21"/>
<circle x="-3.81" y="0" radius="0.4763" width="0.127" layer="102"/>
<circle x="3.81" y="0" radius="0.4762" width="0.127" layer="102"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" diameter="1.9304" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.8128" diameter="1.9304" shape="octagon"/>
<text x="-0.3178" y="-0.477" size="1.016" layer="27">&gt;Value</text>
<text x="-2.7033" y="-0.477" size="1.016" layer="25">&gt;Name</text>
<text x="2.1354" y="-0.8658" size="0.254" layer="100">PaJa</text>
<rectangle x1="-3.4323" y1="-0.3053" x2="-3.1758" y2="0.3061" layer="21"/>
<rectangle x1="3.1759" y1="-0.3061" x2="3.4324" y2="0.3053" layer="21"/>
</package>
<package name="R-_2W">
<description>&lt;B&gt;Odpor&lt;/B&gt; - vel. 0414 - 2W</description>
<wire x1="4.3998" y1="1.8896" x2="4.5268" y2="2.0166" width="0.127" layer="21"/>
<wire x1="4.3998" y1="-1.8896" x2="4.5268" y2="-2.0166" width="0.127" layer="21"/>
<wire x1="4.9078" y1="2.0126" x2="4.5268" y2="2.0126" width="0.127" layer="21"/>
<wire x1="4.9078" y1="-2.0206" x2="4.5268" y2="-2.0206" width="0.127" layer="21"/>
<wire x1="4.9078" y1="-2.0206" x2="5.1618" y2="-1.7666" width="0.127" layer="21" curve="90" cap="flat"/>
<wire x1="4.9078" y1="2.0126" x2="5.1618" y2="1.7586" width="0.127" layer="21" curve="-90" cap="flat"/>
<wire x1="5.1599" y1="-1.7559" x2="5.1599" y2="1.7403" width="0.127" layer="21"/>
<wire x1="-4.3999" y1="1.8897" x2="-4.5269" y2="2.0167" width="0.127" layer="21"/>
<wire x1="-4.3999" y1="-1.8896" x2="-4.5269" y2="-2.0166" width="0.127" layer="21"/>
<wire x1="4.3998" y1="1.8896" x2="-4.3999" y2="1.8897" width="0.127" layer="21"/>
<wire x1="4.3998" y1="-1.8896" x2="-4.3999" y2="-1.8896" width="0.127" layer="21"/>
<wire x1="-5.1619" y1="1.7666" x2="-4.9079" y2="2.0206" width="0.127" layer="21" curve="-90" cap="flat"/>
<wire x1="-5.1619" y1="-1.7586" x2="-4.9079" y2="-2.0126" width="0.127" layer="21" curve="90" cap="flat"/>
<wire x1="-4.9079" y1="-2.0126" x2="-4.5269" y2="-2.0126" width="0.127" layer="21"/>
<wire x1="-4.9079" y1="2.0206" x2="-4.5269" y2="2.0206" width="0.127" layer="21"/>
<wire x1="-5.16" y1="1.7559" x2="-5.16" y2="-1.7404" width="0.127" layer="21"/>
<circle x="-6.35" y="0" radius="0.4763" width="0.127" layer="102"/>
<circle x="6.35" y="0" radius="0.4762" width="0.127" layer="102"/>
<pad name="1" x="-6.35" y="0" drill="0.8128" diameter="2.54" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.8128" diameter="2.54" shape="octagon"/>
<text x="-0.4813" y="-0.7958" size="1.6764" layer="27">&gt;Value</text>
<text x="-4.9255" y="-0.7144" size="1.6764" layer="25">&gt;Name</text>
<text x="4.1268" y="-1.7396" size="0.254" layer="100">PaJa</text>
<text x="-4.7625" y="-1.5875" size="0.6096" layer="21">2W</text>
<rectangle x1="-5.953" y1="-0.3175" x2="-5.1593" y2="0.3175" layer="21"/>
<rectangle x1="5.1594" y1="-0.3175" x2="5.9531" y2="0.3175" layer="21"/>
</package>
<package name="R-_10W">
<description>&lt;B&gt;Odpor&lt;/B&gt; - 10W - dratovy</description>
<wire x1="-24.13" y1="5.3975" x2="-24.13" y2="-5.3975" width="0.127" layer="21"/>
<wire x1="-24.13" y1="-5.3975" x2="24.13" y2="-5.3975" width="0.127" layer="21"/>
<wire x1="24.13" y1="-5.3975" x2="24.13" y2="5.3975" width="0.127" layer="21"/>
<wire x1="24.13" y1="5.3975" x2="-24.13" y2="5.3975" width="0.127" layer="21"/>
<circle x="-25.7175" y="0" radius="0.5724" width="0.127" layer="102"/>
<circle x="25.7175" y="0" radius="0.5723" width="0.127" layer="102"/>
<pad name="1" x="-25.7175" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<pad name="2" x="25.7175" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<text x="-4.9375" y="-3.08" size="1.9304" layer="27">&gt;VALUE</text>
<text x="-4.9375" y="1.2225" size="1.9304" layer="25">&gt;NAME</text>
<text x="22.86" y="-5.08" size="0.254" layer="100">PaJa</text>
<text x="-23.1775" y="-3.81" size="1.27" layer="21">10W</text>
<rectangle x1="-25.2412" y1="-0.635" x2="-24.1299" y2="0.635" layer="21"/>
<rectangle x1="-25.5587" y1="0.4763" x2="-25.2412" y2="0.635" layer="21"/>
<rectangle x1="-25.5587" y1="-0.6349" x2="-25.2412" y2="-0.4762" layer="21"/>
<rectangle x1="24.13" y1="-0.635" x2="25.2413" y2="0.635" layer="21"/>
<rectangle x1="25.2413" y1="-0.6349" x2="25.5588" y2="-0.4762" layer="21"/>
<rectangle x1="25.2413" y1="0.4763" x2="25.5588" y2="0.635" layer="21"/>
</package>
<package name="R-_20W">
<description>&lt;B&gt;Odpor&lt;/B&gt; - 20W - dratovy</description>
<wire x1="-30.1625" y1="6.985" x2="-30.1625" y2="-6.985" width="0.127" layer="21"/>
<wire x1="-30.1625" y1="-6.985" x2="30.1625" y2="-6.985" width="0.127" layer="21"/>
<wire x1="30.1625" y1="-6.985" x2="30.1625" y2="6.985" width="0.127" layer="21"/>
<wire x1="30.1625" y1="6.985" x2="-30.1625" y2="6.985" width="0.127" layer="21"/>
<circle x="-31.75" y="0" radius="0.7099" width="0.127" layer="102"/>
<circle x="31.75" y="0" radius="0.7099" width="0.127" layer="102"/>
<pad name="1" x="-31.75" y="0" drill="1.27" diameter="3.2" shape="octagon"/>
<pad name="2" x="31.75" y="0" drill="1.27" diameter="3.2" shape="octagon"/>
<text x="-4.9375" y="-4.6675" size="2.1844" layer="27">&gt;VALUE</text>
<text x="-4.9375" y="1.2225" size="2.1844" layer="25">&gt;NAME</text>
<text x="28.8925" y="-6.6675" size="0.254" layer="100">PaJa</text>
<text x="-29.21" y="-5.715" size="1.27" layer="21">20W</text>
<rectangle x1="30.1625" y1="-0.635" x2="31.115" y2="0.635" layer="21"/>
<rectangle x1="-31.1149" y1="-0.635" x2="-30.1624" y2="0.635" layer="21"/>
<rectangle x1="-31.2738" y1="0.4763" x2="-31.115" y2="0.635" layer="21"/>
<rectangle x1="-31.2738" y1="-0.6349" x2="-31.115" y2="-0.4762" layer="21"/>
<rectangle x1="31.115" y1="-0.6349" x2="31.2738" y2="-0.4762" layer="21"/>
<rectangle x1="31.115" y1="0.4763" x2="31.2738" y2="0.635" layer="21"/>
</package>
<package name="R-_5W">
<description>&lt;B&gt;Odpor&lt;/B&gt; - 5W - keramicky</description>
<wire x1="-11.1125" y1="5.08" x2="-11.1125" y2="-5.08" width="0.127" layer="21"/>
<wire x1="-11.1125" y1="-5.08" x2="11.1125" y2="-5.08" width="0.127" layer="21"/>
<wire x1="11.1125" y1="-5.08" x2="11.1125" y2="5.08" width="0.127" layer="21"/>
<wire x1="11.1125" y1="5.08" x2="-11.1125" y2="5.08" width="0.127" layer="21"/>
<circle x="-12.7" y="0" radius="0.5724" width="0.127" layer="102"/>
<circle x="12.7" y="0" radius="0.5723" width="0.127" layer="102"/>
<pad name="1" x="-12.7" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<pad name="2" x="12.7" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<text x="-4.9375" y="-3.08" size="1.9304" layer="27">&gt;VALUE</text>
<text x="-4.9375" y="1.2225" size="1.9304" layer="25">&gt;NAME</text>
<text x="-10.16" y="-3.81" size="1.27" layer="21">5W</text>
<text x="9.8425" y="-4.7625" size="0.254" layer="100">PaJa</text>
<rectangle x1="-12.2237" y1="-0.635" x2="-11.1124" y2="0.635" layer="21"/>
<rectangle x1="-12.5412" y1="0.4763" x2="-12.2237" y2="0.635" layer="21"/>
<rectangle x1="-12.5412" y1="-0.6349" x2="-12.2237" y2="-0.4762" layer="21"/>
<rectangle x1="11.1125" y1="-0.635" x2="12.2238" y2="0.635" layer="21"/>
<rectangle x1="12.2238" y1="-0.6349" x2="12.5413" y2="-0.4762" layer="21"/>
<rectangle x1="12.2238" y1="0.4763" x2="12.5413" y2="0.635" layer="21"/>
</package>
<package name="R-STOJ">
<description>&lt;B&gt;Odpor&lt;/B&gt; - vel. 0207 - 0,6W - nastojato</description>
<circle x="-1.272" y="0" radius="1.2818" width="0.127" layer="21"/>
<circle x="-1.27" y="0" radius="0.4763" width="0.127" layer="102"/>
<circle x="1.27" y="0" radius="0.4762" width="0.127" layer="102"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" diameter="1.27" shape="long" rot="R90"/>
<pad name="2" x="1.27" y="0" drill="0.8128" diameter="1.27" shape="long" rot="R90"/>
<text x="-2.389" y="1.433" size="1.016" layer="25">&gt;Name</text>
<text x="-2.544" y="-2.385" size="1.016" layer="27">&gt;Value</text>
<text x="0.636" y="-1.272" size="0.254" layer="100" rot="R90">PaJa</text>
<rectangle x1="-0.795" y1="-0.318" x2="0.795" y2="0.318" layer="21"/>
</package>
<package name="TO-220">
<wire x1="-2.8575" y1="0.635" x2="-2.8575" y2="1.5875" width="0.127" layer="21"/>
<wire x1="-2.8575" y1="1.5875" x2="-3.175" y2="1.5875" width="0.127" layer="21"/>
<wire x1="-3.175" y1="1.5875" x2="-3.175" y2="5.08" width="0.127" layer="21"/>
<wire x1="-2.2225" y1="0.635" x2="-2.2225" y2="1.5875" width="0.127" layer="21"/>
<wire x1="-2.2225" y1="1.5875" x2="-1.905" y2="1.5875" width="0.127" layer="21"/>
<wire x1="-1.905" y1="1.5875" x2="-1.905" y2="5.08" width="0.127" layer="21"/>
<wire x1="-0.3175" y1="0.635" x2="-0.3175" y2="1.5875" width="0.127" layer="21"/>
<wire x1="-0.3175" y1="1.5875" x2="-0.635" y2="1.5875" width="0.127" layer="21"/>
<wire x1="-0.635" y1="1.5875" x2="-0.635" y2="5.08" width="0.127" layer="21"/>
<wire x1="0.635" y1="5.08" x2="0.635" y2="1.5875" width="0.127" layer="21"/>
<wire x1="0.635" y1="1.5875" x2="0.3175" y2="1.5875" width="0.127" layer="21"/>
<wire x1="0.3175" y1="1.5875" x2="0.3175" y2="0.635" width="0.127" layer="21"/>
<wire x1="1.905" y1="5.08" x2="1.905" y2="1.5875" width="0.127" layer="21"/>
<wire x1="1.905" y1="1.5875" x2="2.2225" y2="1.5875" width="0.127" layer="21"/>
<wire x1="2.2225" y1="1.5875" x2="2.2225" y2="0.635" width="0.127" layer="21"/>
<wire x1="2.8575" y1="0.635" x2="2.8575" y2="1.5875" width="0.127" layer="21"/>
<wire x1="2.8575" y1="1.5875" x2="3.175" y2="1.5875" width="0.127" layer="21"/>
<wire x1="3.175" y1="1.5875" x2="3.175" y2="5.08" width="0.127" layer="21"/>
<wire x1="-3.81" y1="20.6375" x2="3.81" y2="20.6375" width="0.254" layer="21"/>
<wire x1="-3.81" y1="20.6375" x2="-5.08" y2="18.415" width="0.254" layer="21"/>
<wire x1="3.81" y1="20.6375" x2="5.08" y2="18.415" width="0.254" layer="21"/>
<wire x1="-5.08" y1="18.415" x2="-5.08" y2="14.605" width="0.254" layer="21"/>
<wire x1="-5.08" y1="5.08" x2="5.08" y2="5.08" width="0.254" layer="21"/>
<wire x1="5.08" y1="18.415" x2="5.08" y2="14.605" width="0.254" layer="21"/>
<wire x1="5.08" y1="14.605" x2="5.08" y2="5.08" width="0.254" layer="21"/>
<wire x1="-5.08" y1="14.605" x2="5.08" y2="14.605" width="0.127" layer="21"/>
<wire x1="-4.7625" y1="13.6525" x2="4.7625" y2="13.6525" width="0.127" layer="51"/>
<wire x1="4.7625" y1="13.6525" x2="5.08" y2="14.605" width="0.127" layer="51"/>
<wire x1="-4.7625" y1="13.6525" x2="-5.08" y2="14.605" width="0.127" layer="51"/>
<wire x1="-4.7625" y1="7.3025" x2="-4.7625" y2="9.8425" width="0.127" layer="51" curve="180"/>
<wire x1="-4.7625" y1="13.6525" x2="-4.7625" y2="9.8425" width="0.127" layer="51"/>
<wire x1="-4.7625" y1="7.3025" x2="-4.7625" y2="5.3975" width="0.127" layer="51"/>
<wire x1="-4.7625" y1="5.3975" x2="4.7625" y2="5.3975" width="0.127" layer="51"/>
<wire x1="4.7625" y1="5.3975" x2="4.7625" y2="7.3025" width="0.127" layer="51"/>
<wire x1="4.7625" y1="13.6525" x2="4.7625" y2="9.8425" width="0.127" layer="51"/>
<wire x1="4.7625" y1="5.3975" x2="5.08" y2="5.08" width="0.127" layer="51"/>
<wire x1="-4.7625" y1="5.3975" x2="-5.08" y2="5.08" width="0.127" layer="51"/>
<wire x1="4.7625" y1="7.3025" x2="4.7625" y2="9.8425" width="0.127" layer="51" curve="-180"/>
<wire x1="-4.7625" y1="9.8425" x2="-5.08" y2="9.525" width="0.127" layer="51"/>
<wire x1="-4.7625" y1="9.525" x2="-4.7625" y2="7.62" width="0.127" layer="51" curve="-180"/>
<wire x1="-4.7625" y1="9.525" x2="-5.08" y2="9.525" width="0.127" layer="51"/>
<wire x1="-5.08" y1="7.62" x2="-4.7625" y2="7.62" width="0.127" layer="51"/>
<wire x1="-5.08" y1="7.62" x2="-4.7625" y2="7.3025" width="0.127" layer="51"/>
<wire x1="-5.08" y1="14.605" x2="-5.08" y2="5.08" width="0.254" layer="21"/>
<wire x1="4.7625" y1="9.8425" x2="5.08" y2="9.525" width="0.127" layer="51"/>
<wire x1="5.08" y1="7.62" x2="4.7625" y2="7.3025" width="0.127" layer="51"/>
<wire x1="5.08" y1="7.62" x2="4.7625" y2="7.62" width="0.127" layer="51"/>
<wire x1="4.7625" y1="9.525" x2="5.08" y2="9.525" width="0.127" layer="51"/>
<wire x1="4.7625" y1="9.525" x2="4.7625" y2="7.62" width="0.127" layer="51" curve="180"/>
<circle x="-2.54" y="0" radius="0.5724" width="0.127" layer="102"/>
<circle x="0" y="0" radius="0.5724" width="0.127" layer="102"/>
<circle x="2.54" y="0" radius="0.5723" width="0.127" layer="102"/>
<circle x="0" y="17.78" radius="1.7097" width="0.127" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="1.016" diameter="1.6" shape="long" rot="R90"/>
<pad name="2" x="0" y="0" drill="1.016" diameter="1.6" shape="long" rot="R90"/>
<pad name="3" x="2.54" y="0" drill="1.016" diameter="1.6" shape="long" rot="R90"/>
<text x="-3.6525" y="10.815" size="1.6764" layer="25">&gt;NAME</text>
<text x="-3.971" y="5.7365" size="1.6764" layer="27">&gt;VALUE</text>
<text x="-0.489" y="14.1335" size="0.254" layer="100">PaJa</text>
<text x="-2.54" y="14.9225" size="1.016" layer="101">TO-220</text>
</package>
<package name="TO-220S">
<wire x1="-5.0006" y1="1.3494" x2="-4.9213" y2="-1.905" width="0.127" layer="21"/>
<wire x1="5.0006" y1="1.3494" x2="4.9212" y2="-1.905" width="0.127" layer="21"/>
<wire x1="-4.9213" y1="-1.905" x2="4.9213" y2="-1.905" width="0.127" layer="21"/>
<circle x="-2.54" y="0" radius="0.5724" width="0.127" layer="102"/>
<circle x="0" y="0" radius="0.5724" width="0.127" layer="102"/>
<circle x="2.54" y="0" radius="0.5724" width="0.127" layer="102"/>
<pad name="1" x="-2.54" y="0" drill="1.016" diameter="1.6" shape="long" rot="R90"/>
<pad name="2" x="0" y="0" drill="1.016" diameter="1.6" shape="long" rot="R90"/>
<pad name="3" x="2.54" y="0" drill="1.016" diameter="1.6" shape="long" rot="R90"/>
<text x="-4.929" y="2.703" size="1.27" layer="25">&gt;Name</text>
<text x="-0.795" y="2.703" size="1.27" layer="27">&gt;Value</text>
<text x="-4.77" y="-1.749" size="0.254" layer="100">PaJa</text>
<rectangle x1="-5.084" y1="1.27" x2="5.076" y2="2.54" layer="21"/>
</package>
<package name="ARK500/2">
<wire x1="-5.08" y1="-3.556" x2="-5.08" y2="-2.159" width="0.127" layer="21"/>
<wire x1="5.08" y1="3.937" x2="-5.08" y2="3.937" width="0.127" layer="21"/>
<wire x1="5.08" y1="3.937" x2="5.08" y2="3.088" width="0.127" layer="21"/>
<wire x1="5.08" y1="2.199" x2="5.08" y2="2.159" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-3.556" x2="5.08" y2="-3.556" width="0.127" layer="21"/>
<wire x1="-5.08" y1="2.159" x2="-5.08" y2="3.937" width="0.127" layer="21"/>
<wire x1="-5.08" y1="2.159" x2="5.08" y2="2.159" width="0.127" layer="21"/>
<wire x1="5.08" y1="2.159" x2="5.08" y2="-2.159" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-2.159" x2="5.08" y2="-2.159" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-2.159" x2="-5.08" y2="2.159" width="0.127" layer="21"/>
<wire x1="5.08" y1="-2.159" x2="5.08" y2="-3.556" width="0.127" layer="21"/>
<wire x1="1.4986" y1="-1.397" x2="3.9116" y2="1.016" width="0.127" layer="51"/>
<wire x1="1.1176" y1="-1.016" x2="3.5306" y2="1.397" width="0.127" layer="51"/>
<wire x1="-3.9116" y1="-1.016" x2="-1.4986" y2="1.397" width="0.127" layer="51"/>
<wire x1="-3.5306" y1="-1.397" x2="-1.1176" y2="1.016" width="0.127" layer="51"/>
<wire x1="1.4986" y1="-1.016" x2="3.5306" y2="1.016" width="0.6096" layer="51"/>
<wire x1="-3.5306" y1="-1.016" x2="-1.4986" y2="1.016" width="0.6096" layer="51"/>
<wire x1="1.1176" y1="-1.016" x2="1.4986" y2="-1.397" width="0.127" layer="51"/>
<wire x1="3.5306" y1="1.397" x2="3.9116" y2="1.016" width="0.127" layer="51"/>
<wire x1="-3.9116" y1="-1.016" x2="-3.5306" y2="-1.397" width="0.127" layer="51"/>
<wire x1="-1.4986" y1="1.397" x2="-1.1176" y2="1.016" width="0.127" layer="51"/>
<wire x1="5.08" y1="2.199" x2="5.588" y2="2.072" width="0.127" layer="21"/>
<wire x1="5.588" y1="2.072" x2="5.588" y2="3.215" width="0.127" layer="21"/>
<wire x1="5.588" y1="3.215" x2="5.08" y2="3.088" width="0.127" layer="21"/>
<wire x1="5.08" y1="3.088" x2="5.08" y2="2.199" width="0.127" layer="21"/>
<circle x="2.5146" y="3.048" radius="0.508" width="0.127" layer="21"/>
<circle x="-2.5146" y="3.048" radius="0.508" width="0.127" layer="21"/>
<circle x="2.5146" y="0" radius="1.778" width="0.127" layer="51"/>
<circle x="-2.5146" y="0" radius="1.778" width="0.127" layer="51"/>
<circle x="-2.54" y="0" radius="0.7099" width="0.127" layer="102"/>
<circle x="2.54" y="0" radius="0.7099" width="0.127" layer="102"/>
<pad name="1" x="-2.54" y="0" drill="1.27" diameter="3.2" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.27" diameter="3.2" shape="octagon"/>
<text x="-2.695" y="-5.398" size="1.4224" layer="25">&gt;NAME</text>
<text x="-4.7093" y="-3.33" size="1.016" layer="27">&gt;VALUE</text>
<text x="-4.127" y="2.54" size="0.9906" layer="21" ratio="12">1</text>
<text x="0.794" y="2.54" size="0.9906" layer="21" ratio="12">2</text>
<text x="-0.477" y="2.2866" size="0.254" layer="100">PaJa</text>
<text x="2.0377" y="-3.4199" size="0.6096" layer="101">dratu</text>
<text x="1.7395" y="-2.7752" size="0.6096" layer="101">strana</text>
<rectangle x1="-0.381" y1="-1.905" x2="0.381" y2="1.905" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="IO_555">
<wire x1="8.89" y1="-10.16" x2="-6.35" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-6.35" y1="-10.16" x2="-6.35" y2="10.16" width="0.254" layer="94"/>
<wire x1="-6.35" y1="10.16" x2="8.89" y2="10.16" width="0.254" layer="94"/>
<wire x1="8.89" y1="10.16" x2="8.89" y2="0" width="0.254" layer="94"/>
<wire x1="8.89" y1="0" x2="8.89" y2="-10.16" width="0.254" layer="94"/>
<wire x1="8.89" y1="0" x2="10.1632" y2="0" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="0" x2="-6.3468" y2="0" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="-7.62" x2="-6.3468" y2="-7.62" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="7.62" x2="-6.3468" y2="7.62" width="0.1524" layer="94"/>
<text x="-1.905" y="2.2225" size="1.9304" layer="95">&gt;Name</text>
<text x="-1.905" y="-4.445" size="1.9304" layer="96">&gt;Value</text>
<text x="8.5813" y="-9.8712" size="0.254" layer="100" rot="R90">PaJa</text>
<text x="-6.0325" y="6.0325" size="1.4224" layer="95">DIS.</text>
<text x="-6.0325" y="-0.9525" size="1.4224" layer="95">TH.</text>
<text x="-6.0325" y="-7.62" size="1.4224" layer="95">TR.</text>
<text x="-3.81" y="-9.8425" size="1.4224" layer="95">CO.</text>
<text x="3.4925" y="-9.8425" size="1.4224" layer="95">GND</text>
<text x="4.7625" y="-0.9525" size="1.4224" layer="95">OUT</text>
<text x="3.175" y="8.255" size="1.4224" layer="95">RES</text>
<text x="-4.445" y="8.255" size="1.4224" layer="95">VCC</text>
<pin name="VCC" x="-2.54" y="12.7" visible="pad" length="short" direction="pwr" rot="R270"/>
<pin name="GND" x="5.08" y="-12.7" visible="pad" length="short" direction="pwr" rot="R90"/>
<pin name="OUT" x="12.7" y="0" visible="pad" length="short" direction="out" rot="R180"/>
<pin name="DIS." x="-10.16" y="7.62" visible="pad" length="short" direction="in"/>
<pin name="TH." x="-10.16" y="0" visible="pad" length="short" direction="in"/>
<pin name="TR." x="-10.16" y="-7.62" visible="pad" length="short" direction="in"/>
<pin name="CO." x="-2.54" y="-12.7" visible="pad" length="short" direction="in" rot="R90"/>
<pin name="RES" x="5.08" y="12.7" visible="pad" length="short" direction="in" rot="R270"/>
</symbol>
<symbol name="D-ZENERK">
<wire x1="-0.1588" y1="1.27" x2="-0.1588" y2="0" width="0.254" layer="94"/>
<wire x1="-0.1588" y1="0" x2="-0.1588" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-2.3813" y1="-1.27" x2="-2.3813" y2="1.27" width="0.254" layer="94"/>
<wire x1="-2.3813" y1="1.27" x2="-0.1588" y2="0" width="0.254" layer="94"/>
<wire x1="-0.1588" y1="0" x2="-2.3813" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-0.1588" y1="1.27" x2="-0.635" y2="1.27" width="0.254" layer="94"/>
<text x="-2.2227" y="0.4759" size="0.254" layer="100" rot="R270">PaJa</text>
<text x="-2.5401" y="-1.905" size="1.6764" layer="96" rot="MR180">&gt;Value</text>
<text x="-2.54" y="1.905" size="1.6764" layer="95">&gt;Part</text>
<pin name="A" x="-5.08" y="0" visible="off" length="short" direction="pas"/>
<pin name="K" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="D">
<wire x1="2.3812" y1="1.27" x2="2.3812" y2="0" width="0.254" layer="94"/>
<wire x1="2.3812" y1="0" x2="2.3812" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0.1587" y1="-1.27" x2="0.1587" y2="1.27" width="0.254" layer="94"/>
<wire x1="0.1587" y1="1.27" x2="2.3812" y2="0" width="0.254" layer="94"/>
<wire x1="2.3812" y1="0" x2="0.1587" y2="-1.27" width="0.254" layer="94"/>
<text x="-0.0001" y="-1.905" size="1.6764" layer="96" rot="MR180">&gt;Value</text>
<text x="0.3173" y="0.4759" size="0.254" layer="100" rot="R270">PaJa</text>
<text x="0" y="1.905" size="1.6764" layer="95">&gt;Part</text>
<pin name="A" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="K" x="5.08" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="LED">
<wire x1="0.95" y1="1.9085" x2="1.745" y2="2.7035" width="0.155" layer="94"/>
<wire x1="1.745" y1="2.7035" x2="1.268" y2="2.7035" width="0.155" layer="94"/>
<wire x1="1.745" y1="2.7035" x2="1.745" y2="2.2265" width="0.155" layer="94"/>
<wire x1="2.699" y1="2.2265" x2="2.222" y2="2.2265" width="0.155" layer="94"/>
<wire x1="2.699" y1="2.2265" x2="2.699" y2="1.7495" width="0.155" layer="94"/>
<wire x1="1.904" y1="1.4315" x2="2.699" y2="2.2265" width="0.155" layer="94"/>
<wire x1="2.3812" y1="1.27" x2="2.3812" y2="0" width="0.254" layer="94"/>
<wire x1="2.3812" y1="0" x2="2.3812" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0.1587" y1="-1.27" x2="0.1587" y2="1.27" width="0.254" layer="94"/>
<wire x1="0.1587" y1="1.27" x2="2.3812" y2="0" width="0.254" layer="94"/>
<wire x1="2.3812" y1="0" x2="0.1587" y2="-1.27" width="0.254" layer="94"/>
<text x="2.8575" y="-2.2224" size="1.6764" layer="96">&gt;Value</text>
<text x="0.3173" y="0.4759" size="0.254" layer="100" rot="R270">PaJa</text>
<text x="3.0163" y="0.4762" size="1.6764" layer="95">&gt;Part</text>
<pin name="A" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="K" x="5.08" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="C">
<wire x1="-2.54" y1="0" x2="-2.0638" y2="0" width="0.152" layer="94"/>
<wire x1="-0.4763" y1="0" x2="0" y2="0" width="0.152" layer="94"/>
<text x="-1.111" y="-0.479" size="0.254" layer="100" rot="R90">PaJa</text>
<text x="0.3175" y="0.635" size="1.6764" layer="95">&gt;Name</text>
<text x="0.3175" y="-0.635" size="1.6764" layer="96" rot="MR180">&gt;Value</text>
<rectangle x1="-2.2225" y1="-1.905" x2="-1.5875" y2="1.905" layer="94"/>
<rectangle x1="-0.9525" y1="-1.905" x2="-0.3175" y2="1.905" layer="94"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="2.54" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="R">
<wire x1="-2.54" y1="1.0319" x2="2.54" y2="1.0319" width="0.254" layer="94"/>
<wire x1="2.54" y1="1.0319" x2="2.54" y2="-1.0319" width="0.254" layer="94"/>
<wire x1="2.54" y1="-1.0319" x2="-2.54" y2="-1.0319" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-1.0319" x2="-2.54" y2="1.0319" width="0.254" layer="94"/>
<text x="2.3815" y="-0.476" size="0.254" layer="100" rot="R90">PaJa</text>
<text x="-2.2225" y="1.5875" size="1.6764" layer="95">&gt;Name</text>
<text x="-2.2225" y="-1.5875" size="1.6764" layer="96" rot="MR180">&gt;Value</text>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="GND">
<wire x1="-1.7463" y1="-0.0001" x2="1.7463" y2="-0.0001" width="0.6096" layer="94"/>
<text x="-1.1113" y="0.3175" size="0.254" layer="100">PaJa</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="N-MOSFET">
<wire x1="-1.1176" y1="2.413" x2="-1.1176" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-1.1176" y1="-2.54" x2="-5.08" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="1.905" x2="0.5334" y2="1.905" width="0.1524" layer="94"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="1.905" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="0.508" y1="-1.905" x2="2.54" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0.508" y1="0" x2="1.778" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="1.778" y1="-0.508" x2="1.778" y2="0.508" width="0.1524" layer="94"/>
<wire x1="1.778" y1="0.508" x2="0.508" y2="0" width="0.1524" layer="94"/>
<wire x1="1.651" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="1.651" y1="0.254" x2="0.762" y2="0" width="0.3048" layer="94"/>
<wire x1="0.762" y1="0" x2="1.651" y2="-0.254" width="0.3048" layer="94"/>
<wire x1="1.651" y1="-0.254" x2="1.651" y2="0" width="0.3048" layer="94"/>
<wire x1="1.651" y1="0" x2="1.397" y2="0" width="0.3048" layer="94"/>
<circle x="0.3175" y="0" radius="3.5921" width="0.254" layer="94"/>
<circle x="2.54" y="-1.905" radius="0.127" width="0.4064" layer="94"/>
<text x="3.4925" y="2.54" size="1.9304" layer="95">&gt;Name</text>
<text x="3.4925" y="-4.445" size="1.9304" layer="96">&gt;Value</text>
<text x="1.431" y="-1.429" size="0.254" layer="100" rot="R180">PaJa</text>
<text x="0.635" y="2.2225" size="0.8128" layer="93">D</text>
<text x="0.635" y="-3.175" size="0.8128" layer="93">S</text>
<text x="-2.54" y="-1.905" size="0.8128" layer="93">G</text>
<rectangle x1="-0.254" y1="-2.54" x2="0.508" y2="-1.27" layer="94"/>
<rectangle x1="-0.254" y1="1.27" x2="0.508" y2="2.54" layer="94"/>
<rectangle x1="-0.254" y1="-0.889" x2="0.508" y2="0.889" layer="94"/>
<pin name="G" x="-5.08" y="-2.54" visible="off" length="point" direction="pas"/>
<pin name="D" x="2.54" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="S" x="2.54" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
</symbol>
<symbol name="ARK500/2">
<wire x1="-1.268" y1="1.906" x2="-1.268" y2="-4.446" width="0.254" layer="94"/>
<wire x1="-1.268" y1="-4.446" x2="3.178" y2="-4.446" width="0.254" layer="94"/>
<wire x1="3.178" y1="-4.446" x2="3.178" y2="1.906" width="0.254" layer="94"/>
<wire x1="3.178" y1="1.906" x2="-1.268" y2="1.906" width="0.254" layer="94"/>
<wire x1="-0.317" y1="-1.268" x2="2.219" y2="1.268" width="0.127" layer="94"/>
<wire x1="-0.317" y1="-3.804" x2="2.219" y2="-1.268" width="0.127" layer="94"/>
<circle x="0.951" y="0" radius="0.951" width="0.254" layer="94"/>
<circle x="0.951" y="-2.536" radius="0.951" width="0.254" layer="94"/>
<text x="-1.8865" y="2.539" size="1.778" layer="95">&gt;Name</text>
<text x="-1.8865" y="-6.979" size="1.778" layer="96">&gt;Value</text>
<text x="1.897" y="-4.1245" size="0.254" layer="100">PaJa</text>
<pin name="K1" x="-5.08" y="0" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="K2" x="-5.08" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="555" prefix="IO">
<description>IO - Casovac 555</description>
<gates>
<gate name="IO" symbol="IO_555" x="-35.56" y="33.02"/>
</gates>
<devices>
<device name="" package="DIL8">
<connects>
<connect gate="IO" pin="CO." pad="5"/>
<connect gate="IO" pin="DIS." pad="7"/>
<connect gate="IO" pin="GND" pad="1"/>
<connect gate="IO" pin="OUT" pad="3"/>
<connect gate="IO" pin="RES" pad="4"/>
<connect gate="IO" pin="TH." pad="6"/>
<connect gate="IO" pin="TR." pad="2"/>
<connect gate="IO" pin="VCC" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="D-ZENEROVA" prefix="DZ">
<description>&lt;B&gt;Zenerova dioda&lt;/B&gt;</description>
<gates>
<gate name="DZ" symbol="D-ZENERK" x="0" y="0" swaplevel="1"/>
</gates>
<devices>
<device name="_0,5W_BZX83V" package="D-7,5">
<connects>
<connect gate="DZ" pin="A" pad="A"/>
<connect gate="DZ" pin="K" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_1,3W_BZX85V" package="D-7,5">
<connects>
<connect gate="DZ" pin="A" pad="A"/>
<connect gate="DZ" pin="K" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_2W_BZY" package="D-10">
<connects>
<connect gate="DZ" pin="A" pad="A"/>
<connect gate="DZ" pin="K" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0,5W_BZV55C_SMD" package="SOD-80">
<connects>
<connect gate="DZ" pin="A" pad="A"/>
<connect gate="DZ" pin="K" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0,35W_BZX84C_SMD" package="SOT-23">
<connects>
<connect gate="DZ" pin="A" pad="2"/>
<connect gate="DZ" pin="K" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="1N4148" prefix="D">
<description>&lt;B&gt;Univerzalni dioda&lt;/B&gt; - 0,15A, 75V</description>
<gates>
<gate name="D" symbol="D" x="-43.18" y="38.1" swaplevel="1"/>
</gates>
<devices>
<device name="_7,5" package="D-7,5">
<connects>
<connect gate="D" pin="A" pad="A"/>
<connect gate="D" pin="K" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_SMD_SOD80" package="SOD-80">
<connects>
<connect gate="D" pin="A" pad="A"/>
<connect gate="D" pin="K" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="1N4007" prefix="D">
<description>&lt;B&gt;Usmernovaci dioda&lt;/B&gt; - 1A, 1000V</description>
<gates>
<gate name="D" symbol="D" x="-40.64" y="35.56" swaplevel="1"/>
</gates>
<devices>
<device name="_10" package="D-10">
<connects>
<connect gate="D" pin="A" pad="A"/>
<connect gate="D" pin="K" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_SMD_SM-1" package="SM-1">
<connects>
<connect gate="D" pin="A" pad="A"/>
<connect gate="D" pin="K" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LED" prefix="D">
<description>&lt;B&gt;LED&lt;/B&gt; - jednobarevna</description>
<gates>
<gate name="D" symbol="LED" x="-2.54" y="0" swaplevel="1"/>
</gates>
<devices>
<device name="_10" package="LED_10">
<connects>
<connect gate="D" pin="A" pad="A"/>
<connect gate="D" pin="K" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_3" package="LED_3">
<connects>
<connect gate="D" pin="A" pad="A"/>
<connect gate="D" pin="K" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_5" package="LED_5">
<connects>
<connect gate="D" pin="A" pad="A"/>
<connect gate="D" pin="K" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_5X5" package="LED_5X5">
<connects>
<connect gate="D" pin="A" pad="A"/>
<connect gate="D" pin="K" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_8" package="LED_8">
<connects>
<connect gate="D" pin="A" pad="A"/>
<connect gate="D" pin="K" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_TROJ" package="LED_TROJ">
<connects>
<connect gate="D" pin="A" pad="A"/>
<connect gate="D" pin="K" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_2,5X5" package="LED2,5X5">
<connects>
<connect gate="D" pin="A" pad="A"/>
<connect gate="D" pin="K" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_SMD_1206" package="P1206">
<connects>
<connect gate="D" pin="A" pad="A"/>
<connect gate="D" pin="K" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="C-KERAMIK" prefix="C" uservalue="yes">
<description>&lt;b&gt;Kondenzator - keramicky&lt;/b&gt;</description>
<gates>
<gate name="C" symbol="C" x="0" y="0" swaplevel="1"/>
</gates>
<devices>
<device name="_2,5" package="C-2,5">
<connects>
<connect gate="C" pin="1" pad="1"/>
<connect gate="C" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_5" package="C-5">
<connects>
<connect gate="C" pin="1" pad="1"/>
<connect gate="C" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_7,5" package="C-7,5">
<connects>
<connect gate="C" pin="1" pad="1"/>
<connect gate="C" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_10" package="C-10">
<connects>
<connect gate="C" pin="1" pad="1"/>
<connect gate="C" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_SMD_1206" package="1206">
<connects>
<connect gate="C" pin="1" pad="1"/>
<connect gate="C" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_SMD_0805" package="0805">
<connects>
<connect gate="C" pin="1" pad="1"/>
<connect gate="C" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="R" prefix="R" uservalue="yes">
<description>&lt;b&gt;Rezistor&lt;/b&gt;</description>
<gates>
<gate name="R" symbol="R" x="0" y="0" swaplevel="1"/>
</gates>
<devices>
<device name="_5" package="R-5">
<connects>
<connect gate="R" pin="1" pad="1"/>
<connect gate="R" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_10" package="R-10">
<connects>
<connect gate="R" pin="1" pad="1"/>
<connect gate="R" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_12,7" package="R-12,7">
<connects>
<connect gate="R" pin="1" pad="1"/>
<connect gate="R" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_7,5" package="R-7,5">
<connects>
<connect gate="R" pin="1" pad="1"/>
<connect gate="R" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_SMD_1206" package="1206">
<connects>
<connect gate="R" pin="1" pad="1"/>
<connect gate="R" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="__2W" package="R-_2W">
<connects>
<connect gate="R" pin="1" pad="1"/>
<connect gate="R" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="__10W" package="R-_10W">
<connects>
<connect gate="R" pin="1" pad="1"/>
<connect gate="R" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="__20W" package="R-_20W">
<connects>
<connect gate="R" pin="1" pad="1"/>
<connect gate="R" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="__5W" package="R-_5W">
<connects>
<connect gate="R" pin="1" pad="1"/>
<connect gate="R" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_STOJ" package="R-STOJ">
<connects>
<connect gate="R" pin="1" pad="1"/>
<connect gate="R" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_SMD_0805" package="0805">
<connects>
<connect gate="R" pin="1" pad="1"/>
<connect gate="R" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" prefix="GND">
<description>&lt;B&gt;SCH symbol&lt;/B&gt; - zem - &lt;I&gt;GrouND&lt;/I&gt;</description>
<gates>
<gate name="ZEM" symbol="GND" x="-45.72" y="35.56"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="BUZ10" prefix="T">
<description>&lt;b&gt;N-Channel Enhancement MOSFET&lt;/b&gt; 50V; 20A; 0,080Ohm</description>
<gates>
<gate name="T" symbol="N-MOSFET" x="0" y="0" swaplevel="1"/>
</gates>
<devices>
<device name="_LEZ" package="TO-220">
<connects>
<connect gate="T" pin="D" pad="2"/>
<connect gate="T" pin="G" pad="1"/>
<connect gate="T" pin="S" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_STOJ" package="TO-220S">
<connects>
<connect gate="T" pin="D" pad="2"/>
<connect gate="T" pin="G" pad="1"/>
<connect gate="T" pin="S" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ARK500/2" prefix="K">
<description>&lt;B&gt;Svorkovnice&lt;/B&gt; - roztec 5mm - dvojita</description>
<gates>
<gate name="K" symbol="ARK500/2" x="-40.64" y="35.56" swaplevel="1"/>
</gates>
<devices>
<device name="" package="ARK500/2">
<connects>
<connect gate="K" pin="K1" pad="1"/>
<connect gate="K" pin="K2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="#PaJa_C-el">
<packages>
<package name="C_EL_5">
<wire x1="-0.762" y1="-1.524" x2="-0.254" y2="-1.524" width="0.127" layer="21"/>
<wire x1="-0.254" y1="-1.524" x2="-0.254" y2="1.524" width="0.127" layer="21"/>
<wire x1="-0.254" y1="1.524" x2="-0.762" y2="1.524" width="0.127" layer="21"/>
<wire x1="-0.762" y1="1.524" x2="-0.762" y2="0" width="0.127" layer="21"/>
<wire x1="0.635" y1="0" x2="1.524" y2="0" width="0.127" layer="21"/>
<wire x1="-0.762" y1="0" x2="-1.236" y2="0" width="0.127" layer="21"/>
<wire x1="-1.236" y1="0" x2="-1.524" y2="0" width="0.127" layer="21"/>
<wire x1="-0.762" y1="0" x2="-0.762" y2="-1.524" width="0.127" layer="21"/>
<wire x1="-2.003" y1="0" x2="-1.236" y2="0" width="0.127" layer="21"/>
<wire x1="1.55" y1="0" x2="2.003" y2="0" width="0.127" layer="21"/>
<wire x1="-1.86" y1="-0.93" x2="-1.24" y2="-0.93" width="0.127" layer="21"/>
<wire x1="-1.55" y1="-0.62" x2="-1.55" y2="-1.24" width="0.127" layer="21"/>
<circle x="0" y="0" radius="5.1308" width="0.127" layer="21"/>
<circle x="-2.54" y="0" radius="0.4763" width="0.127" layer="102"/>
<circle x="2.54" y="0" radius="0.4762" width="0.127" layer="102"/>
<pad name="C-" x="2.54" y="0" drill="0.8128" diameter="2.1844" shape="square"/>
<pad name="C+" x="-2.54" y="0" drill="0.8128" diameter="2.1844"/>
<text x="-3.663" y="-3.186" size="1.4224" layer="27">&gt;VALUE</text>
<text x="-3.017" y="1.758" size="1.4224" layer="25">&gt;NAME</text>
<text x="1.272" y="0.318" size="0.254" layer="100" rot="R90">PaJa</text>
<rectangle x1="0.254" y1="-1.524" x2="0.762" y2="1.524" layer="21"/>
</package>
<package name="C_EL_2">
<wire x1="-0.1378" y1="-1.02" x2="-0.1378" y2="-1.782" width="0.127" layer="21"/>
<wire x1="-0.5188" y1="-1.401" x2="0.2432" y2="-1.401" width="0.127" layer="21"/>
<wire x1="0.4762" y1="-1.524" x2="0.9842" y2="-1.524" width="0.127" layer="21"/>
<wire x1="0.9842" y1="-1.524" x2="0.9842" y2="1.524" width="0.127" layer="21"/>
<wire x1="0.9842" y1="1.524" x2="0.4762" y2="1.524" width="0.127" layer="21"/>
<wire x1="0.4762" y1="1.524" x2="0.4762" y2="-1.524" width="0.127" layer="21"/>
<wire x1="1.4112" y1="1.5" x2="1.7862" y2="1.5" width="0.127" layer="21"/>
<wire x1="1.7862" y1="1.5" x2="1.7862" y2="-1.5" width="0.127" layer="21"/>
<wire x1="1.7862" y1="-1.5" x2="1.4112" y2="-1.5" width="0.127" layer="21"/>
<wire x1="1.4112" y1="-1.5" x2="1.4112" y2="1.5" width="0.127" layer="21"/>
<wire x1="1.5762" y1="0" x2="2.1962" y2="0" width="0.127" layer="21"/>
<wire x1="0.0262" y1="0" x2="0.2712" y2="0" width="0.127" layer="21"/>
<wire x1="0.2712" y1="0" x2="0.3362" y2="0" width="0.127" layer="21"/>
<wire x1="0.2712" y1="0" x2="0.439" y2="0" width="0.127" layer="21"/>
<circle x="1.1112" y="0" radius="2.4848" width="0.127" layer="21"/>
<circle x="-0.0018" y="0" radius="0.477" width="0.127" layer="102"/>
<circle x="2.2242" y="0" radius="0.477" width="0.127" layer="102"/>
<pad name="C+" x="0" y="0" drill="0.8128" diameter="1.27" shape="long" rot="R90"/>
<pad name="C-" x="2.2225" y="0" drill="0.8128" diameter="1.27" shape="long" rot="R90"/>
<text x="-1.2738" y="2.544" size="1.27" layer="25">&gt;Name</text>
<text x="-1.5918" y="-3.816" size="1.27" layer="27">&gt;Value</text>
<text x="0.6342" y="1.908" size="0.254" layer="100">PaJa</text>
<rectangle x1="1.4112" y1="-1.5" x2="1.7862" y2="1.425" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="C-EL">
<wire x1="-3.8173" y1="0.9547" x2="-2.5453" y2="0.9547" width="0.155" layer="94"/>
<wire x1="-3.1812" y1="1.5908" x2="-3.1812" y2="0.3188" width="0.155" layer="94"/>
<wire x1="-2.0638" y1="1.7463" x2="-1.4288" y2="1.7463" width="0.254" layer="94"/>
<wire x1="-1.4288" y1="1.7463" x2="-1.4288" y2="-1.5875" width="0.254" layer="94"/>
<wire x1="-1.4288" y1="-1.5875" x2="-2.0638" y2="-1.5875" width="0.254" layer="94"/>
<wire x1="-2.0638" y1="-1.5875" x2="-2.0638" y2="0" width="0.254" layer="94"/>
<wire x1="-2.0638" y1="0" x2="-2.0638" y2="1.7463" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-2.0638" y2="0" width="0.152" layer="94"/>
<wire x1="-0.4763" y1="0" x2="0" y2="0" width="0.152" layer="94"/>
<text x="-1.589" y="-0.477" size="0.254" layer="100" rot="R90">PaJa</text>
<text x="0.1593" y="0.4775" size="1.6764" layer="95">&gt;Name</text>
<text x="0.1593" y="-2.0675" size="1.6764" layer="96">&gt;Value</text>
<rectangle x1="-0.9525" y1="-1.7463" x2="-0.3175" y2="1.905" layer="94"/>
<pin name="C_EL+" x="-5.08" y="0" visible="off" length="short" direction="pas"/>
<pin name="C_EL-" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="220M/63V" prefix="C" uservalue="yes">
<gates>
<gate name="C" symbol="C-EL" x="0" y="0" swaplevel="1"/>
</gates>
<devices>
<device name="" package="C_EL_5">
<connects>
<connect gate="C" pin="C_EL+" pad="C+"/>
<connect gate="C" pin="C_EL-" pad="C-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="47M/25V" prefix="C" uservalue="yes">
<gates>
<gate name="C" symbol="C-EL" x="0" y="0" swaplevel="1"/>
</gates>
<devices>
<device name="" package="C_EL_2">
<connects>
<connect gate="C" pin="C_EL+" pad="C+"/>
<connect gate="C" pin="C_EL-" pad="C-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="#PaJa_C-folie">
<packages>
<package name="RM5_5,0">
<wire x1="-3.81" y1="-2.54" x2="-3.81" y2="2.54" width="0.127" layer="21"/>
<wire x1="-3.81" y1="2.54" x2="3.81" y2="2.54" width="0.127" layer="21"/>
<wire x1="3.81" y1="2.54" x2="3.81" y2="-2.54" width="0.127" layer="21"/>
<wire x1="3.81" y1="-2.54" x2="-3.81" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-2.067" y1="0" x2="-0.636" y2="0" width="0.127" layer="21"/>
<wire x1="0.636" y1="0" x2="2.067" y2="0" width="0.127" layer="21"/>
<circle x="-2.54" y="0" radius="0.4762" width="0.127" layer="102"/>
<circle x="2.54" y="0" radius="0.4762" width="0.127" layer="102"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" diameter="2.1844" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" diameter="2.1844" shape="octagon"/>
<text x="-2.615" y="-2.4451" size="1.016" layer="27">&gt;Value</text>
<text x="-2.2974" y="1.3537" size="1.016" layer="25">&gt;Name</text>
<text x="0.159" y="0.3182" size="0.254" layer="100" rot="R90">PaJa</text>
<rectangle x1="-0.635" y1="-1.1906" x2="-0.2381" y2="1.1906" layer="21"/>
<rectangle x1="0.2382" y1="-1.1906" x2="0.6351" y2="1.1906" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="C">
<wire x1="-2.54" y1="0" x2="-2.0638" y2="0" width="0.152" layer="94"/>
<wire x1="-0.4763" y1="0" x2="0" y2="0" width="0.152" layer="94"/>
<text x="-1.111" y="-0.479" size="0.254" layer="100" rot="R90">PaJa</text>
<text x="0.1592" y="0.4775" size="1.6764" layer="95">&gt;Name</text>
<text x="0.1592" y="-2.2262" size="1.6764" layer="96">&gt;Value</text>
<rectangle x1="-2.2225" y1="-1.905" x2="-1.5875" y2="1.905" layer="94"/>
<rectangle x1="-0.9525" y1="-1.905" x2="-0.3175" y2="1.905" layer="94"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="2.54" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="1M-K/63V" prefix="C" uservalue="yes">
<gates>
<gate name="CF" symbol="C" x="0" y="0" swaplevel="1"/>
</gates>
<devices>
<device name="" package="RM5_5,0">
<connects>
<connect gate="CF" pin="1" pad="1"/>
<connect gate="CF" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="#_GM99">
<packages>
<package name="CFAC-22">
<wire x1="-2" y1="5" x2="24.5001" y2="5" width="0.127" layer="21"/>
<wire x1="24.5001" y1="5" x2="24.5001" y2="3" width="0.127" layer="21"/>
<wire x1="24.5001" y1="-5" x2="-2" y2="-5" width="0.127" layer="21"/>
<wire x1="-2" y1="-5" x2="-2" y2="-3" width="0.127" layer="21"/>
<wire x1="-2" y1="3" x2="-2" y2="5" width="0.127" layer="21"/>
<wire x1="-2" y1="3" x2="24.5001" y2="3" width="0.127" layer="21"/>
<wire x1="24.5001" y1="3" x2="24.5001" y2="-3" width="0.127" layer="21"/>
<wire x1="24.5001" y1="-3" x2="24.5001" y2="-5" width="0.127" layer="21"/>
<wire x1="24.5001" y1="-3" x2="-2" y2="-3" width="0.127" layer="21"/>
<wire x1="-2" y1="-3" x2="-2" y2="3" width="0.127" layer="21"/>
<pad name="1" x="0" y="0" drill="0.8128" diameter="1.778"/>
<pad name="2" x="22.5001" y="0" drill="0.8128" diameter="1.778"/>
<text x="3.5001" y="1" size="1.27" layer="25">&gt;NAME</text>
<text x="3.5001" y="-1.5001" size="1.27" layer="27">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="KOND">
<wire x1="-2.54" y1="0" x2="-0.508" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.508" y1="1.524" x2="-0.508" y2="-1.524" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="0.508" y2="0" width="0.1524" layer="94"/>
<wire x1="0.508" y1="1.524" x2="0.508" y2="-1.524" width="0.254" layer="94"/>
<text x="-2.54" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CFAC-22,5" prefix="C" uservalue="yes">
<gates>
<gate name="G$2" symbol="KOND" x="5.08" y="0"/>
</gates>
<devices>
<device name="" package="CFAC-22">
<connects>
<connect gate="G$2" pin="1" pad="1"/>
<connect gate="G$2" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="#PaJa_31">
<packages>
<package name="DO41">
<description>&lt;B&gt;Dioda&lt;/B&gt; - roztec nozek 10 mm</description>
<wire x1="-3.0163" y1="1.5875" x2="3.0163" y2="1.5875" width="0.127" layer="21"/>
<wire x1="3.0163" y1="1.5875" x2="3.0163" y2="-1.5875" width="0.127" layer="21"/>
<wire x1="3.0163" y1="-1.5875" x2="-3.0163" y2="-1.5875" width="0.127" layer="21"/>
<wire x1="-3.0163" y1="-1.5875" x2="-3.0163" y2="1.5875" width="0.127" layer="21"/>
<circle x="-5.08" y="0" radius="0.5724" width="0.127" layer="102"/>
<circle x="5.08" y="0" radius="0.5724" width="0.127" layer="102"/>
<pad name="K" x="5.08" y="0" drill="1.016" diameter="2.1844" shape="square"/>
<pad name="A" x="-5.08" y="0" drill="1.016" diameter="2.1844"/>
<text x="-2.8655" y="0.14" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-2.8655" y="-1.315" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<text x="-3.169" y="0.4765" size="0.254" layer="100" font="vector" rot="R90">PaJa</text>
<rectangle x1="1.842" y1="-1.524" x2="2.604" y2="1.524" layer="21"/>
<rectangle x1="3.0163" y1="-0.3175" x2="3.81" y2="0.3175" layer="21"/>
<rectangle x1="-3.81" y1="-0.3175" x2="-3.0163" y2="0.3175" layer="21"/>
<rectangle x1="-4.6038" y1="-0.3175" x2="-3.81" y2="0.3175" layer="51"/>
<rectangle x1="3.81" y1="-0.3175" x2="4.6038" y2="0.3175" layer="51"/>
</package>
<package name="SM-1">
<wire x1="-0.635" y1="0" x2="0.3175" y2="0.635" width="0.127" layer="21"/>
<wire x1="0.3175" y1="0.635" x2="0.3175" y2="-0.635" width="0.127" layer="21"/>
<wire x1="0.3175" y1="-0.635" x2="-0.635" y2="0" width="0.127" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-1.905" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-1.905" y1="-1.27" x2="1.905" y2="-1.27" width="0.127" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.905" y2="1.27" width="0.127" layer="21"/>
<wire x1="1.905" y1="1.27" x2="-1.905" y2="1.27" width="0.127" layer="21"/>
<smd name="K" x="-2.8575" y="0" dx="2.54" dy="1.27" layer="1" rot="R90"/>
<smd name="A" x="2.8575" y="0" dx="2.54" dy="1.27" layer="1" rot="R90"/>
<text x="-1.3494" y="-0.4763" size="0.254" layer="100" font="vector" rot="R90">PaJa</text>
<text x="-2.0638" y="-2.5401" size="1.016" layer="27" font="vector">&gt;Value</text>
<text x="1.5875" y="-0.9525" size="1.016" layer="25" font="vector" rot="R90">&gt;Name</text>
<rectangle x1="-2.54" y1="-1.27" x2="-1.905" y2="1.27" layer="51"/>
<rectangle x1="1.905" y1="-1.27" x2="2.54" y2="1.27" layer="51"/>
<rectangle x1="-1.27" y1="-1.27" x2="-0.635" y2="1.27" layer="21"/>
</package>
<package name="C-2,5">
<circle x="-1.272" y="0" radius="0.477" width="0.127" layer="102"/>
<circle x="1.272" y="0" radius="0.477" width="0.127" layer="102"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" diameter="1.27" shape="long" rot="R90"/>
<pad name="2" x="1.27" y="0" drill="0.8128" diameter="1.27" shape="long" rot="R90"/>
<text x="-1.6704" y="1.3513" size="1.016" layer="25" font="vector">&gt;Name</text>
<text x="-1.6709" y="-2.3853" size="1.016" layer="27" font="vector">&gt;Value</text>
<text x="0.159" y="0.318" size="0.254" layer="100" font="vector" rot="R90">PaJa</text>
<rectangle x1="-0.5556" y1="-1.27" x2="-0.1746" y2="1.27" layer="21"/>
<rectangle x1="0.1746" y1="-1.27" x2="0.5556" y2="1.27" layer="21"/>
<rectangle x1="-0.7938" y1="-0.1588" x2="-0.5556" y2="0.1588" layer="51"/>
<rectangle x1="0.5556" y1="-0.1588" x2="0.7938" y2="0.1588" layer="51"/>
</package>
<package name="C-5">
<circle x="-2.544" y="0" radius="0.477" width="0.127" layer="102"/>
<circle x="2.544" y="0" radius="0.477" width="0.127" layer="102"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" diameter="1.9304" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" diameter="1.9304" shape="octagon"/>
<text x="0.795" y="0.954" size="1.016" layer="25" font="vector">&gt;Name</text>
<text x="0.795" y="-1.9085" size="1.016" layer="27" font="vector">&gt;Value</text>
<text x="0.159" y="0.3182" size="0.254" layer="100" font="vector" rot="R90">PaJa</text>
<rectangle x1="-0.7143" y1="-1.27" x2="-0.238" y2="1.27" layer="21"/>
<rectangle x1="0.2381" y1="-1.27" x2="0.7144" y2="1.27" layer="21"/>
<rectangle x1="-2.0638" y1="-0.1588" x2="-1.4288" y2="0.1588" layer="51"/>
<rectangle x1="1.4288" y1="-0.1588" x2="2.0638" y2="0.1588" layer="51"/>
<rectangle x1="-1.4288" y1="-0.1588" x2="-0.635" y2="0.1588" layer="21"/>
<rectangle x1="0.635" y1="-0.1588" x2="1.4288" y2="0.1588" layer="21"/>
</package>
<package name="C-7,5">
<circle x="-3.814" y="0" radius="0.477" width="0.127" layer="102"/>
<circle x="3.814" y="0" radius="0.477" width="0.127" layer="102"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" diameter="1.9304" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.8128" diameter="1.9304" shape="octagon"/>
<text x="0.795" y="0.954" size="1.016" layer="25" font="vector">&gt;Name</text>
<text x="0.795" y="-1.9085" size="1.016" layer="27" font="vector">&gt;Value</text>
<text x="0.159" y="0.477" size="0.254" layer="100" font="vector" rot="R90">PaJa</text>
<rectangle x1="-0.7155" y1="-1.431" x2="-0.2385" y2="1.431" layer="21"/>
<rectangle x1="0.2385" y1="-1.431" x2="0.7155" y2="1.431" layer="21"/>
<rectangle x1="-2.6988" y1="-0.1588" x2="-0.635" y2="0.1588" layer="21"/>
<rectangle x1="0.635" y1="-0.1588" x2="2.6988" y2="0.1588" layer="21"/>
<rectangle x1="-3.3338" y1="-0.1588" x2="-2.6988" y2="0.1588" layer="51"/>
<rectangle x1="2.6988" y1="-0.1588" x2="3.3338" y2="0.1588" layer="51"/>
</package>
<package name="C-10">
<wire x1="-6.35" y1="2.6035" x2="-6.35" y2="-2.6035" width="0.127" layer="21"/>
<wire x1="-6.35" y1="-2.6035" x2="6.35" y2="-2.6035" width="0.127" layer="21"/>
<wire x1="6.35" y1="-2.6035" x2="6.35" y2="2.6035" width="0.127" layer="21"/>
<wire x1="6.35" y1="2.6035" x2="-6.35" y2="2.6035" width="0.127" layer="21"/>
<circle x="-5.08" y="0" radius="0.4763" width="0.127" layer="102"/>
<circle x="5.08" y="0" radius="0.4762" width="0.127" layer="102"/>
<pad name="1" x="-5.08" y="0" drill="0.8128" diameter="2.1844" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="0.8128" diameter="2.1844" shape="octagon"/>
<text x="0.159" y="0.3182" size="0.254" layer="100" font="vector" rot="R90">PaJa</text>
<text x="-4.0444" y="1.1525" size="1.27" layer="25" font="vector">&gt;Name</text>
<text x="-4.3507" y="-2.4225" size="1.27" layer="27" font="vector">&gt;Value</text>
<rectangle x1="-0.7144" y1="-1.27" x2="-0.2381" y2="1.27" layer="21"/>
<rectangle x1="0.238" y1="-1.27" x2="0.7143" y2="1.27" layer="21"/>
<rectangle x1="-4.6038" y1="-0.1588" x2="-3.81" y2="0.1588" layer="51"/>
<rectangle x1="3.81" y1="-0.1588" x2="4.6038" y2="0.1588" layer="51"/>
<rectangle x1="-3.81" y1="-0.1588" x2="-0.635" y2="0.1588" layer="21"/>
<rectangle x1="0.635" y1="-0.1588" x2="3.81" y2="0.1588" layer="21"/>
</package>
<package name="1206">
<description>&lt;B&gt;SMD&lt;/B&gt; - velikost 1206</description>
<wire x1="-1.0541" y1="0.7938" x2="-0.7938" y2="0.7938" width="0.127" layer="51"/>
<wire x1="-0.7938" y1="0.7938" x2="0.7938" y2="0.7938" width="0.127" layer="21"/>
<wire x1="0.7938" y1="0.7938" x2="1.0541" y2="0.7938" width="0.127" layer="51"/>
<wire x1="-1.0541" y1="-0.7938" x2="-0.7938" y2="-0.7938" width="0.127" layer="51"/>
<wire x1="-0.7938" y1="-0.7938" x2="0.7938" y2="-0.7938" width="0.127" layer="21"/>
<wire x1="0.7938" y1="-0.7938" x2="1.0541" y2="-0.7938" width="0.127" layer="51"/>
<wire x1="1.0541" y1="0.7938" x2="1.0541" y2="-0.7938" width="0.127" layer="51"/>
<wire x1="-1.0541" y1="0.7938" x2="-1.0541" y2="-0.7938" width="0.127" layer="51"/>
<smd name="1" x="-1.5875" y="0" dx="1.143" dy="1.7018" layer="1"/>
<smd name="2" x="1.5875" y="0" dx="1.143" dy="1.7018" layer="1"/>
<text x="-0.3175" y="-1.1906" size="0.254" layer="100" font="vector">PaJa</text>
<text x="-0.7938" y="-0.4763" size="1.016" layer="25" font="vector">&gt;Name</text>
<text x="-0.7938" y="0.9525" size="1.016" layer="27" font="vector">&gt;Value</text>
<rectangle x1="-1.4541" y1="-0.7874" x2="-0.9461" y2="0.7874" layer="51"/>
<rectangle x1="0.9461" y1="-0.7874" x2="1.4541" y2="0.7874" layer="51"/>
</package>
<package name="0805">
<description>&lt;B&gt;SMD&lt;/B&gt; - velikost 0805</description>
<wire x1="-0.3226" y1="0.5645" x2="-0.5645" y2="0.5645" width="0.127" layer="51"/>
<wire x1="-0.5645" y1="0.5645" x2="-0.5645" y2="-0.5645" width="0.127" layer="51"/>
<wire x1="-0.5645" y1="-0.5645" x2="-0.3226" y2="-0.5645" width="0.127" layer="51"/>
<wire x1="0.3226" y1="0.5645" x2="0.5645" y2="0.5645" width="0.127" layer="51"/>
<wire x1="0.5645" y1="0.5645" x2="0.5645" y2="-0.5645" width="0.127" layer="51"/>
<wire x1="0.5645" y1="-0.5645" x2="0.3226" y2="-0.5645" width="0.127" layer="51"/>
<wire x1="-0.3226" y1="0.5645" x2="0.3226" y2="0.5645" width="0.127" layer="21"/>
<wire x1="0.3226" y1="-0.5645" x2="-0.3226" y2="-0.5645" width="0.127" layer="21"/>
<smd name="1" x="-0.9525" y="0" dx="1.016" dy="1.4224" layer="1"/>
<smd name="2" x="0.9525" y="0" dx="1.016" dy="1.4224" layer="1"/>
<text x="-1.397" y="-1.6351" size="0.8128" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<text x="-1.3177" y="0.8413" size="0.8128" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="0.3956" y="-0.4763" size="0.254" layer="100" font="vector" rot="R90">PaJa</text>
<rectangle x1="0.4064" y1="-0.6096" x2="0.9144" y2="0.6096" layer="51"/>
<rectangle x1="-0.9144" y1="-0.6096" x2="-0.4064" y2="0.6096" layer="51"/>
</package>
<package name="64P">
<wire x1="-5.3975" y1="-5.08" x2="-5.3975" y2="5.08" width="0.127" layer="21"/>
<wire x1="-5.3975" y1="5.08" x2="4.7625" y2="5.08" width="0.127" layer="21"/>
<wire x1="4.7625" y1="5.08" x2="4.7625" y2="-1.5875" width="0.127" layer="21"/>
<wire x1="4.7625" y1="-1.5875" x2="4.7625" y2="-3.4925" width="0.127" layer="21"/>
<wire x1="4.7625" y1="-3.4925" x2="4.7625" y2="-5.08" width="0.127" layer="21"/>
<wire x1="4.7625" y1="-5.08" x2="-5.3975" y2="-5.08" width="0.127" layer="21"/>
<wire x1="4.7625" y1="-1.5875" x2="5.715" y2="-1.5875" width="0.127" layer="21"/>
<wire x1="5.715" y1="-1.5875" x2="5.715" y2="-2.2225" width="0.127" layer="21"/>
<wire x1="5.715" y1="-2.2225" x2="5.3975" y2="-2.2225" width="0.127" layer="21"/>
<wire x1="5.3975" y1="-2.2225" x2="5.3975" y2="-2.8575" width="0.127" layer="21"/>
<wire x1="5.3975" y1="-2.8575" x2="5.715" y2="-2.8575" width="0.127" layer="21"/>
<wire x1="5.715" y1="-2.8575" x2="5.715" y2="-3.4925" width="0.127" layer="21"/>
<wire x1="5.715" y1="-3.4925" x2="4.7625" y2="-3.4925" width="0.127" layer="21"/>
<circle x="0" y="-2.54" radius="0.449" width="0.127" layer="102"/>
<circle x="2.54" y="0" radius="0.449" width="0.127" layer="102"/>
<circle x="0" y="2.54" radius="0.449" width="0.127" layer="102"/>
<pad name="1" x="0" y="2.54" drill="0.8128" diameter="1.9304" shape="octagon"/>
<pad name="2" x="0" y="-2.54" drill="0.8128" diameter="1.9304" shape="octagon"/>
<pad name="3" x="2.54" y="0" drill="0.8128" diameter="1.9304" shape="octagon"/>
<text x="-1.5875" y="-2.8575" size="1.27" layer="25" font="vector" rot="R90">&gt;NAME</text>
<text x="-3.8099" y="-3.6513" size="1.27" layer="27" font="vector" rot="R90">&gt;VALUE</text>
<text x="5.2388" y="-3.0162" size="0.254" layer="100" font="vector" rot="R90">PaJa</text>
</package>
<package name="64W">
<wire x1="-2.54" y1="-5.08" x2="-2.54" y2="5.08" width="0.127" layer="21"/>
<wire x1="-2.54" y1="5.08" x2="2.54" y2="5.08" width="0.127" layer="21"/>
<wire x1="2.54" y1="5.08" x2="2.54" y2="-5.08" width="0.127" layer="21"/>
<wire x1="2.54" y1="-5.08" x2="-2.54" y2="-5.08" width="0.127" layer="21"/>
<wire x1="-0.2381" y1="3.7306" x2="-0.2381" y2="1.3494" width="0.127" layer="51"/>
<wire x1="0.2381" y1="3.7306" x2="0.2381" y2="1.3494" width="0.127" layer="51"/>
<circle x="0" y="2.54" radius="0.449" width="0.127" layer="102"/>
<circle x="0" y="0" radius="0.449" width="0.127" layer="102"/>
<circle x="0" y="-2.54" radius="0.449" width="0.127" layer="102"/>
<circle x="0" y="2.54" radius="1.27" width="0.127" layer="51"/>
<pad name="1" x="0" y="2.54" drill="0.8128" diameter="1.4224" shape="long"/>
<pad name="2" x="0" y="-2.54" drill="0.8128" diameter="1.4224" shape="long"/>
<pad name="3" x="0" y="0" drill="0.8128" diameter="1.4224" shape="long"/>
<text x="-2.2225" y="5.3975" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="4.1276" y="-3.3338" size="1.27" layer="27" font="vector" rot="R90">&gt;VALUE</text>
<text x="1.4288" y="-4.9213" size="0.254" layer="100" font="vector">PaJa</text>
</package>
<package name="64X">
<wire x1="-2.54" y1="-5.08" x2="-2.54" y2="5.08" width="0.127" layer="21"/>
<wire x1="-2.54" y1="5.08" x2="2.54" y2="5.08" width="0.127" layer="21"/>
<wire x1="2.54" y1="5.08" x2="2.54" y2="-5.08" width="0.127" layer="21"/>
<wire x1="2.54" y1="-5.08" x2="0.9525" y2="-5.08" width="0.127" layer="21"/>
<wire x1="0.9525" y1="-5.08" x2="-0.9525" y2="-5.08" width="0.127" layer="21"/>
<wire x1="-0.9525" y1="-5.08" x2="-2.54" y2="-5.08" width="0.127" layer="21"/>
<wire x1="0.9525" y1="-5.08" x2="0.9525" y2="-6.0325" width="0.127" layer="21"/>
<wire x1="0.9525" y1="-6.0325" x2="0.3175" y2="-6.0325" width="0.127" layer="21"/>
<wire x1="0.3175" y1="-6.0325" x2="0.3175" y2="-5.715" width="0.127" layer="21"/>
<wire x1="0.3175" y1="-5.715" x2="-0.3175" y2="-5.715" width="0.127" layer="21"/>
<wire x1="-0.3175" y1="-5.715" x2="-0.3175" y2="-6.0325" width="0.127" layer="21"/>
<wire x1="-0.3175" y1="-6.0325" x2="-0.9525" y2="-6.0325" width="0.127" layer="21"/>
<wire x1="-0.9525" y1="-6.0325" x2="-0.9525" y2="-5.08" width="0.127" layer="21"/>
<circle x="0" y="-2.54" radius="0.449" width="0.127" layer="102"/>
<circle x="0" y="0" radius="0.449" width="0.127" layer="102"/>
<circle x="0" y="2.54" radius="0.449" width="0.127" layer="102"/>
<pad name="1" x="0" y="2.54" drill="0.8128" diameter="1.4224" shape="long"/>
<pad name="2" x="0" y="-2.54" drill="0.8128" diameter="1.4224" shape="long"/>
<pad name="3" x="0" y="0" drill="0.8128" diameter="1.4224" shape="long"/>
<text x="-2.2225" y="3.4925" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="4.1276" y="-3.6513" size="1.27" layer="27" font="vector" rot="R90">&gt;VALUE</text>
<text x="0.4762" y="-5.2387" size="0.254" layer="100" font="vector" rot="R180">PaJa</text>
</package>
<package name="64Y">
<description>&lt;B&gt;trimr&lt;/B&gt; - 25-ti otackovy - nastojato</description>
<wire x1="-1.27" y1="-5.08" x2="-1.27" y2="5.08" width="0.127" layer="21"/>
<wire x1="-1.27" y1="5.08" x2="3.81" y2="5.08" width="0.127" layer="21"/>
<wire x1="3.81" y1="5.08" x2="3.81" y2="-5.08" width="0.127" layer="21"/>
<wire x1="3.81" y1="-5.08" x2="-1.27" y2="-5.08" width="0.127" layer="21"/>
<wire x1="0.4763" y1="3.4925" x2="0.4763" y2="1.5875" width="0.127" layer="51" curve="-259.607592" cap="flat"/>
<wire x1="0.4763" y1="1.5875" x2="0.4763" y2="3.4925" width="0.127" layer="51" curve="-100.392408" cap="flat"/>
<wire x1="1.0319" y1="3.7306" x2="1.0319" y2="1.3494" width="0.127" layer="51"/>
<wire x1="1.5081" y1="3.7306" x2="1.5081" y2="1.3494" width="0.127" layer="51"/>
<circle x="0" y="-2.54" radius="0.449" width="0.127" layer="102"/>
<circle x="2.54" y="0" radius="0.449" width="0.127" layer="102"/>
<circle x="0" y="2.54" radius="0.449" width="0.127" layer="102"/>
<pad name="1" x="0" y="2.54" drill="0.8128" diameter="1.9304" shape="octagon"/>
<pad name="2" x="0" y="-2.54" drill="0.8128" diameter="1.9304" shape="octagon"/>
<pad name="3" x="2.54" y="0" drill="0.8128" diameter="1.9304" shape="octagon"/>
<text x="-0.9525" y="5.3975" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="5.3976" y="-3.3338" size="1.27" layer="27" font="vector" rot="R90">&gt;VALUE</text>
<text x="2.6988" y="-4.9213" size="0.254" layer="100" font="vector">PaJa</text>
</package>
<package name="64Z">
<description>&lt;B&gt;trimr&lt;/B&gt; - 25-ti otackovy - nalezato</description>
<wire x1="-1.27" y1="-5.08" x2="-1.27" y2="5.08" width="0.127" layer="21"/>
<wire x1="-1.27" y1="5.08" x2="3.81" y2="5.08" width="0.127" layer="21"/>
<wire x1="3.81" y1="5.08" x2="3.81" y2="-5.08" width="0.127" layer="21"/>
<wire x1="3.81" y1="-5.08" x2="2.2225" y2="-5.08" width="0.127" layer="21"/>
<wire x1="2.2225" y1="-5.08" x2="0.3175" y2="-5.08" width="0.127" layer="21"/>
<wire x1="0.3175" y1="-5.08" x2="-1.27" y2="-5.08" width="0.127" layer="21"/>
<wire x1="2.2225" y1="-5.08" x2="2.2225" y2="-6.0325" width="0.127" layer="21"/>
<wire x1="2.2225" y1="-6.0325" x2="1.5875" y2="-6.0325" width="0.127" layer="21"/>
<wire x1="1.5875" y1="-6.0325" x2="1.5875" y2="-5.715" width="0.127" layer="21"/>
<wire x1="1.5875" y1="-5.715" x2="0.9525" y2="-5.715" width="0.127" layer="21"/>
<wire x1="0.9525" y1="-5.715" x2="0.9525" y2="-6.0325" width="0.127" layer="21"/>
<wire x1="0.9525" y1="-6.0325" x2="0.3175" y2="-6.0325" width="0.127" layer="21"/>
<wire x1="0.3175" y1="-6.0325" x2="0.3175" y2="-5.08" width="0.127" layer="21"/>
<circle x="0" y="2.54" radius="0.449" width="0.127" layer="102"/>
<circle x="2.54" y="0" radius="0.449" width="0.127" layer="102"/>
<circle x="0" y="-2.54" radius="0.449" width="0.127" layer="102"/>
<pad name="1" x="0" y="2.54" drill="0.8128" diameter="1.9304" shape="octagon"/>
<pad name="2" x="0" y="-2.54" drill="0.8128" diameter="1.9304" shape="octagon"/>
<pad name="3" x="2.54" y="0" drill="0.8128" diameter="1.9304" shape="octagon"/>
<text x="-0.9525" y="5.3975" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="5.3976" y="-3.3338" size="1.27" layer="27" font="vector" rot="R90">&gt;VALUE</text>
<text x="2.6988" y="-4.9213" size="0.254" layer="100" font="vector">PaJa</text>
</package>
<package name="PM19">
<description>&lt;B&gt;Trimr&lt;/B&gt; - 20-ti otackovy - nalezato</description>
<wire x1="-9.6838" y1="2.3813" x2="-9.6838" y2="-2.3812" width="0.127" layer="21"/>
<wire x1="9.6838" y1="2.3813" x2="9.6838" y2="1.27" width="0.127" layer="21"/>
<wire x1="9.6838" y1="1.27" x2="9.6838" y2="-1.27" width="0.127" layer="21"/>
<wire x1="9.6838" y1="-1.27" x2="9.6838" y2="-2.3812" width="0.127" layer="21"/>
<wire x1="-9.6838" y1="-2.3812" x2="9.6838" y2="-2.3812" width="0.127" layer="21"/>
<wire x1="9.6838" y1="1.27" x2="10.9538" y2="1.27" width="0.127" layer="21"/>
<wire x1="10.9538" y1="1.27" x2="10.9538" y2="0.3175" width="0.127" layer="21"/>
<wire x1="10.9538" y1="0.3175" x2="10.4776" y2="0.3175" width="0.127" layer="21"/>
<wire x1="10.4776" y1="0.3175" x2="10.4776" y2="-0.3175" width="0.127" layer="21"/>
<wire x1="10.4776" y1="-0.3175" x2="10.9538" y2="-0.3175" width="0.127" layer="21"/>
<wire x1="10.9538" y1="-0.3175" x2="10.9538" y2="-1.27" width="0.127" layer="21"/>
<wire x1="10.9538" y1="-1.27" x2="9.6838" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-7.3025" y1="2.3813" x2="-5.3975" y2="2.3813" width="0.127" layer="51"/>
<wire x1="5.3975" y1="2.3813" x2="7.3025" y2="2.3813" width="0.127" layer="51"/>
<wire x1="9.6838" y1="2.3813" x2="7.3025" y2="2.3813" width="0.127" layer="21"/>
<wire x1="5.3975" y1="2.3813" x2="-5.3975" y2="2.3813" width="0.127" layer="21"/>
<wire x1="-7.3025" y1="2.3813" x2="-9.6838" y2="2.3813" width="0.127" layer="21"/>
<circle x="-6.35" y="1.27" radius="0.5724" width="0.127" layer="102"/>
<circle x="1.27" y="-1.27" radius="0.5723" width="0.127" layer="102"/>
<circle x="6.35" y="1.27" radius="0.5724" width="0.127" layer="102"/>
<pad name="A" x="-6.35" y="1.27" drill="1.016" diameter="2.54" shape="octagon"/>
<pad name="B" x="6.35" y="1.27" drill="1.016" diameter="2.54" shape="octagon"/>
<pad name="C" x="1.27" y="-1.27" drill="1.016" diameter="2.54" shape="octagon"/>
<text x="10.1601" y="-0.4763" size="0.254" layer="100" font="vector" rot="R90">PaJa</text>
<text x="-3.175" y="0.3175" size="1.6764" layer="25" font="vector">&gt;Name</text>
<text x="-8.89" y="-1.905" size="1.6764" layer="27" font="vector">&gt;Value</text>
</package>
<package name="PT10H">
<description>&lt;B&gt;Trimr&lt;/B&gt; - nastojato - 10,3mm - 100R az 5M</description>
<wire x1="-5.08" y1="2.2225" x2="-5.08" y2="-2.54" width="0.127" layer="21"/>
<wire x1="5.08" y1="-2.54" x2="5.08" y2="2.2225" width="0.127" layer="21"/>
<wire x1="-3.4925" y1="-2.54" x2="-1.5875" y2="-2.54" width="0.127" layer="51"/>
<wire x1="1.5875" y1="-2.54" x2="3.4925" y2="-2.54" width="0.127" layer="51"/>
<wire x1="-1.27" y1="2.2225" x2="1.27" y2="2.2225" width="0.127" layer="51"/>
<wire x1="1.27" y1="2.2225" x2="5.08" y2="2.2225" width="0.127" layer="21"/>
<wire x1="5.08" y1="-2.54" x2="3.4925" y2="-2.54" width="0.127" layer="21"/>
<wire x1="1.5875" y1="-2.54" x2="-1.5875" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-3.4925" y1="-2.54" x2="-5.08" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-1.27" y1="2.2225" x2="-5.08" y2="2.2225" width="0.127" layer="21"/>
<circle x="0" y="1.272" radius="0.5732" width="0.127" layer="102"/>
<circle x="2.544" y="-1.272" radius="0.5732" width="0.127" layer="102"/>
<circle x="-2.544" y="-1.272" radius="0.5732" width="0.127" layer="102"/>
<pad name="1" x="-2.54" y="-1.27" drill="1.016" diameter="2.54" shape="octagon"/>
<pad name="2" x="2.54" y="-1.27" drill="1.016" diameter="2.54" shape="octagon"/>
<pad name="3" x="0" y="1.27" drill="1.016" diameter="2.54" shape="octagon"/>
<text x="-3.431" y="2.7511" size="1.27" layer="27">&gt;VALUE</text>
<text x="-2.636" y="-3.954" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.477" y="-2.226" size="0.254" layer="100">PaJa</text>
</package>
<package name="PT10V">
<description>&lt;B&gt;Trimr&lt;/B&gt; - nalezato - 10,3mm - 250R az 5M</description>
<wire x1="4.445" y1="-5.3975" x2="4.7625" y2="-5.08" width="0.127" layer="21"/>
<wire x1="4.7625" y1="-1.587" x2="4.7625" y2="-1.5875" width="0.127" layer="21"/>
<wire x1="4.7625" y1="-1.5875" x2="4.7625" y2="-5.08" width="0.127" layer="21"/>
<wire x1="-4.445" y1="-5.3975" x2="-4.7625" y2="-5.08" width="0.127" layer="21"/>
<wire x1="-4.7625" y1="-5.08" x2="-4.7625" y2="-1.5875" width="0.127" layer="21"/>
<wire x1="-5.08" y1="5.08" x2="-5.08" y2="-5.08" width="0.127" layer="101"/>
<wire x1="-5.08" y1="-5.08" x2="-3.175" y2="-5.08" width="0.127" layer="101"/>
<wire x1="-1.905" y1="-5.08" x2="1.905" y2="-5.08" width="0.127" layer="101"/>
<wire x1="3.175" y1="-5.08" x2="5.08" y2="-5.08" width="0.127" layer="101"/>
<wire x1="5.08" y1="-5.08" x2="5.08" y2="5.08" width="0.127" layer="101"/>
<wire x1="5.08" y1="5.08" x2="0.635" y2="5.08" width="0.127" layer="101"/>
<wire x1="-0.635" y1="5.08" x2="-5.08" y2="5.08" width="0.127" layer="101"/>
<wire x1="4.7625" y1="-1.5875" x2="1.5875" y2="4.7625" width="0.127" layer="21" curve="90"/>
<wire x1="-4.7625" y1="-1.5875" x2="-1.5875" y2="4.7625" width="0.127" layer="21" curve="-90"/>
<wire x1="-1.5875" y1="4.7625" x2="1.5875" y2="4.7625" width="0.127" layer="51" curve="-36.869898"/>
<wire x1="-4.1275" y1="-5.3975" x2="-0.9525" y2="-5.3975" width="0.127" layer="51"/>
<wire x1="0.9525" y1="-5.3975" x2="4.1275" y2="-5.3975" width="0.127" layer="51"/>
<wire x1="4.1275" y1="-5.3975" x2="4.445" y2="-5.3975" width="0.127" layer="21"/>
<wire x1="0.9525" y1="-5.3975" x2="-0.9525" y2="-5.3975" width="0.127" layer="21"/>
<wire x1="-4.1275" y1="-5.3975" x2="-4.445" y2="-5.3975" width="0.127" layer="21"/>
<circle x="0" y="0" radius="1.905" width="0.127" layer="51"/>
<circle x="0" y="5.088" radius="0.5732" width="0.127" layer="102"/>
<circle x="-2.544" y="-5.088" radius="0.5732" width="0.127" layer="102"/>
<circle x="2.544" y="-5.088" radius="0.5732" width="0.127" layer="102"/>
<pad name="1" x="-2.54" y="-5.08" drill="1.016" diameter="2.54" shape="octagon"/>
<pad name="3" x="0" y="5.08" drill="1.016" diameter="2.54" shape="octagon"/>
<pad name="2" x="2.54" y="-5.08" drill="1.016" diameter="2.54" shape="octagon"/>
<text x="-0.477" y="-4.9215" size="0.254" layer="100">PaJa</text>
<text x="-2.871" y="2.1895" size="1.27" layer="25">&gt;Name</text>
<text x="-3.2131" y="-3.309" size="1.27" layer="27">&gt;Value</text>
</package>
<package name="PT15H">
<description>&lt;B&gt;Trimr&lt;/B&gt; - nastojato - 15mm - 100R az 1M</description>
<wire x1="-1.9065" y1="3.021" x2="-7.791" y2="3.021" width="0.127" layer="21"/>
<wire x1="-7.791" y1="3.021" x2="-7.791" y2="-3.18" width="0.127" layer="21"/>
<wire x1="-7.791" y1="-3.18" x2="-7.153" y2="-3.18" width="0.127" layer="21"/>
<wire x1="-3.023" y1="-3.18" x2="3.023" y2="-3.18" width="0.127" layer="21"/>
<wire x1="7.153" y1="-3.18" x2="7.791" y2="-3.18" width="0.127" layer="21"/>
<wire x1="7.791" y1="-3.18" x2="7.791" y2="3.021" width="0.127" layer="21"/>
<wire x1="7.791" y1="3.021" x2="1.9065" y2="3.021" width="0.127" layer="21"/>
<wire x1="-7.153" y1="-3.18" x2="-3.023" y2="-3.18" width="0.127" layer="51"/>
<wire x1="3.023" y1="-3.18" x2="7.153" y2="-3.18" width="0.127" layer="51"/>
<wire x1="-1.9065" y1="3.021" x2="1.9065" y2="3.021" width="0.127" layer="51"/>
<circle x="0" y="2.54" radius="0.7099" width="0.127" layer="102"/>
<circle x="5.08" y="-2.54" radius="0.7099" width="0.127" layer="102"/>
<circle x="-5.08" y="-2.54" radius="0.7099" width="0.127" layer="102"/>
<pad name="1" x="-5.08" y="-2.54" drill="1.27" diameter="3.2" shape="octagon"/>
<pad name="2" x="5.08" y="-2.54" drill="1.27" diameter="3.2" shape="octagon"/>
<pad name="3" x="0" y="2.54" drill="1.27" diameter="3.2" shape="octagon"/>
<text x="-3.08" y="-2.858" size="1.4224" layer="25">&gt;NAME</text>
<text x="-3.875" y="-0.858" size="1.4224" layer="27">&gt;VALUE</text>
<text x="4.611" y="-0.477" size="0.254" layer="100">PaJa</text>
</package>
<package name="PT15V">
<description>&lt;B&gt;Trimr&lt;/B&gt; - nalezato - 15mm - 100R az 5M</description>
<wire x1="-7.62" y1="-5.715" x2="-7.62" y2="-1.27" width="0.127" layer="21"/>
<wire x1="7.62" y1="-1.27" x2="7.62" y2="-5.715" width="0.127" layer="21"/>
<wire x1="-7.62" y1="-1.27" x2="-1.5875" y2="7.62" width="0.127" layer="21" curve="-85.910491"/>
<wire x1="7.62" y1="-1.27" x2="1.5875" y2="7.62" width="0.127" layer="21" curve="87.989828"/>
<wire x1="-1.5875" y1="7.62" x2="1.5875" y2="7.62" width="0.127" layer="51" curve="-24.023113"/>
<wire x1="-7.62" y1="-5.715" x2="-6.985" y2="-6.35" width="0.127" layer="21"/>
<wire x1="-6.985" y1="-6.35" x2="-6.35" y2="-6.985" width="0.127" layer="51"/>
<wire x1="-6.35" y1="-6.985" x2="-3.175" y2="-6.985" width="0.127" layer="51"/>
<wire x1="3.175" y1="-6.985" x2="6.35" y2="-6.985" width="0.127" layer="51"/>
<wire x1="6.35" y1="-6.985" x2="6.985" y2="-6.35" width="0.127" layer="51"/>
<wire x1="6.985" y1="-6.35" x2="7.62" y2="-5.715" width="0.127" layer="21"/>
<wire x1="3.175" y1="-6.985" x2="-3.175" y2="-6.985" width="0.127" layer="21"/>
<circle x="0" y="0" radius="3.81" width="0.127" layer="51"/>
<circle x="0" y="6.36" radius="0.795" width="0.127" layer="102"/>
<circle x="-5.088" y="-6.36" radius="0.795" width="0.127" layer="102"/>
<circle x="5.088" y="-6.36" radius="0.795" width="0.127" layer="102"/>
<pad name="1" x="-5.08" y="-6.35" drill="1.4224" diameter="3.2" shape="octagon"/>
<pad name="3" x="0" y="6.35" drill="1.4224" diameter="3.2" shape="octagon"/>
<pad name="2" x="5.08" y="-6.35" drill="1.4224" diameter="3.2" shape="octagon"/>
<text x="-2.7389" y="0.6731" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.3749" y="-1.9629" size="1.27" layer="27">&gt;VALUE</text>
<text x="-0.4773" y="-6.8201" size="0.3048" layer="100">PaJa</text>
</package>
<package name="PT6H">
<description>&lt;B&gt;Trimr&lt;/B&gt; - nastojato - 6,3mm - 100R az 2,5M</description>
<wire x1="1.076" y1="-1.272" x2="-1.052" y2="-1.272" width="0.127" layer="21"/>
<wire x1="-3.18" y1="-0.001" x2="-3.18" y2="1.431" width="0.127" layer="21"/>
<wire x1="-3.18" y1="1.431" x2="-1.2715" y2="1.431" width="0.127" layer="21"/>
<wire x1="1.2715" y1="1.431" x2="3.18" y2="1.431" width="0.127" layer="21"/>
<wire x1="3.18" y1="-0.001" x2="3.18" y2="1.431" width="0.127" layer="21"/>
<wire x1="-1.2715" y1="1.431" x2="1.2715" y2="1.431" width="0.127" layer="51"/>
<wire x1="-3.18" y1="-0.001" x2="-3.18" y2="-1.272" width="0.127" layer="51"/>
<wire x1="-3.18" y1="-1.272" x2="-1.114" y2="-1.272" width="0.127" layer="51"/>
<wire x1="1.114" y1="-1.272" x2="3.18" y2="-1.272" width="0.127" layer="51"/>
<wire x1="3.18" y1="-1.272" x2="3.18" y2="-0.001" width="0.127" layer="51"/>
<circle x="2.544" y="-1.272" radius="0.5732" width="0.127" layer="102"/>
<circle x="-2.544" y="-1.272" radius="0.5732" width="0.127" layer="102"/>
<circle x="0" y="1.272" radius="0.5732" width="0.127" layer="102"/>
<pad name="1" x="-2.54" y="-1.27" drill="1.016" diameter="2.1844" shape="octagon"/>
<pad name="3" x="0" y="1.27" drill="1.016" diameter="2.1844" shape="octagon"/>
<pad name="2" x="2.54" y="-1.27" drill="1.016" diameter="2.1844" shape="octagon"/>
<text x="-3.544" y="2.3871" size="1.27" layer="27">&gt;VALUE</text>
<text x="-2.7261" y="-3.7721" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.477" y="-1.113" size="0.3048" layer="100">PaJa</text>
</package>
<package name="PT6V">
<description>&lt;B&gt;Trimr&lt;/B&gt; - nalezato - 6,3mm - 250R az 5M</description>
<wire x1="-3.175" y1="3.0163" x2="-3.175" y2="-3.4925" width="0.127" layer="101"/>
<wire x1="-3.175" y1="-3.4925" x2="3.175" y2="-3.4925" width="0.127" layer="101"/>
<wire x1="3.175" y1="-3.4925" x2="3.175" y2="3.0163" width="0.127" layer="101"/>
<wire x1="-3.175" y1="3.0163" x2="-0.3175" y2="3.0163" width="0.127" layer="101"/>
<wire x1="0.3175" y1="3.0163" x2="3.175" y2="3.0163" width="0.127" layer="101"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.127" layer="21"/>
<wire x1="3.175" y1="0" x2="3.175" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-2.54" x2="1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="1.27" y1="-2.54" x2="3.175" y2="-2.54" width="0.127" layer="51"/>
<wire x1="3.175" y1="-2.54" x2="3.175" y2="-1.27" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-2.54" x2="-3.175" y2="-2.54" width="0.127" layer="51"/>
<wire x1="-3.175" y1="-2.54" x2="-3.175" y2="-1.27" width="0.127" layer="51"/>
<wire x1="-3.175" y1="0" x2="-1.27" y2="2.8575" width="0.127" layer="21" curve="-65.470453"/>
<wire x1="3.175" y1="0" x2="1.27" y2="2.8575" width="0.127" layer="21" curve="65.470453"/>
<wire x1="-1.27" y1="2.8575" x2="1.27" y2="2.8575" width="0.127" layer="51" curve="-47.924978"/>
<circle x="0" y="0" radius="1.27" width="0.127" layer="51"/>
<circle x="0" y="2.544" radius="0.5732" width="0.127" layer="102"/>
<circle x="-2.544" y="-2.544" radius="0.5732" width="0.127" layer="102"/>
<circle x="2.544" y="-2.544" radius="0.5732" width="0.127" layer="102"/>
<pad name="2" x="2.54" y="-2.54" drill="1.016" diameter="2.1844" shape="octagon"/>
<pad name="1" x="-2.54" y="-2.54" drill="1.016" diameter="2.1844" shape="octagon"/>
<pad name="3" x="0" y="2.54" drill="1.016" diameter="2.1844" shape="octagon"/>
<text x="2.703" y="-1.1135" size="1.27" layer="25" rot="R90">&gt;Name</text>
<text x="-3.1803" y="-5.0877" size="1.27" layer="27">&gt;Value</text>
<text x="-0.477" y="-2.3815" size="0.254" layer="100">PaJa</text>
</package>
<package name="TP011">
<wire x1="-6.35" y1="-2.2225" x2="-6.35" y2="0.3175" width="0.254" layer="21"/>
<wire x1="-6.35" y1="0.3175" x2="-2.54" y2="0.3175" width="0.254" layer="21"/>
<wire x1="-2.54" y1="0.3175" x2="2.54" y2="0.3175" width="0.254" layer="21"/>
<wire x1="2.54" y1="0.3175" x2="6.35" y2="0.3175" width="0.254" layer="21"/>
<wire x1="6.35" y1="0.3175" x2="6.35" y2="-2.2225" width="0.254" layer="21"/>
<wire x1="-2.54" y1="0.3175" x2="-2.54" y2="2.54" width="0.127" layer="21"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="0.3175" width="0.127" layer="21"/>
<wire x1="-1.5875" y1="-3.81" x2="-1.5875" y2="-4.7625" width="0.127" layer="21"/>
<wire x1="-1.5875" y1="-4.7625" x2="1.5875" y2="-4.7625" width="0.127" layer="21"/>
<wire x1="1.5875" y1="-4.7625" x2="1.5875" y2="-3.81" width="0.127" layer="21"/>
<wire x1="-5.715" y1="-2.2225" x2="-5.715" y2="-3.175" width="0.127" layer="51"/>
<wire x1="-5.715" y1="-3.175" x2="5.715" y2="-3.175" width="0.127" layer="51"/>
<wire x1="5.715" y1="-3.175" x2="5.715" y2="-2.2225" width="0.127" layer="51"/>
<wire x1="-6.35" y1="-2.2225" x2="-4.1275" y2="-2.2225" width="0.254" layer="21"/>
<wire x1="-4.1275" y1="-2.2225" x2="-1.5875" y2="-2.2225" width="0.254" layer="51"/>
<wire x1="-1.5875" y1="-2.2225" x2="-0.9525" y2="-2.2225" width="0.254" layer="51"/>
<wire x1="0.9525" y1="-2.2225" x2="1.5875" y2="-2.2225" width="0.254" layer="51"/>
<wire x1="1.5875" y1="-2.2225" x2="4.1275" y2="-2.2225" width="0.254" layer="51"/>
<wire x1="4.1275" y1="-2.2225" x2="6.35" y2="-2.2225" width="0.254" layer="21"/>
<wire x1="-2.54" y1="2.54" x2="-1.5875" y2="2.54" width="0.127" layer="21"/>
<wire x1="-1.5875" y1="2.54" x2="1.5875" y2="2.54" width="0.127" layer="51"/>
<wire x1="1.5875" y1="2.54" x2="2.54" y2="2.54" width="0.127" layer="21"/>
<wire x1="-0.9525" y1="-2.2225" x2="0.9525" y2="-2.2225" width="0.254" layer="21"/>
<wire x1="-1.5875" y1="-3.81" x2="-1.5875" y2="-2.2225" width="0.127" layer="51"/>
<wire x1="1.5875" y1="-3.81" x2="1.5875" y2="-2.2225" width="0.127" layer="51"/>
<circle x="-2.54" y="-2.54" radius="0.5724" width="0.127" layer="102"/>
<circle x="2.54" y="-2.54" radius="0.5723" width="0.127" layer="102"/>
<circle x="0" y="2.54" radius="0.5724" width="0.127" layer="102"/>
<pad name="1" x="-2.54" y="-2.54" drill="1.016" diameter="2.54" shape="octagon"/>
<pad name="2" x="2.54" y="-2.54" drill="1.016" diameter="2.54" shape="octagon"/>
<pad name="3" x="0" y="2.54" drill="1.016" diameter="2.54" shape="octagon"/>
<text x="-0.3175" y="-1.27" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<text x="-6.025" y="-1.27" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-0.477" y="-2.0655" size="0.254" layer="100" font="vector">PaJa</text>
</package>
<package name="TP012">
<wire x1="-6.35" y1="-10.16" x2="-6.35" y2="0" width="0.254" layer="21"/>
<wire x1="6.35" y1="0" x2="6.35" y2="-10.16" width="0.254" layer="21"/>
<wire x1="6.35" y1="-10.16" x2="-6.35" y2="-10.16" width="0.254" layer="21"/>
<wire x1="-6.35" y1="0" x2="6.35" y2="0" width="0.254" layer="21" curve="-180"/>
<wire x1="-0.9525" y1="-0.635" x2="0.635" y2="0.9525" width="0.127" layer="51"/>
<wire x1="-0.635" y1="-0.9525" x2="0.9525" y2="0.635" width="0.127" layer="51"/>
<circle x="-2.544" y="-5.084" radius="0.5732" width="0.127" layer="102"/>
<circle x="2.544" y="-5.084" radius="0.5732" width="0.127" layer="102"/>
<circle x="0" y="4.7625" radius="0.5724" width="0.127" layer="102"/>
<circle x="0" y="0" radius="5.3975" width="0.127" layer="51"/>
<circle x="0" y="0" radius="1.27" width="0.127" layer="51"/>
<pad name="2" x="2.54" y="-5.08" drill="1.016" diameter="2.54" shape="octagon"/>
<pad name="1" x="-2.54" y="-5.08" drill="1.016" diameter="2.54" shape="octagon"/>
<pad name="3" x="0" y="4.7625" drill="1.016" diameter="2.54" shape="octagon"/>
<text x="-3.175" y="-3.175" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<text x="-2.8575" y="1.5875" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-0.477" y="3.1845" size="0.254" layer="100" font="vector">PaJa</text>
</package>
<package name="TP062">
<wire x1="7.9375" y1="-11.7475" x2="-7.9375" y2="-11.7475" width="0.254" layer="21"/>
<wire x1="-7.9375" y1="0.3175" x2="7.9375" y2="0.3175" width="0.254" layer="21" curve="-180"/>
<wire x1="-7.9375" y1="-11.7475" x2="-7.9375" y2="0.3175" width="0.254" layer="21"/>
<wire x1="-1.5875" y1="-0.3175" x2="1.27" y2="0.9525" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-0.9525" x2="1.5875" y2="0.3175" width="0.127" layer="51"/>
<wire x1="7.9375" y1="-11.7475" x2="7.9375" y2="0.3175" width="0.254" layer="21"/>
<circle x="0" y="0" radius="1.7097" width="0.127" layer="51"/>
<circle x="0" y="6.0325" radius="0.7099" width="0.127" layer="102"/>
<circle x="-5.08" y="-6.985" radius="0.7099" width="0.127" layer="102"/>
<circle x="5.08" y="-6.985" radius="0.7099" width="0.127" layer="102"/>
<circle x="0" y="0" radius="6.985" width="0.127" layer="51"/>
<pad name="2" x="5.08" y="-6.985" drill="1.27" diameter="3.2" shape="octagon"/>
<pad name="1" x="-5.08" y="-6.985" drill="1.27" diameter="3.2" shape="octagon"/>
<pad name="3" x="0" y="6.0325" drill="1.27" diameter="3.2" shape="octagon"/>
<text x="-5.08" y="-4.445" size="1.9304" layer="27" font="vector">&gt;VALUE</text>
<text x="-4.1275" y="2.2225" size="1.9304" layer="25" font="vector">&gt;NAME</text>
<text x="-0.477" y="-11.4205" size="0.254" layer="100" font="vector">PaJa</text>
</package>
</packages>
<symbols>
<symbol name="D">
<wire x1="2.3812" y1="1.27" x2="2.3812" y2="0" width="0.254" layer="94"/>
<wire x1="2.3812" y1="0" x2="2.3812" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0.1587" y1="-1.27" x2="0.1587" y2="1.27" width="0.254" layer="94"/>
<wire x1="0.1587" y1="1.27" x2="2.3812" y2="0" width="0.254" layer="94"/>
<wire x1="2.3812" y1="0" x2="0.1587" y2="-1.27" width="0.254" layer="94"/>
<text x="-0.0001" y="-1.905" size="1.6764" layer="96" rot="MR180">&gt;Value</text>
<text x="0.3173" y="0.4759" size="0.254" layer="100" rot="R270">PaJa</text>
<text x="0" y="1.905" size="1.6764" layer="95">&gt;Part</text>
<pin name="A" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="K" x="5.08" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="C">
<wire x1="-2.54" y1="0" x2="-2.0638" y2="0" width="0.152" layer="94"/>
<wire x1="-0.4763" y1="0" x2="0" y2="0" width="0.152" layer="94"/>
<text x="-1.111" y="-0.479" size="0.254" layer="100" rot="R90">PaJa</text>
<text x="0.3175" y="0.635" size="1.6764" layer="95">&gt;Name</text>
<text x="0.3175" y="-0.635" size="1.6764" layer="96" rot="MR180">&gt;Value</text>
<rectangle x1="-2.2225" y1="-1.905" x2="-1.5875" y2="1.905" layer="94"/>
<rectangle x1="-0.9525" y1="-1.905" x2="-0.3175" y2="1.905" layer="94"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="2.54" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="TRIMR">
<wire x1="-2.54" y1="0.9525" x2="2.54" y2="0.9525" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.9525" x2="2.54" y2="-0.9525" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.9525" x2="-2.54" y2="-0.9525" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.9525" x2="-2.54" y2="0.9525" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="-2.2225" y2="-2.2225" width="0.155" layer="94"/>
<wire x1="-2.8575" y1="-1.5875" x2="-1.5875" y2="-2.8575" width="0.155" layer="94"/>
<text x="2.3815" y="-0.476" size="0.254" layer="100" rot="R90">PaJa</text>
<text x="-4.7625" y="1.5875" size="1.6764" layer="95">&gt;Name</text>
<text x="0.3175" y="-1.5875" size="1.6764" layer="96" rot="MR180">&gt;Value</text>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="3" x="2.54" y="2.54" visible="off" length="point" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="1N4007" prefix="D">
<description>&lt;B&gt;Usmernovaci dioda&lt;/B&gt; - 1A, 1000V</description>
<gates>
<gate name="D" symbol="D" x="-40.64" y="35.56" swaplevel="1"/>
</gates>
<devices>
<device name="_10" package="DO41">
<connects>
<connect gate="D" pin="A" pad="A"/>
<connect gate="D" pin="K" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_SM-1" package="SM-1">
<connects>
<connect gate="D" pin="A" pad="A"/>
<connect gate="D" pin="K" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="C-KERAMIK" prefix="C" uservalue="yes">
<description>&lt;b&gt;Kondenzator - keramicky&lt;/b&gt;</description>
<gates>
<gate name="C" symbol="C" x="0" y="0" swaplevel="1"/>
</gates>
<devices>
<device name="_2,5" package="C-2,5">
<connects>
<connect gate="C" pin="1" pad="1"/>
<connect gate="C" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_5" package="C-5">
<connects>
<connect gate="C" pin="1" pad="1"/>
<connect gate="C" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_7,5" package="C-7,5">
<connects>
<connect gate="C" pin="1" pad="1"/>
<connect gate="C" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_10" package="C-10">
<connects>
<connect gate="C" pin="1" pad="1"/>
<connect gate="C" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_SMD_1206" package="1206">
<connects>
<connect gate="C" pin="1" pad="1"/>
<connect gate="C" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_SMD_0805" package="0805">
<connects>
<connect gate="C" pin="1" pad="1"/>
<connect gate="C" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TRIMR" prefix="P" uservalue="yes">
<description>&lt;B&gt;Trimr&lt;/B&gt;</description>
<gates>
<gate name="TRIMR" symbol="TRIMR" x="0" y="0" swaplevel="2"/>
</gates>
<devices>
<device name="64P" package="64P">
<connects>
<connect gate="TRIMR" pin="1" pad="1"/>
<connect gate="TRIMR" pin="2" pad="2"/>
<connect gate="TRIMR" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="64W" package="64W">
<connects>
<connect gate="TRIMR" pin="1" pad="1"/>
<connect gate="TRIMR" pin="2" pad="2"/>
<connect gate="TRIMR" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="64X" package="64X">
<connects>
<connect gate="TRIMR" pin="1" pad="1"/>
<connect gate="TRIMR" pin="2" pad="2"/>
<connect gate="TRIMR" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="64Y" package="64Y">
<connects>
<connect gate="TRIMR" pin="1" pad="1"/>
<connect gate="TRIMR" pin="2" pad="2"/>
<connect gate="TRIMR" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="64Z" package="64Z">
<connects>
<connect gate="TRIMR" pin="1" pad="1"/>
<connect gate="TRIMR" pin="2" pad="2"/>
<connect gate="TRIMR" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PM19" package="PM19">
<connects>
<connect gate="TRIMR" pin="1" pad="A"/>
<connect gate="TRIMR" pin="2" pad="B"/>
<connect gate="TRIMR" pin="3" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PT10H" package="PT10H">
<connects>
<connect gate="TRIMR" pin="1" pad="1"/>
<connect gate="TRIMR" pin="2" pad="2"/>
<connect gate="TRIMR" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PT10V" package="PT10V">
<connects>
<connect gate="TRIMR" pin="1" pad="1"/>
<connect gate="TRIMR" pin="2" pad="2"/>
<connect gate="TRIMR" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PT15H" package="PT15H">
<connects>
<connect gate="TRIMR" pin="1" pad="1"/>
<connect gate="TRIMR" pin="2" pad="2"/>
<connect gate="TRIMR" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PT15V" package="PT15V">
<connects>
<connect gate="TRIMR" pin="1" pad="1"/>
<connect gate="TRIMR" pin="2" pad="2"/>
<connect gate="TRIMR" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PT6H" package="PT6H">
<connects>
<connect gate="TRIMR" pin="1" pad="1"/>
<connect gate="TRIMR" pin="2" pad="2"/>
<connect gate="TRIMR" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PT6V" package="PT6V">
<connects>
<connect gate="TRIMR" pin="1" pad="1"/>
<connect gate="TRIMR" pin="2" pad="2"/>
<connect gate="TRIMR" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TP011" package="TP011">
<connects>
<connect gate="TRIMR" pin="1" pad="1"/>
<connect gate="TRIMR" pin="2" pad="2"/>
<connect gate="TRIMR" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TP012" package="TP012">
<connects>
<connect gate="TRIMR" pin="1" pad="1"/>
<connect gate="TRIMR" pin="2" pad="2"/>
<connect gate="TRIMR" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TP062" package="TP062">
<connects>
<connect gate="TRIMR" pin="1" pad="1"/>
<connect gate="TRIMR" pin="2" pad="2"/>
<connect gate="TRIMR" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="rcl">
<packages>
<package name="C0402">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="C0504">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.294" y1="0.559" x2="0.294" y2="0.559" width="0.1016" layer="51"/>
<wire x1="-0.294" y1="-0.559" x2="0.294" y2="-0.559" width="0.1016" layer="51"/>
<smd name="1" x="-0.7" y="0" dx="1" dy="1.3" layer="1"/>
<smd name="2" x="0.7" y="0" dx="1" dy="1.3" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.6604" y1="-0.6223" x2="-0.2804" y2="0.6276" layer="51"/>
<rectangle x1="0.2794" y1="-0.6223" x2="0.6594" y2="0.6276" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="C0603">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="C0805">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.381" y1="0.66" x2="0.381" y2="0.66" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.66" x2="0.381" y2="-0.66" width="0.1016" layer="51"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0922" y1="-0.7239" x2="-0.3421" y2="0.7262" layer="51"/>
<rectangle x1="0.3556" y1="-0.7239" x2="1.1057" y2="0.7262" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="C1005">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="C1206">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="C1210">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-0.9652" y1="1.2446" x2="0.9652" y2="1.2446" width="0.1016" layer="51"/>
<wire x1="-0.9652" y1="-1.2446" x2="0.9652" y2="-1.2446" width="0.1016" layer="51"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-1.2954" x2="-0.9517" y2="1.3045" layer="51"/>
<rectangle x1="0.9517" y1="-1.3045" x2="1.7018" y2="1.2954" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="C1310">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.294" y1="0.559" x2="0.294" y2="0.559" width="0.1016" layer="51"/>
<wire x1="-0.294" y1="-0.559" x2="0.294" y2="-0.559" width="0.1016" layer="51"/>
<smd name="1" x="-0.7" y="0" dx="1" dy="1.3" layer="1"/>
<smd name="2" x="0.7" y="0" dx="1" dy="1.3" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.6604" y1="-0.6223" x2="-0.2804" y2="0.6276" layer="51"/>
<rectangle x1="0.2794" y1="-0.6223" x2="0.6594" y2="0.6276" layer="51"/>
<rectangle x1="-0.1001" y1="-0.3" x2="0.1001" y2="0.3" layer="35"/>
</package>
<package name="C1608">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="C1812">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.973" y1="1.983" x2="2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-1.983" x2="-2.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-1.983" x2="-2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="-1.4732" y1="1.6002" x2="1.4732" y2="1.6002" width="0.1016" layer="51"/>
<wire x1="-1.4478" y1="-1.6002" x2="1.4732" y2="-1.6002" width="0.1016" layer="51"/>
<wire x1="2.973" y1="1.983" x2="2.973" y2="-1.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<text x="-1.905" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.3876" y1="-1.651" x2="-1.4376" y2="1.649" layer="51"/>
<rectangle x1="1.4478" y1="-1.651" x2="2.3978" y2="1.649" layer="51"/>
<rectangle x1="-0.3" y1="-0.4001" x2="0.3" y2="0.4001" layer="35"/>
</package>
<package name="C1825">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.973" y1="3.483" x2="2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-3.483" x2="-2.973" y2="-3.483" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-3.483" x2="-2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="-1.4986" y1="3.2766" x2="1.4732" y2="3.2766" width="0.1016" layer="51"/>
<wire x1="-1.4732" y1="-3.2766" x2="1.4986" y2="-3.2766" width="0.1016" layer="51"/>
<wire x1="2.973" y1="3.483" x2="2.973" y2="-3.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<text x="-1.905" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.413" y1="-3.3528" x2="-1.463" y2="3.3472" layer="51"/>
<rectangle x1="1.4478" y1="-3.3528" x2="2.3978" y2="3.3472" layer="51"/>
<rectangle x1="-0.7" y1="-0.7" x2="0.7" y2="0.7" layer="35"/>
</package>
<package name="C2012">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.381" y1="0.66" x2="0.381" y2="0.66" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.66" x2="0.381" y2="-0.66" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0922" y1="-0.7239" x2="-0.3421" y2="0.7262" layer="51"/>
<rectangle x1="0.3556" y1="-0.7239" x2="1.1057" y2="0.7262" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="C3216">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.3" y1="-0.5001" x2="0.3" y2="0.5001" layer="35"/>
</package>
<package name="C3225">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-0.9652" y1="1.2446" x2="0.9652" y2="1.2446" width="0.1016" layer="51"/>
<wire x1="-0.9652" y1="-1.2446" x2="0.9652" y2="-1.2446" width="0.1016" layer="51"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-1.2954" x2="-0.9517" y2="1.3045" layer="51"/>
<rectangle x1="0.9517" y1="-1.3045" x2="1.7018" y2="1.2954" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="C4532">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.973" y1="1.983" x2="2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-1.983" x2="-2.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-1.983" x2="-2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="-1.4732" y1="1.6002" x2="1.4732" y2="1.6002" width="0.1016" layer="51"/>
<wire x1="-1.4478" y1="-1.6002" x2="1.4732" y2="-1.6002" width="0.1016" layer="51"/>
<wire x1="2.973" y1="1.983" x2="2.973" y2="-1.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<text x="-1.905" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.3876" y1="-1.651" x2="-1.4376" y2="1.649" layer="51"/>
<rectangle x1="1.4478" y1="-1.651" x2="2.3978" y2="1.649" layer="51"/>
<rectangle x1="-0.4001" y1="-0.7" x2="0.4001" y2="0.7" layer="35"/>
</package>
<package name="C4564">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.973" y1="3.483" x2="2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-3.483" x2="-2.973" y2="-3.483" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-3.483" x2="-2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="-1.4986" y1="3.2766" x2="1.4732" y2="3.2766" width="0.1016" layer="51"/>
<wire x1="-1.4732" y1="-3.2766" x2="1.4986" y2="-3.2766" width="0.1016" layer="51"/>
<wire x1="2.973" y1="3.483" x2="2.973" y2="-3.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<text x="-1.905" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.413" y1="-3.3528" x2="-1.463" y2="3.3472" layer="51"/>
<rectangle x1="1.4478" y1="-3.3528" x2="2.3978" y2="3.3472" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="C025-024X044">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 2.4 x 4.4 mm</description>
<wire x1="-2.159" y1="-0.635" x2="-2.159" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="0.635" x2="-1.651" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.159" y1="-0.635" x2="-1.651" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="1.143" x2="-1.651" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-0.635" x2="2.159" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.651" y1="-1.143" x2="-1.651" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="1.651" y1="1.143" x2="2.159" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="1.651" y1="-1.143" x2="2.159" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="-0.3048" y1="0.762" x2="-0.3048" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0.762" x2="0.3302" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="1.27" y1="0" x2="0.3302" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="0" x2="-0.3048" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.778" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.778" y="-2.667" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-025X050">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 2.5 x 5 mm</description>
<wire x1="-2.159" y1="1.27" x2="2.159" y2="1.27" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.27" x2="-2.159" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.27" x2="2.413" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.016" x2="-2.159" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.27" x2="2.413" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.016" x2="-2.159" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="1.524" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-030X050">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 3 x 5 mm</description>
<wire x1="-2.159" y1="1.524" x2="2.159" y2="1.524" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.524" x2="-2.159" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.27" x2="2.413" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.27" x2="-2.413" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.524" x2="2.413" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.27" x2="-2.159" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.524" x2="2.413" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.27" x2="-2.159" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.048" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-040X050">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 4 x 5 mm</description>
<wire x1="-2.159" y1="1.905" x2="2.159" y2="1.905" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.905" x2="-2.159" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.651" x2="2.413" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.651" x2="-2.413" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.905" x2="2.413" y2="1.651" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.651" x2="-2.159" y2="1.905" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.905" x2="2.413" y2="-1.651" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.651" x2="-2.159" y2="-1.905" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.429" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-050X050">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 5 x 5 mm</description>
<wire x1="-2.159" y1="2.286" x2="2.159" y2="2.286" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-2.286" x2="-2.159" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="2.413" y1="2.032" x2="2.413" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="2.032" x2="-2.413" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="2.159" y1="2.286" x2="2.413" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="2.032" x2="-2.159" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-2.286" x2="2.413" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-2.032" x2="-2.159" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="2.54" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.81" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-060X050">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 6 x 5 mm</description>
<wire x1="-2.159" y1="2.794" x2="2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-2.794" x2="-2.159" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="2.413" y1="2.54" x2="2.413" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="2.54" x2="-2.413" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.159" y1="2.794" x2="2.413" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="2.54" x2="-2.159" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-2.794" x2="2.413" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-2.54" x2="-2.159" y2="-2.794" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="3.048" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.032" y="-2.413" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-024X070">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm + 5 mm, outline 2.4 x 7 mm</description>
<wire x1="-2.159" y1="-0.635" x2="-2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-2.159" y1="0.635" x2="-1.651" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.159" y1="-0.635" x2="-1.651" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="1.143" x2="-1.651" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-0.635" x2="2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-1.143" x2="-1.651" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="1.651" y1="1.143" x2="2.159" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.191" y1="-1.143" x2="-3.9624" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-4.191" y1="1.143" x2="-3.9624" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="-0.635" x2="-4.191" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="-1.143" x2="2.159" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.699" y1="0.635" x2="-4.191" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.699" y1="-0.635" x2="-4.699" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="1.143" x2="-2.5654" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.143" x2="-2.5654" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-0.3048" y1="0.762" x2="-0.3048" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0.762" x2="0.3302" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="1.27" y1="0" x2="0.3302" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="0" x2="-0.3048" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.81" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-2.667" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-025X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 + 5 mm, outline 2.5 x 7.5 mm</description>
<wire x1="-2.159" y1="1.27" x2="2.159" y2="1.27" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.27" x2="-2.159" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.27" x2="2.413" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.016" x2="-2.159" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.27" x2="2.413" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.016" x2="-2.159" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="4.953" y1="1.016" x2="4.953" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="4.699" y1="1.27" x2="4.953" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-1.27" x2="4.953" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="2.794" y1="1.27" x2="4.699" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-1.27" x2="2.794" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.413" y2="0.762" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-0.762" x2="2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="0.254" x2="2.413" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0" x2="2.286" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="2.794" y2="0" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="3.302" y2="0" width="0.1524" layer="51"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.159" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.159" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-035X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 + 5 mm, outline 3.5 x 7.5 mm</description>
<wire x1="-2.159" y1="1.778" x2="2.159" y2="1.778" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.778" x2="-2.159" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.524" x2="-2.413" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.778" x2="2.413" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.524" x2="-2.159" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.778" x2="2.413" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.524" x2="-2.159" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="4.953" y1="1.524" x2="4.953" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.699" y1="1.778" x2="4.953" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-1.778" x2="4.953" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="2.794" y1="1.778" x2="4.699" y2="1.778" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-1.778" x2="2.794" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.524" x2="2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.413" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="2.413" y1="0.508" x2="2.413" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="0.381" y1="0" x2="0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="2.794" y2="0" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="3.302" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="1.778" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.302" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-045X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 + 5 mm, outline 4.5 x 7.5 mm</description>
<wire x1="-2.159" y1="2.286" x2="2.159" y2="2.286" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-2.286" x2="-2.159" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="2.032" x2="-2.413" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="2.159" y1="2.286" x2="2.413" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="2.032" x2="-2.159" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-2.286" x2="2.413" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-2.032" x2="-2.159" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="4.953" y1="2.032" x2="4.953" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.286" x2="4.953" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-2.286" x2="4.953" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="2.794" y1="2.286" x2="4.699" y2="2.286" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.286" x2="2.794" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="2.413" y1="2.032" x2="2.413" y2="1.397" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.397" x2="2.413" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="2.413" y1="0.762" x2="2.413" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="2.286" y1="0" x2="2.794" y2="0" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="3.302" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="1.778" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="2.667" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.81" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-055X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 + 5 mm, outline 5.5 x 7.5 mm</description>
<wire x1="-2.159" y1="2.794" x2="2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-2.794" x2="-2.159" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="2.54" x2="-2.413" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.159" y1="2.794" x2="2.413" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="2.54" x2="-2.159" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-2.794" x2="2.413" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-2.54" x2="-2.159" y2="-2.794" width="0.1524" layer="21" curve="90"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="4.953" y1="2.54" x2="4.953" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.794" x2="4.953" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-2.794" x2="4.953" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="2.794" y1="2.794" x2="4.699" y2="2.794" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.794" x2="2.794" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="2.413" y1="2.54" x2="2.413" y2="2.032" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-2.032" x2="2.413" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.413" y1="0.762" x2="2.413" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0" x2="2.286" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="2.794" y2="0" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="3.302" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.032" y="-2.286" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-024X044">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 2.4 x 4.4 mm</description>
<wire x1="-2.159" y1="-0.635" x2="-2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-2.159" y1="0.635" x2="-1.651" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.159" y1="-0.635" x2="-1.651" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="1.143" x2="-1.651" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-0.635" x2="2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-1.143" x2="-1.651" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="1.651" y1="1.143" x2="2.159" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="1.651" y1="-1.143" x2="2.159" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="-0.3048" y1="0.762" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0.762" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="1.27" y1="0" x2="0.3302" y2="0" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0" x2="-0.3048" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.159" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.159" y="-2.667" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="2.159" y1="-0.381" x2="2.54" y2="0.381" layer="51"/>
<rectangle x1="-2.54" y1="-0.381" x2="-2.159" y2="0.381" layer="51"/>
</package>
<package name="C050-025X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 2.5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="1.016" x2="-3.683" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.27" x2="3.429" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-1.016" x2="3.683" y2="1.016" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.27" x2="-3.429" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.27" x2="3.683" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-1.27" x2="3.683" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-1.016" x2="-3.429" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="1.016" x2="-3.429" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.429" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-045X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 4.5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="2.032" x2="-3.683" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-2.286" x2="3.429" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-2.032" x2="3.683" y2="2.032" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.286" x2="-3.429" y2="2.286" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.286" x2="3.683" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-2.286" x2="3.683" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-2.032" x2="-3.429" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="2.032" x2="-3.429" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.556" y="2.667" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.556" y="-3.81" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-030X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 3 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="1.27" x2="-3.683" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.524" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-1.27" x2="3.683" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.524" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.524" x2="3.683" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-1.524" x2="3.683" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-1.27" x2="-3.429" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="1.27" x2="-3.429" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.556" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.556" y="-3.048" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-050X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="2.286" x2="-3.683" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-2.54" x2="3.429" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-2.286" x2="3.683" y2="2.286" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.54" x2="-3.429" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.54" x2="3.683" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-2.54" x2="3.683" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-2.286" x2="-3.429" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="2.286" x2="-3.429" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.429" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-2.159" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-055X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 5.5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="2.54" x2="-3.683" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-2.794" x2="3.429" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-2.54" x2="3.683" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.794" x2="-3.429" y2="2.794" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.794" x2="3.683" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-2.794" x2="3.683" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-2.54" x2="-3.429" y2="-2.794" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="2.54" x2="-3.429" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.429" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.302" y="-2.286" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-075X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 7.5 x 7.5 mm</description>
<wire x1="-1.524" y1="0" x2="-0.4572" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.4572" y1="0" x2="-0.4572" y2="0.762" width="0.4064" layer="21"/>
<wire x1="-0.4572" y1="0" x2="-0.4572" y2="-0.762" width="0.4064" layer="21"/>
<wire x1="0.4318" y1="0.762" x2="0.4318" y2="0" width="0.4064" layer="21"/>
<wire x1="0.4318" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.4318" y1="0" x2="0.4318" y2="-0.762" width="0.4064" layer="21"/>
<wire x1="-3.683" y1="3.429" x2="-3.683" y2="-3.429" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-3.683" x2="3.429" y2="-3.683" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-3.429" x2="3.683" y2="3.429" width="0.1524" layer="21"/>
<wire x1="3.429" y1="3.683" x2="-3.429" y2="3.683" width="0.1524" layer="21"/>
<wire x1="3.429" y1="3.683" x2="3.683" y2="3.429" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-3.683" x2="3.683" y2="-3.429" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-3.429" x2="-3.429" y2="-3.683" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="3.429" x2="-3.429" y2="3.683" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.429" y="4.064" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-2.921" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050H075X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
Horizontal, grid 5 mm, outline 7.5 x 7.5 mm</description>
<wire x1="-3.683" y1="7.112" x2="-3.683" y2="0.508" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="0.508" x2="-3.302" y2="0.508" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="0.508" x2="-1.778" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-1.778" y1="0.508" x2="1.778" y2="0.508" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.508" x2="3.302" y2="0.508" width="0.1524" layer="51"/>
<wire x1="3.302" y1="0.508" x2="3.683" y2="0.508" width="0.1524" layer="21"/>
<wire x1="3.683" y1="0.508" x2="3.683" y2="7.112" width="0.1524" layer="21"/>
<wire x1="3.175" y1="7.62" x2="-3.175" y2="7.62" width="0.1524" layer="21"/>
<wire x1="-0.3048" y1="2.413" x2="-0.3048" y2="1.778" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="1.778" x2="-0.3048" y2="1.143" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="1.778" x2="-1.651" y2="1.778" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="2.413" x2="0.3302" y2="1.778" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="1.778" x2="0.3302" y2="1.143" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="1.778" x2="1.651" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="7.112" x2="-3.175" y2="7.62" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.175" y1="7.62" x2="3.683" y2="7.112" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="0.254" width="0.508" layer="51"/>
<wire x1="2.54" y1="0" x2="2.54" y2="0.254" width="0.508" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.302" y="8.001" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="3.175" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.794" y1="0.127" x2="-2.286" y2="0.508" layer="51"/>
<rectangle x1="2.286" y1="0.127" x2="2.794" y2="0.508" layer="51"/>
</package>
<package name="C075-032X103">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 3.2 x 10.3 mm</description>
<wire x1="4.826" y1="1.524" x2="-4.826" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="-1.524" x2="4.826" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.826" y1="1.524" x2="5.08" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.826" y1="-1.524" x2="5.08" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="-1.27" x2="-4.826" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.27" x2="-4.826" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="0.508" y1="0" x2="2.54" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0" x2="-0.508" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="0.889" x2="-0.508" y2="0" width="0.4064" layer="21"/>
<wire x1="-0.508" y1="0" x2="-0.508" y2="-0.889" width="0.4064" layer="21"/>
<wire x1="0.508" y1="0.889" x2="0.508" y2="0" width="0.4064" layer="21"/>
<wire x1="0.508" y1="0" x2="0.508" y2="-0.889" width="0.4064" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.826" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.826" y="-3.048" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C075-042X103">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 4.2 x 10.3 mm</description>
<wire x1="4.826" y1="2.032" x2="-4.826" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.778" x2="-5.08" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="-2.032" x2="4.826" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.778" x2="5.08" y2="1.778" width="0.1524" layer="21"/>
<wire x1="4.826" y1="2.032" x2="5.08" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.826" y1="-2.032" x2="5.08" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="-1.778" x2="-4.826" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.778" x2="-4.826" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.27" y1="0" x2="2.667" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="0" x2="-2.159" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="1.27" x2="-2.159" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.159" y1="0" x2="-2.159" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.4064" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.699" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.635" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C075-052X106">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 5.2 x 10.6 mm</description>
<wire x1="4.953" y1="2.54" x2="-4.953" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="2.286" x2="-5.207" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-2.54" x2="4.953" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.286" x2="5.207" y2="2.286" width="0.1524" layer="21"/>
<wire x1="4.953" y1="2.54" x2="5.207" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.953" y1="-2.54" x2="5.207" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="-2.286" x2="-4.953" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="2.286" x2="-4.953" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.27" y1="0" x2="2.667" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="0" x2="-2.159" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="1.27" x2="-2.159" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.159" y1="0" x2="-2.159" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.4064" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.826" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.635" y="-2.032" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C102-043X133">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 10.2 mm, outline 4.3 x 13.3 mm</description>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="0" width="0.4064" layer="21"/>
<wire x1="3.81" y1="0" x2="-2.286" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.81" y1="0" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-6.096" y1="2.032" x2="6.096" y2="2.032" width="0.1524" layer="21"/>
<wire x1="6.604" y1="1.524" x2="6.604" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-2.032" x2="-6.096" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-1.524" x2="-6.604" y2="1.524" width="0.1524" layer="21"/>
<wire x1="6.096" y1="2.032" x2="6.604" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="-2.032" x2="6.604" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="-1.524" x2="-6.096" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="1.524" x2="-6.096" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.016" shape="octagon"/>
<text x="-6.096" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C102-054X133">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 10.2 mm, outline 5.4 x 13.3 mm</description>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="0" width="0.4064" layer="21"/>
<wire x1="3.81" y1="0" x2="-2.286" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.81" y1="0" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-6.096" y1="2.54" x2="6.096" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.604" y1="2.032" x2="6.604" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-2.54" x2="-6.096" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-2.032" x2="-6.604" y2="2.032" width="0.1524" layer="21"/>
<wire x1="6.096" y1="2.54" x2="6.604" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="-2.54" x2="6.604" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="-2.032" x2="-6.096" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="2.032" x2="-6.096" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.016" shape="octagon"/>
<text x="-6.096" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-1.905" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C102-064X133">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 10.2 mm, outline 6.4 x 13.3 mm</description>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="0" width="0.4064" layer="21"/>
<wire x1="3.81" y1="0" x2="-2.286" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.81" y1="0" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-6.096" y1="3.048" x2="6.096" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.604" y1="2.54" x2="6.604" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-3.048" x2="-6.096" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-2.54" x2="-6.604" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.096" y1="3.048" x2="6.604" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="-3.048" x2="6.604" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="-2.54" x2="-6.096" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="2.54" x2="-6.096" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.016" shape="octagon"/>
<text x="-6.096" y="3.429" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-2.032" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C102_152-062X184">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 10.2 mm + 15.2 mm, outline 6.2 x 18.4 mm</description>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.4064" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.683" y1="0" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="0" x2="3.683" y2="0" width="0.1524" layer="21"/>
<wire x1="6.477" y1="0" x2="8.636" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.096" y1="3.048" x2="6.223" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.223" y1="-3.048" x2="-6.096" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-2.54" x2="-6.604" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.223" y1="3.048" x2="6.731" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.223" y1="-3.048" x2="6.731" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="-2.54" x2="-6.096" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="2.54" x2="-6.096" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.731" y1="2.54" x2="6.731" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="11.176" y1="3.048" x2="11.684" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="11.176" y1="-3.048" x2="11.684" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="11.176" y1="-3.048" x2="7.112" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="7.112" y1="3.048" x2="11.176" y2="3.048" width="0.1524" layer="21"/>
<wire x1="11.684" y1="2.54" x2="11.684" y2="-2.54" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="3" x="10.033" y="0" drill="1.016" shape="octagon"/>
<text x="-5.969" y="3.429" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-2.286" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-054X183">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 5.4 x 18.3 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="2.032" x2="9.017" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-2.54" x2="-8.509" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-2.032" x2="-9.017" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="2.54" x2="8.509" y2="2.54" width="0.1524" layer="21"/>
<wire x1="8.509" y1="2.54" x2="9.017" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-2.54" x2="9.017" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-2.032" x2="-8.509" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="2.032" x2="-8.509" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.382" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.032" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-064X183">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 6.4 x 18.3 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="2.54" x2="9.017" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-3.048" x2="-8.509" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-2.54" x2="-9.017" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="3.048" x2="8.509" y2="3.048" width="0.1524" layer="21"/>
<wire x1="8.509" y1="3.048" x2="9.017" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-3.048" x2="9.017" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-2.54" x2="-8.509" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="2.54" x2="-8.509" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.509" y="3.429" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.032" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-072X183">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 7.2 x 18.3 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="3.048" x2="9.017" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-3.556" x2="-8.509" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-3.048" x2="-9.017" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="3.556" x2="8.509" y2="3.556" width="0.1524" layer="21"/>
<wire x1="8.509" y1="3.556" x2="9.017" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-3.556" x2="9.017" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-3.048" x2="-8.509" y2="-3.556" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="3.048" x2="-8.509" y2="3.556" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.509" y="3.937" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.286" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-084X183">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 8.4 x 18.3 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="3.556" x2="9.017" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-4.064" x2="-8.509" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-3.556" x2="-9.017" y2="3.556" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="4.064" x2="8.509" y2="4.064" width="0.1524" layer="21"/>
<wire x1="8.509" y1="4.064" x2="9.017" y2="3.556" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-4.064" x2="9.017" y2="-3.556" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-3.556" x2="-8.509" y2="-4.064" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="3.556" x2="-8.509" y2="4.064" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.509" y="4.445" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-091X182">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 9.1 x 18.2 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="3.937" x2="9.017" y2="-3.937" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-4.445" x2="-8.509" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-3.937" x2="-9.017" y2="3.937" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="4.445" x2="8.509" y2="4.445" width="0.1524" layer="21"/>
<wire x1="8.509" y1="4.445" x2="9.017" y2="3.937" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-4.445" x2="9.017" y2="-3.937" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-3.937" x2="-8.509" y2="-4.445" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="3.937" x2="-8.509" y2="4.445" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.509" y="4.826" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-062X268">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 6.2 x 26.8 mm</description>
<wire x1="-12.827" y1="3.048" x2="12.827" y2="3.048" width="0.1524" layer="21"/>
<wire x1="13.335" y1="2.54" x2="13.335" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-3.048" x2="-12.827" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-2.54" x2="-13.335" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="3.048" x2="13.335" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-3.048" x2="13.335" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-2.54" x2="-12.827" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="2.54" x2="-12.827" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.7" y="3.429" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-074X268">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 7.4 x 26.8 mm</description>
<wire x1="-12.827" y1="3.556" x2="12.827" y2="3.556" width="0.1524" layer="21"/>
<wire x1="13.335" y1="3.048" x2="13.335" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-3.556" x2="-12.827" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-3.048" x2="-13.335" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="3.556" x2="13.335" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-3.556" x2="13.335" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-3.048" x2="-12.827" y2="-3.556" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="3.048" x2="-12.827" y2="3.556" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.827" y="3.937" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-087X268">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 8.7 x 26.8 mm</description>
<wire x1="-12.827" y1="4.318" x2="12.827" y2="4.318" width="0.1524" layer="21"/>
<wire x1="13.335" y1="3.81" x2="13.335" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-4.318" x2="-12.827" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-3.81" x2="-13.335" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="4.318" x2="13.335" y2="3.81" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-4.318" x2="13.335" y2="-3.81" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-3.81" x2="-12.827" y2="-4.318" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="3.81" x2="-12.827" y2="4.318" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.827" y="4.699" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-108X268">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 10.8 x 26.8 mm</description>
<wire x1="-12.827" y1="5.334" x2="12.827" y2="5.334" width="0.1524" layer="21"/>
<wire x1="13.335" y1="4.826" x2="13.335" y2="-4.826" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-5.334" x2="-12.827" y2="-5.334" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-4.826" x2="-13.335" y2="4.826" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="5.334" x2="13.335" y2="4.826" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-5.334" x2="13.335" y2="-4.826" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-4.826" x2="-12.827" y2="-5.334" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="4.826" x2="-12.827" y2="5.334" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.954" y="5.715" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-113X268">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 11.3 x 26.8 mm</description>
<wire x1="-12.827" y1="5.588" x2="12.827" y2="5.588" width="0.1524" layer="21"/>
<wire x1="13.335" y1="5.08" x2="13.335" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-5.588" x2="-12.827" y2="-5.588" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-5.08" x2="-13.335" y2="5.08" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="5.588" x2="13.335" y2="5.08" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-5.588" x2="13.335" y2="-5.08" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-5.08" x2="-12.827" y2="-5.588" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="5.08" x2="-12.827" y2="5.588" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.954" y="5.969" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-093X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 9.3 x 31.6 mm</description>
<wire x1="-15.24" y1="4.572" x2="15.24" y2="4.572" width="0.1524" layer="21"/>
<wire x1="15.748" y1="4.064" x2="15.748" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-4.572" x2="-15.24" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-4.064" x2="-15.748" y2="4.064" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="4.572" x2="15.748" y2="4.064" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-4.572" x2="15.748" y2="-4.064" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-4.064" x2="-15.24" y2="-4.572" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="4.064" x2="-15.24" y2="4.572" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="4.953" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-113X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 11.3 x 31.6 mm</description>
<wire x1="-15.24" y1="5.588" x2="15.24" y2="5.588" width="0.1524" layer="21"/>
<wire x1="15.748" y1="5.08" x2="15.748" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-5.588" x2="-15.24" y2="-5.588" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-5.08" x2="-15.748" y2="5.08" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="5.588" x2="15.748" y2="5.08" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-5.588" x2="15.748" y2="-5.08" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-5.08" x2="-15.24" y2="-5.588" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="5.08" x2="-15.24" y2="5.588" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="5.969" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-134X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 13.4 x 31.6 mm</description>
<wire x1="-15.24" y1="6.604" x2="15.24" y2="6.604" width="0.1524" layer="21"/>
<wire x1="15.748" y1="6.096" x2="15.748" y2="-6.096" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-6.604" x2="-15.24" y2="-6.604" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-6.096" x2="-15.748" y2="6.096" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="6.604" x2="15.748" y2="6.096" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-6.604" x2="15.748" y2="-6.096" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-6.096" x2="-15.24" y2="-6.604" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="6.096" x2="-15.24" y2="6.604" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="6.985" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-205X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 20.5 x 31.6 mm</description>
<wire x1="-15.24" y1="10.16" x2="15.24" y2="10.16" width="0.1524" layer="21"/>
<wire x1="15.748" y1="9.652" x2="15.748" y2="-9.652" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-10.16" x2="-15.24" y2="-10.16" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-9.652" x2="-15.748" y2="9.652" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="10.16" x2="15.748" y2="9.652" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-10.16" x2="15.748" y2="-9.652" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-9.652" x2="-15.24" y2="-10.16" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="9.652" x2="-15.24" y2="10.16" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="10.541" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-4.318" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C325-137X374">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 32.5 mm, outline 13.7 x 37.4 mm</description>
<wire x1="-14.2748" y1="0" x2="-12.7" y2="0" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.7" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="1.905" x2="-11.811" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="0" x2="14.2748" y2="0" width="0.1524" layer="21"/>
<wire x1="-11.811" y1="0" x2="-11.811" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-12.7" y1="0" x2="-12.7" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="18.542" y1="6.731" x2="18.542" y2="-6.731" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="6.731" x2="-18.542" y2="-6.731" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="-6.731" x2="18.542" y2="-6.731" width="0.1524" layer="21"/>
<wire x1="18.542" y1="6.731" x2="-18.542" y2="6.731" width="0.1524" layer="21"/>
<pad name="1" x="-16.256" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="16.256" y="0" drill="1.1938" shape="octagon"/>
<text x="-18.2372" y="7.0612" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-10.8458" y="-2.8702" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C325-162X374">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 32.5 mm, outline 16.2 x 37.4 mm</description>
<wire x1="-14.2748" y1="0" x2="-12.7" y2="0" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.7" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="1.905" x2="-11.811" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="0" x2="14.2748" y2="0" width="0.1524" layer="21"/>
<wire x1="-11.811" y1="0" x2="-11.811" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-12.7" y1="0" x2="-12.7" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="18.542" y1="8.001" x2="18.542" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="8.001" x2="-18.542" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="-8.001" x2="18.542" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="18.542" y1="8.001" x2="-18.542" y2="8.001" width="0.1524" layer="21"/>
<pad name="1" x="-16.256" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="16.256" y="0" drill="1.1938" shape="octagon"/>
<text x="-18.3642" y="8.3312" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-10.8458" y="-2.8702" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C325-182X374">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 32.5 mm, outline 18.2 x 37.4 mm</description>
<wire x1="-14.2748" y1="0" x2="-12.7" y2="0" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.7" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="1.905" x2="-11.811" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="0" x2="14.2748" y2="0" width="0.1524" layer="21"/>
<wire x1="-11.811" y1="0" x2="-11.811" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-12.7" y1="0" x2="-12.7" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="18.542" y1="9.017" x2="18.542" y2="-9.017" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="9.017" x2="-18.542" y2="-9.017" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="-9.017" x2="18.542" y2="-9.017" width="0.1524" layer="21"/>
<wire x1="18.542" y1="9.017" x2="-18.542" y2="9.017" width="0.1524" layer="21"/>
<pad name="1" x="-16.256" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="16.256" y="0" drill="1.1938" shape="octagon"/>
<text x="-18.3642" y="9.3472" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-10.8458" y="-2.8702" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C375-192X418">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 37.5 mm, outline 19.2 x 41.8 mm</description>
<wire x1="-20.32" y1="8.509" x2="20.32" y2="8.509" width="0.1524" layer="21"/>
<wire x1="20.828" y1="8.001" x2="20.828" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-8.509" x2="-20.32" y2="-8.509" width="0.1524" layer="21"/>
<wire x1="-20.828" y1="-8.001" x2="-20.828" y2="8.001" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="20.32" y1="8.509" x2="20.828" y2="8.001" width="0.1524" layer="21" curve="-90"/>
<wire x1="20.32" y1="-8.509" x2="20.828" y2="-8.001" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="-8.001" x2="-20.32" y2="-8.509" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="8.001" x2="-20.32" y2="8.509" width="0.1524" layer="21" curve="-90"/>
<wire x1="-16.002" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="16.002" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-18.796" y="0" drill="1.3208" shape="octagon"/>
<pad name="2" x="18.796" y="0" drill="1.3208" shape="octagon"/>
<text x="-20.447" y="8.89" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C375-203X418">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 37.5 mm, outline 20.3 x 41.8 mm</description>
<wire x1="-20.32" y1="10.16" x2="20.32" y2="10.16" width="0.1524" layer="21"/>
<wire x1="20.828" y1="9.652" x2="20.828" y2="-9.652" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-10.16" x2="-20.32" y2="-10.16" width="0.1524" layer="21"/>
<wire x1="-20.828" y1="-9.652" x2="-20.828" y2="9.652" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="20.32" y1="10.16" x2="20.828" y2="9.652" width="0.1524" layer="21" curve="-90"/>
<wire x1="20.32" y1="-10.16" x2="20.828" y2="-9.652" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="-9.652" x2="-20.32" y2="-10.16" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="9.652" x2="-20.32" y2="10.16" width="0.1524" layer="21" curve="-90"/>
<wire x1="-16.002" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="16.002" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-18.796" y="0" drill="1.3208" shape="octagon"/>
<pad name="2" x="18.796" y="0" drill="1.3208" shape="octagon"/>
<text x="-20.32" y="10.541" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-035X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 3.5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="1.524" x2="-3.683" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.778" x2="3.429" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-1.524" x2="3.683" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.778" x2="-3.429" y2="1.778" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.778" x2="3.683" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-1.778" x2="3.683" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-1.524" x2="-3.429" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="1.524" x2="-3.429" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.556" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.556" y="-3.429" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C375-155X418">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 37.5 mm, outline 15.5 x 41.8 mm</description>
<wire x1="-20.32" y1="7.62" x2="20.32" y2="7.62" width="0.1524" layer="21"/>
<wire x1="20.828" y1="7.112" x2="20.828" y2="-7.112" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-7.62" x2="-20.32" y2="-7.62" width="0.1524" layer="21"/>
<wire x1="-20.828" y1="-7.112" x2="-20.828" y2="7.112" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="20.32" y1="7.62" x2="20.828" y2="7.112" width="0.1524" layer="21" curve="-90"/>
<wire x1="20.32" y1="-7.62" x2="20.828" y2="-7.112" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="-7.112" x2="-20.32" y2="-7.62" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="7.112" x2="-20.32" y2="7.62" width="0.1524" layer="21" curve="-90"/>
<wire x1="-16.002" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="16.002" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-18.796" y="0" drill="1.3208" shape="octagon"/>
<pad name="2" x="18.796" y="0" drill="1.3208" shape="octagon"/>
<text x="-20.447" y="8.001" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C075-063X106">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 6.3 x 10.6 mm</description>
<wire x1="4.953" y1="3.048" x2="-4.953" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="2.794" x2="-5.207" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-3.048" x2="4.953" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.794" x2="5.207" y2="2.794" width="0.1524" layer="21"/>
<wire x1="4.953" y1="3.048" x2="5.207" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.953" y1="-3.048" x2="5.207" y2="-2.794" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="-2.794" x2="-4.953" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="2.794" x2="-4.953" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.27" y1="0" x2="2.667" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="0" x2="-2.159" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="1.27" x2="-2.159" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.159" y1="0" x2="-2.159" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.4064" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.826" y="3.429" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-154X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 15.4 x 31.6 mm</description>
<wire x1="-15.24" y1="7.62" x2="15.24" y2="7.62" width="0.1524" layer="21"/>
<wire x1="15.748" y1="7.112" x2="15.748" y2="-7.112" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-7.62" x2="-15.24" y2="-7.62" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-7.112" x2="-15.748" y2="7.112" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="7.62" x2="15.748" y2="7.112" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-7.62" x2="15.748" y2="-7.112" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-7.112" x2="-15.24" y2="-7.62" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="7.112" x2="-15.24" y2="7.62" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="8.001" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-173X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 17.3 x 31.6 mm</description>
<wire x1="-15.24" y1="8.509" x2="15.24" y2="8.509" width="0.1524" layer="21"/>
<wire x1="15.748" y1="8.001" x2="15.748" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-8.509" x2="-15.24" y2="-8.509" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-8.001" x2="-15.748" y2="8.001" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="8.509" x2="15.748" y2="8.001" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-8.509" x2="15.748" y2="-8.001" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-8.001" x2="-15.24" y2="-8.509" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="8.001" x2="-15.24" y2="8.509" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="8.89" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C0402K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 0204 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 1005</description>
<wire x1="-0.425" y1="0.2" x2="0.425" y2="0.2" width="0.1016" layer="51"/>
<wire x1="0.425" y1="-0.2" x2="-0.425" y2="-0.2" width="0.1016" layer="51"/>
<smd name="1" x="-0.6" y="0" dx="0.925" dy="0.74" layer="1"/>
<smd name="2" x="0.6" y="0" dx="0.925" dy="0.74" layer="1"/>
<text x="-0.5" y="0.425" size="1.016" layer="25">&gt;NAME</text>
<text x="-0.5" y="-1.45" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.5" y1="-0.25" x2="-0.225" y2="0.25" layer="51"/>
<rectangle x1="0.225" y1="-0.25" x2="0.5" y2="0.25" layer="51"/>
</package>
<package name="C0603K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 0603 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 1608</description>
<wire x1="-0.725" y1="0.35" x2="0.725" y2="0.35" width="0.1016" layer="51"/>
<wire x1="0.725" y1="-0.35" x2="-0.725" y2="-0.35" width="0.1016" layer="51"/>
<smd name="1" x="-0.875" y="0" dx="1.05" dy="1.08" layer="1"/>
<smd name="2" x="0.875" y="0" dx="1.05" dy="1.08" layer="1"/>
<text x="-0.8" y="0.65" size="1.016" layer="25">&gt;NAME</text>
<text x="-0.8" y="-1.65" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8" y1="-0.4" x2="-0.45" y2="0.4" layer="51"/>
<rectangle x1="0.45" y1="-0.4" x2="0.8" y2="0.4" layer="51"/>
</package>
<package name="C0805K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 0805 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 2012</description>
<wire x1="-0.925" y1="0.6" x2="0.925" y2="0.6" width="0.1016" layer="51"/>
<wire x1="0.925" y1="-0.6" x2="-0.925" y2="-0.6" width="0.1016" layer="51"/>
<smd name="1" x="-1" y="0" dx="1.3" dy="1.6" layer="1"/>
<smd name="2" x="1" y="0" dx="1.3" dy="1.6" layer="1"/>
<text x="-1" y="0.875" size="1.016" layer="25">&gt;NAME</text>
<text x="-1" y="-1.9" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1" y1="-0.65" x2="-0.5" y2="0.65" layer="51"/>
<rectangle x1="0.5" y1="-0.65" x2="1" y2="0.65" layer="51"/>
</package>
<package name="C1206K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 1206 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 3216</description>
<wire x1="-1.525" y1="0.75" x2="1.525" y2="0.75" width="0.1016" layer="51"/>
<wire x1="1.525" y1="-0.75" x2="-1.525" y2="-0.75" width="0.1016" layer="51"/>
<smd name="1" x="-1.5" y="0" dx="1.5" dy="2" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.5" dy="2" layer="1"/>
<text x="-1.6" y="1.1" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.6" y="-2.1" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6" y1="-0.8" x2="-1.1" y2="0.8" layer="51"/>
<rectangle x1="1.1" y1="-0.8" x2="1.6" y2="0.8" layer="51"/>
</package>
<package name="C1210K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 1210 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 3225</description>
<wire x1="-1.525" y1="1.175" x2="1.525" y2="1.175" width="0.1016" layer="51"/>
<wire x1="1.525" y1="-1.175" x2="-1.525" y2="-1.175" width="0.1016" layer="51"/>
<smd name="1" x="-1.5" y="0" dx="1.5" dy="2.9" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.5" dy="2.9" layer="1"/>
<text x="-1.6" y="1.55" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.6" y="-2.575" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6" y1="-1.25" x2="-1.1" y2="1.25" layer="51"/>
<rectangle x1="1.1" y1="-1.25" x2="1.6" y2="1.25" layer="51"/>
</package>
<package name="C1812K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 1812 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 4532</description>
<wire x1="-2.175" y1="1.525" x2="2.175" y2="1.525" width="0.1016" layer="51"/>
<wire x1="2.175" y1="-1.525" x2="-2.175" y2="-1.525" width="0.1016" layer="51"/>
<smd name="1" x="-2.05" y="0" dx="1.8" dy="3.7" layer="1"/>
<smd name="2" x="2.05" y="0" dx="1.8" dy="3.7" layer="1"/>
<text x="-2.25" y="1.95" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.25" y="-2.975" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-2.25" y1="-1.6" x2="-1.65" y2="1.6" layer="51"/>
<rectangle x1="1.65" y1="-1.6" x2="2.25" y2="1.6" layer="51"/>
</package>
<package name="C1825K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 1825 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 4564</description>
<wire x1="-1.525" y1="3.125" x2="1.525" y2="3.125" width="0.1016" layer="51"/>
<wire x1="1.525" y1="-3.125" x2="-1.525" y2="-3.125" width="0.1016" layer="51"/>
<smd name="1" x="-1.5" y="0" dx="1.8" dy="6.9" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.8" dy="6.9" layer="1"/>
<text x="-1.6" y="3.55" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.6" y="-4.625" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6" y1="-3.2" x2="-1.1" y2="3.2" layer="51"/>
<rectangle x1="1.1" y1="-3.2" x2="1.6" y2="3.2" layer="51"/>
</package>
<package name="C2220K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 2220 reflow solder&lt;/b&gt;&lt;p&gt;Metric Code Size 5650</description>
<wire x1="-2.725" y1="2.425" x2="2.725" y2="2.425" width="0.1016" layer="51"/>
<wire x1="2.725" y1="-2.425" x2="-2.725" y2="-2.425" width="0.1016" layer="51"/>
<smd name="1" x="-2.55" y="0" dx="1.85" dy="5.5" layer="1"/>
<smd name="2" x="2.55" y="0" dx="1.85" dy="5.5" layer="1"/>
<text x="-2.8" y="2.95" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.8" y="-3.975" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-2.8" y1="-2.5" x2="-2.2" y2="2.5" layer="51"/>
<rectangle x1="2.2" y1="-2.5" x2="2.8" y2="2.5" layer="51"/>
</package>
<package name="C2225K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 2225 reflow solder&lt;/b&gt;&lt;p&gt;Metric Code Size 5664</description>
<wire x1="-2.725" y1="3.075" x2="2.725" y2="3.075" width="0.1016" layer="51"/>
<wire x1="2.725" y1="-3.075" x2="-2.725" y2="-3.075" width="0.1016" layer="51"/>
<smd name="1" x="-2.55" y="0" dx="1.85" dy="6.8" layer="1"/>
<smd name="2" x="2.55" y="0" dx="1.85" dy="6.8" layer="1"/>
<text x="-2.8" y="3.6" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.8" y="-4.575" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-2.8" y1="-3.15" x2="-2.2" y2="3.15" layer="51"/>
<rectangle x1="2.2" y1="-3.15" x2="2.8" y2="3.15" layer="51"/>
</package>
<package name="HPC0201">
<description>&lt;b&gt; &lt;/b&gt;&lt;p&gt;
Source: http://www.vishay.com/docs/10129/hpc0201a.pdf</description>
<smd name="1" x="-0.18" y="0" dx="0.2" dy="0.35" layer="1"/>
<smd name="2" x="0.18" y="0" dx="0.2" dy="0.35" layer="1"/>
<text x="-0.75" y="0.74" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.785" y="-1.865" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.305" y1="-0.15" x2="0.305" y2="0.15" layer="51"/>
</package>
<package name="C0201">
<description>Source: http://www.avxcorp.com/docs/catalogs/cx5r.pdf</description>
<smd name="1" x="-0.25" y="0" dx="0.25" dy="0.35" layer="1"/>
<smd name="2" x="0.25" y="0" dx="0.25" dy="0.35" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.3" y1="-0.15" x2="-0.15" y2="0.15" layer="51"/>
<rectangle x1="0.15" y1="-0.15" x2="0.3" y2="0.15" layer="51"/>
<rectangle x1="-0.15" y1="0.1" x2="0.15" y2="0.15" layer="51"/>
<rectangle x1="-0.15" y1="-0.15" x2="0.15" y2="-0.1" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="C-EU">
<wire x1="0" y1="0" x2="0" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-2.032" width="0.1524" layer="94"/>
<text x="1.524" y="0.381" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-4.699" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-2.032" x2="2.032" y2="-1.524" layer="94"/>
<rectangle x1="-2.032" y1="-1.016" x2="2.032" y2="-0.508" layer="94"/>
<pin name="1" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="C-EU" prefix="C" uservalue="yes">
<description>&lt;B&gt;CAPACITOR&lt;/B&gt;, European symbol</description>
<gates>
<gate name="G$1" symbol="C-EU" x="0" y="0"/>
</gates>
<devices>
<device name="C0402" package="C0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0504" package="C0504">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0603" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0805" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1005" package="C1005">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1206" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1210" package="C1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1310" package="C1310">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1608" package="C1608">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1812" package="C1812">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1825" package="C1825">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C2012" package="C2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C3216" package="C3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C3225" package="C3225">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C4532" package="C4532">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C4564" package="C4564">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-024X044" package="C025-024X044">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-025X050" package="C025-025X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-030X050" package="C025-030X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-040X050" package="C025-040X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-050X050" package="C025-050X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-060X050" package="C025-060X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C025_050-024X070" package="C025_050-024X070">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025_050-025X075" package="C025_050-025X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025_050-035X075" package="C025_050-035X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025_050-045X075" package="C025_050-045X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025_050-055X075" package="C025_050-055X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-024X044" package="C050-024X044">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-025X075" package="C050-025X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-045X075" package="C050-045X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-030X075" package="C050-030X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-050X075" package="C050-050X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-055X075" package="C050-055X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-075X075" package="C050-075X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050H075X075" package="C050H075X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="075-032X103" package="C075-032X103">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="075-042X103" package="C075-042X103">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="075-052X106" package="C075-052X106">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="102-043X133" package="C102-043X133">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="102-054X133" package="C102-054X133">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="102-064X133" package="C102-064X133">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="102_152-062X184" package="C102_152-062X184">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-054X183" package="C150-054X183">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-064X183" package="C150-064X183">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-072X183" package="C150-072X183">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-084X183" package="C150-084X183">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-091X182" package="C150-091X182">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-062X268" package="C225-062X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-074X268" package="C225-074X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-087X268" package="C225-087X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-108X268" package="C225-108X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-113X268" package="C225-113X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-093X316" package="C275-093X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-113X316" package="C275-113X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-134X316" package="C275-134X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-205X316" package="C275-205X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="325-137X374" package="C325-137X374">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="325-162X374" package="C325-162X374">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="325-182X374" package="C325-182X374">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="375-192X418" package="C375-192X418">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="375-203X418" package="C375-203X418">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-035X075" package="C050-035X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="375-155X418" package="C375-155X418">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="075-063X106" package="C075-063X106">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-154X316" package="C275-154X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-173X316" package="C275-173X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0402K" package="C0402K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0603K" package="C0603K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0805K" package="C0805K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1206K" package="C1206K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1210K" package="C1210K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1812K" package="C1812K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1825K" package="C1825K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C2220K" package="C2220K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C2225K" package="C2225K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="HPC0201" package="HPC0201">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0201" package="C0201">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="IO1" library="#PaJa_20" deviceset="555" device=""/>
<part name="DZ1" library="#PaJa_20" deviceset="D-ZENEROVA" device="_0,5W_BZX83V" value="10V"/>
<part name="D1." library="#PaJa_20" deviceset="1N4148" device="_SMD_SOD80" value="1N4148"/>
<part name="D3" library="#PaJa_20" deviceset="1N4007" device="_10" value="1N4007"/>
<part name="D2" library="#PaJa_20" deviceset="LED" device="_5" value="zelena_2mA"/>
<part name="D4" library="#PaJa_20" deviceset="1N4007" device="_10" value="1N4007"/>
<part name="C1" library="#PaJa_C-el" deviceset="220M/63V" device="" value="220M/63V"/>
<part name="C6" library="#PaJa_C-el" deviceset="47M/25V" device="" value="47M/25V"/>
<part name="C2" library="#PaJa_20" deviceset="C-KERAMIK" device="_5" value="10n"/>
<part name="C5" library="#PaJa_20" deviceset="C-KERAMIK" device="_5" value="100n"/>
<part name="C7" library="#PaJa_20" deviceset="C-KERAMIK" device="_5" value="100n"/>
<part name="R1" library="#PaJa_20" deviceset="R" device="_7,5" value="15k"/>
<part name="R2" library="#PaJa_20" deviceset="R" device="_7,5" value="1M8"/>
<part name="R6" library="#PaJa_20" deviceset="R" device="_7,5" value="470"/>
<part name="R7" library="#PaJa_20" deviceset="R" device="_7,5" value="560"/>
<part name="GND1" library="#PaJa_20" deviceset="GND" device=""/>
<part name="GND2" library="#PaJa_20" deviceset="GND" device=""/>
<part name="GND3" library="#PaJa_20" deviceset="GND" device=""/>
<part name="GND4" library="#PaJa_20" deviceset="GND" device=""/>
<part name="GND5" library="#PaJa_20" deviceset="GND" device=""/>
<part name="GND6" library="#PaJa_20" deviceset="GND" device=""/>
<part name="GND7" library="#PaJa_20" deviceset="GND" device=""/>
<part name="GND8" library="#PaJa_20" deviceset="GND" device=""/>
<part name="GND9" library="#PaJa_20" deviceset="GND" device=""/>
<part name="GND10" library="#PaJa_20" deviceset="GND" device=""/>
<part name="GND11" library="#PaJa_20" deviceset="GND" device=""/>
<part name="T1" library="#PaJa_20" deviceset="BUZ10" device="_STOJ" value="IRF840"/>
<part name="K1" library="#PaJa_20" deviceset="ARK500/2" device=""/>
<part name="K2" library="#PaJa_20" deviceset="ARK500/2" device=""/>
<part name="C3" library="#PaJa_C-folie" deviceset="1M-K/63V" device="" value="1M"/>
<part name="C4" library="#_GM99" deviceset="CFAC-22,5" device="" value="220n/630V"/>
<part name="GND12" library="#PaJa_20" deviceset="GND" device=""/>
<part name="D4." library="#PaJa_31" deviceset="1N4007" device="_SM-1" value="1N4007"/>
<part name="D3." library="#PaJa_31" deviceset="1N4007" device="_SM-1" value="1N4007"/>
<part name="C2." library="#PaJa_31" deviceset="C-KERAMIK" device="_SMD_1206" value="10n"/>
<part name="C7." library="#PaJa_31" deviceset="C-KERAMIK" device="_SMD_1206" value="100n"/>
<part name="C5." library="#PaJa_31" deviceset="C-KERAMIK" device="_SMD_1206" value="100n"/>
<part name="R1." library="#PaJa_20" deviceset="R" device="_SMD_1206" value="15k"/>
<part name="R2." library="#PaJa_20" deviceset="R" device="_SMD_1206" value="1M8"/>
<part name="R6." library="#PaJa_20" deviceset="R" device="_SMD_1206" value="470"/>
<part name="R7." library="#PaJa_20" deviceset="R" device="_SMD_1206" value="560"/>
<part name="DZ1." library="#PaJa_20" deviceset="D-ZENEROVA" device="_0,5W_BZV55C_SMD" value="10V"/>
<part name="C3." library="rcl" deviceset="C-EU" device="C2225K" value="1M"/>
<part name="GND13" library="#PaJa_20" deviceset="GND" device=""/>
<part name="P1" library="#PaJa_31" deviceset="TRIMR" device="PT6V" value="50k"/>
<part name="D1" library="#PaJa_20" deviceset="1N4148" device="_7,5" value="1N4148"/>
<part name="GND14" library="#PaJa_20" deviceset="GND" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<wire x1="177.8" y1="60.96" x2="177.8" y2="63.5" width="0.1524" layer="94" curve="-180"/>
<wire x1="177.8" y1="63.5" x2="177.8" y2="66.04" width="0.1524" layer="94" curve="-180"/>
<wire x1="177.8" y1="66.04" x2="177.8" y2="68.58" width="0.1524" layer="94" curve="-180"/>
<wire x1="177.8" y1="68.58" x2="177.8" y2="71.12" width="0.1524" layer="94" curve="-180"/>
<wire x1="177.8" y1="71.12" x2="177.8" y2="73.66" width="0.1524" layer="94" curve="-180"/>
<wire x1="170.18" y1="68.58" x2="170.18" y2="66.04" width="0.1524" layer="94" curve="-180"/>
<wire x1="170.18" y1="66.04" x2="170.18" y2="63.5" width="0.1524" layer="94" curve="-180"/>
<wire x1="170.18" y1="63.5" x2="170.18" y2="60.96" width="0.1524" layer="94" curve="-180"/>
<wire x1="177.8" y1="73.66" x2="177.8" y2="78.74" width="0.1524" layer="94"/>
<wire x1="177.8" y1="60.96" x2="177.8" y2="58.42" width="0.1524" layer="94"/>
<wire x1="177.8" y1="58.42" x2="170.18" y2="58.42" width="0.1524" layer="94"/>
<wire x1="170.18" y1="58.42" x2="170.18" y2="60.96" width="0.1524" layer="94"/>
<wire x1="170.18" y1="68.58" x2="170.18" y2="78.74" width="0.1524" layer="94"/>
<wire x1="170.18" y1="58.42" x2="170.18" y2="53.34" width="0.1524" layer="94"/>
<wire x1="180.34" y1="55.88" x2="167.64" y2="55.88" width="0.254" layer="94"/>
<wire x1="167.64" y1="55.88" x2="167.64" y2="76.2" width="0.254" layer="94"/>
<wire x1="167.64" y1="76.2" x2="180.34" y2="76.2" width="0.254" layer="94"/>
<wire x1="180.34" y1="76.2" x2="180.34" y2="55.88" width="0.254" layer="94"/>
<wire x1="173.99" y1="60.96" x2="173.99" y2="73.66" width="0.4064" layer="94"/>
<circle x="170.18" y="58.42" radius="0.1618" width="0.6096" layer="94"/>
<text x="149.86" y="53.34" size="1.778" layer="91">15</text>
<text x="149.86" y="45.72" size="1.778" layer="91">1</text>
<text x="157.48" y="15.24" size="2.54" layer="91">www.paja-trb.cz</text>
<text x="157.48" y="7.62" size="2.54" layer="91">paja@paja-trb.cz</text>
<text x="170.815" y="76.835" size="1.778" layer="94">15</text>
<text x="172.4025" y="53.0225" size="1.778" layer="94" rot="R180">1</text>
<text x="177.8" y="83.82" size="1.778" layer="91">Vn</text>
<text x="185.42" y="88.9" size="1.778" layer="91" rot="R180">10kV - 20kV</text>
<text x="71.12" y="91.44" size="2.54" layer="91">Elektrický ohradník</text>
</plain>
<instances>
<instance part="IO1" gate="IO" x="99.06" y="38.1"/>
<instance part="DZ1" gate="DZ" x="33.02" y="68.58" rot="R90"/>
<instance part="D1." gate="D" x="83.82" y="40.64" rot="R270"/>
<instance part="D3" gate="D" x="139.7" y="22.86" smashed="yes" rot="R90">
<attribute name="VALUE" x="137.795" y="20.3201" size="1.6764" layer="96" rot="MR270"/>
<attribute name="PART" x="137.795" y="22.86" size="1.6764" layer="95" rot="R90"/>
</instance>
<instance part="D2" gate="D" x="33.02" y="38.1" rot="MR270"/>
<instance part="D4" gate="D" x="53.34" y="83.82" smashed="yes" rot="MR270">
<attribute name="VALUE" x="55.245" y="86.3601" size="1.6764" layer="96" rot="R270"/>
<attribute name="PART" x="59.055" y="83.82" size="1.6764" layer="95" rot="MR270"/>
</instance>
<instance part="C1" gate="C" x="43.18" y="53.34" rot="MR270"/>
<instance part="C6" gate="C" x="83.82" y="66.04" rot="MR270"/>
<instance part="C2" gate="C" x="96.52" y="17.78" rot="R270"/>
<instance part="C5" gate="C" x="76.2" y="66.04" rot="MR270"/>
<instance part="C7" gate="C" x="129.54" y="68.58" rot="MR270"/>
<instance part="R1" gate="R" x="53.34" y="68.58" rot="R90"/>
<instance part="R2" gate="R" x="68.58" y="30.48"/>
<instance part="R6" gate="R" x="116.84" y="38.1"/>
<instance part="R7" gate="R" x="33.02" y="53.34" rot="R90"/>
<instance part="GND1" gate="ZEM" x="83.82" y="7.62"/>
<instance part="GND2" gate="ZEM" x="96.52" y="7.62"/>
<instance part="GND3" gate="ZEM" x="33.02" y="7.62"/>
<instance part="GND4" gate="ZEM" x="43.18" y="7.62"/>
<instance part="GND5" gate="ZEM" x="20.32" y="7.62"/>
<instance part="GND6" gate="ZEM" x="104.14" y="7.62"/>
<instance part="GND7" gate="ZEM" x="76.2" y="55.88"/>
<instance part="GND8" gate="ZEM" x="83.82" y="55.88"/>
<instance part="GND9" gate="ZEM" x="139.7" y="7.62"/>
<instance part="GND10" gate="ZEM" x="147.32" y="7.62"/>
<instance part="GND11" gate="ZEM" x="129.54" y="7.62"/>
<instance part="T1" gate="T" x="127" y="40.64" smashed="yes">
<attribute name="NAME" x="130.4925" y="43.18" size="1.9304" layer="95"/>
<attribute name="VALUE" x="120.3325" y="33.655" size="1.9304" layer="96"/>
</instance>
<instance part="K1" gate="K" x="15.24" y="50.8" smashed="yes" rot="MR0">
<attribute name="NAME" x="17.1265" y="53.339" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="14.599" y="42.5265" size="1.778" layer="96" rot="MR270"/>
</instance>
<instance part="K2" gate="K" x="154.94" y="48.26" smashed="yes" rot="MR180">
<attribute name="NAME" x="153.0535" y="45.721" size="1.778" layer="95" rot="MR180"/>
<attribute name="VALUE" x="153.0535" y="42.539" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="C3" gate="CF" x="83.82" y="17.78" rot="R270"/>
<instance part="C4" gate="G$2" x="147.32" y="38.1" smashed="yes" rot="R90">
<attribute name="NAME" x="144.78" y="35.56" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="149.86" y="22.86" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND12" gate="ZEM" x="129.54" y="58.42"/>
<instance part="D4." gate="D" x="48.26" y="83.82" rot="MR270"/>
<instance part="D3." gate="D" x="144.78" y="22.86" smashed="yes" rot="R90">
<attribute name="VALUE" x="141.605" y="12.6999" size="1.6764" layer="96" rot="MR90"/>
<attribute name="PART" x="142.875" y="22.86" size="1.6764" layer="95" rot="R90"/>
</instance>
<instance part="C2." gate="C" x="91.44" y="17.78" rot="R270"/>
<instance part="C7." gate="C" x="134.62" y="68.58" rot="R270"/>
<instance part="C5." gate="C" x="71.12" y="66.04" rot="R270"/>
<instance part="R1." gate="R" x="48.26" y="68.58" rot="R90"/>
<instance part="R2." gate="R" x="68.58" y="35.56"/>
<instance part="R6." gate="R" x="116.84" y="45.72"/>
<instance part="R7." gate="R" x="25.4" y="53.34" rot="R90"/>
<instance part="DZ1." gate="DZ" x="25.4" y="68.58" rot="R90"/>
<instance part="C3." gate="G$1" x="73.66" y="20.32"/>
<instance part="GND13" gate="ZEM" x="73.66" y="7.62"/>
<instance part="P1" gate="TRIMR" x="53.34" y="43.18" rot="MR90"/>
<instance part="D1" gate="D" x="78.74" y="40.64" rot="R270"/>
<instance part="GND14" gate="ZEM" x="91.44" y="7.62"/>
</instances>
<busses>
</busses>
<nets>
<net name="N$3" class="0">
<segment>
<wire x1="53.34" y1="63.5" x2="53.34" y2="60.96" width="0.1524" layer="91"/>
<wire x1="53.34" y1="60.96" x2="48.26" y2="60.96" width="0.1524" layer="91"/>
<wire x1="48.26" y1="60.96" x2="48.26" y2="63.5" width="0.1524" layer="91"/>
<wire x1="53.34" y1="60.96" x2="53.34" y2="48.26" width="0.1524" layer="91"/>
<junction x="53.34" y="60.96"/>
<pinref part="R1" gate="R" pin="1"/>
<pinref part="R1." gate="R" pin="1"/>
<pinref part="P1" gate="TRIMR" pin="2"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<wire x1="96.52" y1="22.86" x2="96.52" y2="25.4" width="0.1524" layer="91"/>
<wire x1="91.44" y1="22.86" x2="96.52" y2="22.86" width="0.1524" layer="91"/>
<junction x="96.52" y="22.86"/>
<pinref part="IO1" gate="IO" pin="CO."/>
<pinref part="C2" gate="C" pin="1"/>
<pinref part="C2." gate="C" pin="1"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<wire x1="104.14" y1="10.16" x2="104.14" y2="25.4" width="0.1524" layer="91"/>
<pinref part="GND6" gate="ZEM" pin="GND"/>
<pinref part="IO1" gate="IO" pin="GND"/>
</segment>
<segment>
<wire x1="83.82" y1="10.16" x2="83.82" y2="15.24" width="0.1524" layer="91"/>
<pinref part="GND1" gate="ZEM" pin="GND"/>
<pinref part="C3" gate="CF" pin="2"/>
</segment>
<segment>
<wire x1="76.2" y1="58.42" x2="76.2" y2="60.96" width="0.1524" layer="91"/>
<wire x1="76.2" y1="60.96" x2="76.2" y2="63.5" width="0.1524" layer="91"/>
<wire x1="71.12" y1="63.5" x2="71.12" y2="60.96" width="0.1524" layer="91"/>
<wire x1="71.12" y1="60.96" x2="76.2" y2="60.96" width="0.1524" layer="91"/>
<junction x="76.2" y="60.96"/>
<pinref part="C5" gate="C" pin="2"/>
<pinref part="GND7" gate="ZEM" pin="GND"/>
<pinref part="C5." gate="C" pin="2"/>
</segment>
<segment>
<wire x1="83.82" y1="58.42" x2="83.82" y2="63.5" width="0.1524" layer="91"/>
<pinref part="C6" gate="C" pin="C_EL-"/>
<pinref part="GND8" gate="ZEM" pin="GND"/>
</segment>
<segment>
<wire x1="33.02" y1="10.16" x2="33.02" y2="33.02" width="0.1524" layer="91"/>
<pinref part="D2" gate="D" pin="K"/>
<pinref part="GND3" gate="ZEM" pin="GND"/>
</segment>
<segment>
<wire x1="43.18" y1="50.8" x2="43.18" y2="10.16" width="0.1524" layer="91"/>
<pinref part="C1" gate="C" pin="C_EL-"/>
<pinref part="GND4" gate="ZEM" pin="GND"/>
</segment>
<segment>
<wire x1="139.7" y1="10.16" x2="139.7" y2="20.32" width="0.1524" layer="91"/>
<wire x1="144.78" y1="20.32" x2="139.7" y2="20.32" width="0.1524" layer="91"/>
<junction x="139.7" y="20.32"/>
<pinref part="D3" gate="D" pin="A"/>
<pinref part="GND9" gate="ZEM" pin="GND"/>
<pinref part="D3." gate="D" pin="A"/>
</segment>
<segment>
<wire x1="129.54" y1="10.16" x2="129.54" y2="35.56" width="0.1524" layer="91"/>
<pinref part="GND11" gate="ZEM" pin="GND"/>
<pinref part="T1" gate="T" pin="S"/>
</segment>
<segment>
<wire x1="20.32" y1="48.26" x2="20.32" y2="10.16" width="0.1524" layer="91"/>
<pinref part="K1" gate="K" pin="K2"/>
<pinref part="GND5" gate="ZEM" pin="GND"/>
</segment>
<segment>
<wire x1="147.32" y1="33.02" x2="147.32" y2="10.16" width="0.1524" layer="91"/>
<pinref part="GND10" gate="ZEM" pin="GND"/>
<pinref part="C4" gate="G$2" pin="1"/>
</segment>
<segment>
<wire x1="129.54" y1="60.96" x2="129.54" y2="63.5" width="0.1524" layer="91"/>
<wire x1="129.54" y1="63.5" x2="129.54" y2="66.04" width="0.1524" layer="91"/>
<wire x1="134.62" y1="66.04" x2="134.62" y2="63.5" width="0.1524" layer="91"/>
<wire x1="134.62" y1="63.5" x2="129.54" y2="63.5" width="0.1524" layer="91"/>
<junction x="129.54" y="63.5"/>
<pinref part="GND12" gate="ZEM" pin="GND"/>
<pinref part="C7" gate="C" pin="2"/>
<pinref part="C7." gate="C" pin="2"/>
</segment>
<segment>
<wire x1="73.66" y1="15.24" x2="73.66" y2="10.16" width="0.1524" layer="91"/>
<pinref part="C3." gate="G$1" pin="2"/>
<pinref part="GND13" gate="ZEM" pin="GND"/>
</segment>
<segment>
<wire x1="96.52" y1="15.24" x2="96.52" y2="10.16" width="0.1524" layer="91"/>
<pinref part="C2" gate="C" pin="2"/>
<pinref part="GND2" gate="ZEM" pin="GND"/>
</segment>
<segment>
<wire x1="91.44" y1="15.24" x2="91.44" y2="10.16" width="0.1524" layer="91"/>
<pinref part="C2." gate="C" pin="2"/>
<pinref part="GND14" gate="ZEM" pin="GND"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<wire x1="111.76" y1="38.1" x2="111.76" y2="45.72" width="0.1524" layer="91"/>
<junction x="111.76" y="38.1"/>
<pinref part="IO1" gate="IO" pin="OUT"/>
<pinref part="R6" gate="R" pin="1"/>
<pinref part="R6." gate="R" pin="1"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<wire x1="88.9" y1="45.72" x2="83.82" y2="45.72" width="0.1524" layer="91"/>
<wire x1="83.82" y1="45.72" x2="83.82" y2="43.18" width="0.1524" layer="91"/>
<wire x1="55.88" y1="45.72" x2="78.74" y2="45.72" width="0.1524" layer="91"/>
<wire x1="78.74" y1="45.72" x2="83.82" y2="45.72" width="0.1524" layer="91"/>
<wire x1="78.74" y1="43.18" x2="78.74" y2="45.72" width="0.1524" layer="91"/>
<junction x="83.82" y="45.72"/>
<junction x="78.74" y="45.72"/>
<pinref part="IO1" gate="IO" pin="DIS."/>
<pinref part="D1." gate="D" pin="A"/>
<pinref part="P1" gate="TRIMR" pin="3"/>
<pinref part="D1" gate="D" pin="A"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<wire x1="73.66" y1="35.56" x2="73.66" y2="30.48" width="0.1524" layer="91"/>
<wire x1="88.9" y1="38.1" x2="88.9" y2="30.48" width="0.1524" layer="91"/>
<wire x1="88.9" y1="30.48" x2="83.82" y2="30.48" width="0.1524" layer="91"/>
<wire x1="83.82" y1="30.48" x2="83.82" y2="35.56" width="0.1524" layer="91"/>
<wire x1="83.82" y1="30.48" x2="83.82" y2="22.86" width="0.1524" layer="91"/>
<wire x1="73.66" y1="22.86" x2="83.82" y2="22.86" width="0.1524" layer="91"/>
<wire x1="73.66" y1="30.48" x2="78.74" y2="30.48" width="0.1524" layer="91"/>
<wire x1="78.74" y1="30.48" x2="83.82" y2="30.48" width="0.1524" layer="91"/>
<wire x1="78.74" y1="35.56" x2="78.74" y2="30.48" width="0.1524" layer="91"/>
<junction x="88.9" y="30.48"/>
<junction x="83.82" y="30.48"/>
<junction x="83.82" y="22.86"/>
<junction x="73.66" y="30.48"/>
<junction x="78.74" y="30.48"/>
<pinref part="R2" gate="R" pin="2"/>
<pinref part="R2." gate="R" pin="2"/>
<pinref part="IO1" gate="IO" pin="TH."/>
<pinref part="IO1" gate="IO" pin="TR."/>
<pinref part="D1." gate="D" pin="K"/>
<pinref part="C3" gate="CF" pin="1"/>
<pinref part="C3." gate="G$1" pin="1"/>
<pinref part="D1" gate="D" pin="K"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<wire x1="33.02" y1="40.64" x2="33.02" y2="48.26" width="0.1524" layer="91"/>
<wire x1="25.4" y1="48.26" x2="33.02" y2="48.26" width="0.1524" layer="91"/>
<junction x="33.02" y="48.26"/>
<pinref part="D2" gate="D" pin="A"/>
<pinref part="R7" gate="R" pin="1"/>
<pinref part="R7." gate="R" pin="1"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<wire x1="33.02" y1="58.42" x2="33.02" y2="63.5" width="0.1524" layer="91"/>
<wire x1="25.4" y1="58.42" x2="33.02" y2="58.42" width="0.1524" layer="91"/>
<wire x1="25.4" y1="63.5" x2="25.4" y2="58.42" width="0.1524" layer="91"/>
<junction x="33.02" y="58.42"/>
<junction x="25.4" y="58.42"/>
<pinref part="R7" gate="R" pin="2"/>
<pinref part="DZ1" gate="DZ" pin="A"/>
<pinref part="R7." gate="R" pin="2"/>
<pinref part="DZ1." gate="DZ" pin="A"/>
</segment>
</net>
<net name="+12V" class="0">
<segment>
<wire x1="33.02" y1="71.12" x2="33.02" y2="88.9" width="0.1524" layer="91"/>
<wire x1="33.02" y1="88.9" x2="43.18" y2="88.9" width="0.1524" layer="91"/>
<wire x1="43.18" y1="88.9" x2="48.26" y2="88.9" width="0.1524" layer="91"/>
<wire x1="48.26" y1="88.9" x2="53.34" y2="88.9" width="0.1524" layer="91"/>
<wire x1="53.34" y1="88.9" x2="53.34" y2="86.36" width="0.1524" layer="91"/>
<wire x1="43.18" y1="58.42" x2="43.18" y2="88.9" width="0.1524" layer="91"/>
<wire x1="53.34" y1="88.9" x2="129.54" y2="88.9" width="0.1524" layer="91"/>
<wire x1="129.54" y1="88.9" x2="147.32" y2="88.9" width="0.1524" layer="91"/>
<wire x1="20.32" y1="50.8" x2="20.32" y2="88.9" width="0.1524" layer="91"/>
<wire x1="20.32" y1="88.9" x2="33.02" y2="88.9" width="0.1524" layer="91"/>
<wire x1="149.86" y1="50.8" x2="147.32" y2="50.8" width="0.1524" layer="91"/>
<wire x1="147.32" y1="50.8" x2="147.32" y2="88.9" width="0.1524" layer="91"/>
<wire x1="129.54" y1="73.66" x2="129.54" y2="76.2" width="0.1524" layer="91"/>
<wire x1="129.54" y1="76.2" x2="129.54" y2="88.9" width="0.1524" layer="91"/>
<wire x1="134.62" y1="73.66" x2="134.62" y2="76.2" width="0.1524" layer="91"/>
<wire x1="134.62" y1="76.2" x2="129.54" y2="76.2" width="0.1524" layer="91"/>
<wire x1="25.4" y1="71.12" x2="33.02" y2="71.12" width="0.1524" layer="91"/>
<wire x1="48.26" y1="86.36" x2="48.26" y2="88.9" width="0.1524" layer="91"/>
<junction x="43.18" y="88.9"/>
<junction x="53.34" y="88.9"/>
<junction x="33.02" y="88.9"/>
<junction x="129.54" y="88.9"/>
<junction x="129.54" y="76.2"/>
<junction x="33.02" y="71.12"/>
<junction x="48.26" y="88.9"/>
<label x="27.94" y="86.36" size="1.778" layer="95" rot="R180"/>
<pinref part="DZ1" gate="DZ" pin="K"/>
<pinref part="D4" gate="D" pin="A"/>
<pinref part="C1" gate="C" pin="C_EL+"/>
<pinref part="K1" gate="K" pin="K1"/>
<pinref part="K2" gate="K" pin="K2"/>
<pinref part="C7" gate="C" pin="1"/>
<pinref part="D4." gate="D" pin="A"/>
<pinref part="C7." gate="C" pin="1"/>
<pinref part="DZ1." gate="DZ" pin="K"/>
</segment>
</net>
<net name="+NAP" class="0">
<segment>
<wire x1="104.14" y1="50.8" x2="104.14" y2="76.2" width="0.1524" layer="91"/>
<wire x1="104.14" y1="76.2" x2="96.52" y2="76.2" width="0.1524" layer="91"/>
<wire x1="96.52" y1="76.2" x2="96.52" y2="50.8" width="0.1524" layer="91"/>
<wire x1="96.52" y1="76.2" x2="83.82" y2="76.2" width="0.1524" layer="91"/>
<wire x1="83.82" y1="76.2" x2="76.2" y2="76.2" width="0.1524" layer="91"/>
<wire x1="76.2" y1="76.2" x2="71.12" y2="76.2" width="0.1524" layer="91"/>
<wire x1="71.12" y1="76.2" x2="53.34" y2="76.2" width="0.1524" layer="91"/>
<wire x1="53.34" y1="76.2" x2="53.34" y2="73.66" width="0.1524" layer="91"/>
<wire x1="53.34" y1="78.74" x2="53.34" y2="76.2" width="0.1524" layer="91"/>
<wire x1="48.26" y1="78.74" x2="48.26" y2="76.2" width="0.1524" layer="91"/>
<wire x1="48.26" y1="76.2" x2="48.26" y2="73.66" width="0.1524" layer="91"/>
<wire x1="53.34" y1="76.2" x2="48.26" y2="76.2" width="0.1524" layer="91"/>
<wire x1="83.82" y1="71.12" x2="83.82" y2="76.2" width="0.1524" layer="91"/>
<wire x1="76.2" y1="71.12" x2="76.2" y2="76.2" width="0.1524" layer="91"/>
<wire x1="71.12" y1="71.12" x2="71.12" y2="76.2" width="0.1524" layer="91"/>
<junction x="96.52" y="76.2"/>
<junction x="53.34" y="76.2"/>
<junction x="48.26" y="76.2"/>
<junction x="83.82" y="76.2"/>
<junction x="76.2" y="76.2"/>
<junction x="71.12" y="76.2"/>
<pinref part="IO1" gate="IO" pin="RES"/>
<pinref part="IO1" gate="IO" pin="VCC"/>
<pinref part="C6" gate="C" pin="C_EL+"/>
<pinref part="C5" gate="C" pin="1"/>
<pinref part="R1" gate="R" pin="2"/>
<pinref part="D4" gate="D" pin="K"/>
<pinref part="D4." gate="D" pin="K"/>
<pinref part="C5." gate="C" pin="1"/>
<pinref part="R1." gate="R" pin="2"/>
</segment>
</net>
<net name="OUT" class="0">
<segment>
<wire x1="147.32" y1="43.18" x2="147.32" y2="48.26" width="0.1524" layer="91"/>
<wire x1="147.32" y1="48.26" x2="139.7" y2="48.26" width="0.1524" layer="91"/>
<wire x1="139.7" y1="48.26" x2="139.7" y2="27.94" width="0.1524" layer="91"/>
<wire x1="139.7" y1="48.26" x2="129.54" y2="48.26" width="0.1524" layer="91"/>
<wire x1="129.54" y1="48.26" x2="129.54" y2="45.72" width="0.1524" layer="91"/>
<wire x1="149.86" y1="48.26" x2="147.32" y2="48.26" width="0.1524" layer="91"/>
<wire x1="144.78" y1="27.94" x2="139.7" y2="27.94" width="0.1524" layer="91"/>
<junction x="139.7" y="48.26"/>
<junction x="147.32" y="48.26"/>
<junction x="139.7" y="27.94"/>
<pinref part="D3" gate="D" pin="K"/>
<pinref part="T1" gate="T" pin="D"/>
<pinref part="K2" gate="K" pin="K1"/>
<pinref part="C4" gate="G$2" pin="2"/>
<pinref part="D3." gate="D" pin="K"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<wire x1="121.92" y1="45.72" x2="121.92" y2="38.1" width="0.1524" layer="91"/>
<junction x="121.92" y="38.1"/>
<pinref part="R6" gate="R" pin="2"/>
<pinref part="T1" gate="T" pin="G"/>
<pinref part="R6." gate="R" pin="2"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<wire x1="170.18" y1="53.34" x2="170.18" y2="48.26" width="0.1524" layer="91"/>
<wire x1="170.18" y1="48.26" x2="160.02" y2="48.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<wire x1="160.02" y1="50.8" x2="162.56" y2="50.8" width="0.1524" layer="91"/>
<wire x1="162.56" y1="50.8" x2="162.56" y2="81.28" width="0.1524" layer="91"/>
<wire x1="162.56" y1="81.28" x2="170.18" y2="81.28" width="0.1524" layer="91"/>
<wire x1="170.18" y1="81.28" x2="170.18" y2="78.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<wire x1="177.8" y1="78.74" x2="177.8" y2="81.28" width="0.1524" layer="91"/>
<wire x1="177.8" y1="81.28" x2="185.42" y2="81.28" width="0.1524" layer="91"/>
<wire x1="185.42" y1="81.28" x2="182.88" y2="83.82" width="0.1524" layer="91"/>
<wire x1="185.42" y1="81.28" x2="182.88" y2="78.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<wire x1="53.34" y1="38.1" x2="53.34" y2="30.48" width="0.1524" layer="91"/>
<wire x1="53.34" y1="30.48" x2="60.96" y2="30.48" width="0.1524" layer="91"/>
<wire x1="60.96" y1="30.48" x2="63.5" y2="30.48" width="0.1524" layer="91"/>
<wire x1="63.5" y1="35.56" x2="60.96" y2="35.56" width="0.1524" layer="91"/>
<wire x1="60.96" y1="35.56" x2="60.96" y2="30.48" width="0.1524" layer="91"/>
<junction x="60.96" y="30.48"/>
<pinref part="P1" gate="TRIMR" pin="1"/>
<pinref part="R2" gate="R" pin="1"/>
<pinref part="R2." gate="R" pin="1"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
