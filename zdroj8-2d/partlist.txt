Partlist

Exported from zdroj8_2d.sch at 11. 8. 2013 14:16:24

EAGLE Version 6.0.0 Copyright (c) 1988-2011 CadSoft

Assembly variant: 

Part       Value          Device           Package      Library       Sheet

21V                       ARK800/3         ARK800/3     #Palito_rozne 1
C1         4700uF/35V     4700M/35V        C_EL7,5+     #PaJa_C-el_2  1
C2         4700uF/35V     4700M/35V        C_EL7,5+     #PaJa_C-el_2  1
C3         4700uF/35V     4700M/35V        C_EL7,5+     #PaJa_C-el_2  1
C4         4700uF/35V     4700M/35V        C_EL7,5+     #PaJa_C-el_2  1
C5         10uF/35V       CPOL-EUE1.8-4    E1,8-4       rcl           1
C9         47nF ker/100V  C-EU050-024X044  C050-024X044 rcl           1
C10        47nF ker/100V  C-EU050-024X044  C050-024X044 rcl           1
C11        47nF ker/100V  C-EU050-024X044  C050-024X044 rcl           1
C12        47nF ker/100V  C-EU050-024X044  C050-024X044 rcl           1
D1         1N4148         1N4148DO35-7     DO35-7       diode         1
LADA2075_1                ARK800/3         ARK800/3     #Palito_rozne 1
LED                       ARK800/3         ARK800/3     #Palito_rozne 1
MOSTIK1    8A             RECTIFIER-2KBB-R 2KBB-R       rectifier     1
OCHRANA                   ARK800/3         ARK800/3     #Palito_rozne 1
R1         2k2            R-EU_0204/7      0204/7       rcl           1
